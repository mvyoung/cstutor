package db;
import java.util.Collection;
import quiz.QuizResult;
/**
 * This class offers a vendor-independent interface to access a database of
 * scores.
 *
 * <p>This class will eventually be an interface. But
 * for the purposes of modeling it is an abstract
 * class.
 *
 * @author Raymond Pederson
 * @author Sajjad Mirza
 */
public abstract class ScoringDatabase {

   /*
    * Inserts a QuizResult into the database.
    */
   abstract void insert(QuizResult result);

   /*
    * Reads the entire set of QuizResults from the database.
    *
    * <p>Eventually the method will accept some form of parameter, to perform
    * selection operations on the database.
    */
   abstract Collection<QuizResult> getAllQuizResults();
}
