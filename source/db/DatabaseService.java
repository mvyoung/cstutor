package db;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * This class is responsible for establishing connections to
 * the database.
 * 
 * @author smirza
 *
 */
public class DatabaseService {
   
   private Connection connect; 
   
   /**
    * Constructs the service object and initializes the connections.
    */
   public DatabaseService() {
      connect = null;
      try {
         Class.forName("org.sqlite.JDBC");
         connect = DriverManager.getConnection("jdbc:sqlite:CSTutor.db");
      } 
      catch (Exception e) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
      System.out.println("Opened database successfully");
   }
   
   public DatabaseService(String name) {
      connect = null;
      try {
         Class.forName("org.sqlite.JDBC");
         connect = DriverManager.getConnection("jdbc:sqlite:" + name + ".db");
      } 
      catch (Exception e) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         System.exit(0);
      }
   }
   
   /**
    * Returns a java.sql.Connection object for this database.
    * @return
    */
   public Connection getConnect() {
      return connect;
   }
}
