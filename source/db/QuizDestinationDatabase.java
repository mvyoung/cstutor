package db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import main.CSTutor;
import quiz.Quiz;
import course.CodeSegment;
import course.Exercise;
import course.Lesson;
import course.Page;
import course.TextSegment;
import course.Topic;

/**
 * The QuizDestinationDatabase is the table to be put into the CSTutor.db
 * which allows administrators set up quizzes with branching paths in
 * where they go when a quiz is failed based on there score.
 * @author Raymond Pederson
 *
 */
public class QuizDestinationDatabase {

   /**
    * The connection to the database
    */
   private Connection connect;

   /**
    * Constructs the table into the CSTutor.db
    */
   public QuizDestinationDatabase() {
      connect = CSTutor.getInstance().getDBService().getConnect();
      String sql;
      String tableName = "QUIZ_DESTINATION";
      Statement stmt;
      
      try {
         stmt = connect.createStatement();
         sql = QuizDestinationStatement(tableName);
         stmt.executeUpdate(sql);
         stmt.close();
      }
      catch (Exception e) {
         System.err.println(e.getClass().getName() + ": " + e.getMessage());
         System.exit(0);
      }
   }
   
   /**
    * This method creates the sql command to make a table
    * @param name is the name of the table
    * @return the string of the sql command
    */
   private String QuizDestinationStatement(String name) {
      return "CREATE TABLE IF NOT EXISTS " + name +
            "(ID          INTEGER         PRIMARY KEY, " +
            "QUIZ_ID      INTEGER         NOT NULL, " +
            "LOW_SCORE    INTEGER         NOT NULL, " +
            "HIGH_SCORE   INTEGER         NOT NULL, " +
            "PAGE_ID_DEST INTEGER         NOT NULL)";
   }
   
   /**
    * Adds into the table a quiz id of where this destination 
    * is from, the new destination, and the range of values of when
    * this new destination is used.
    * @param q is the QUiz it is based off
    * @param low is the low range
    * @param high is the high range
    * @param p is the page destination
    * @return is the ID of the quizDestination
    */
   public long addQuizDestination(Quiz q, Integer low, Integer high, Page p) {
      try {
         connect.setAutoCommit(false);
         Statement stmt = connect.createStatement();
         String sql = "INSERT INTO QUIZ_DESTINATION " +
                      "(QUIZ_ID, LOW_SCORE, HIGH_SCORE, PAGE_ID_DEST) " +
                      "VALUES (" + Long.toString(q.pageID) + 
                      ", " + Integer.toString(low) + 
                      ", " + Integer.toString(high) + 
                      ", " + Long.toString(p.pageID) + ");";
         stmt.executeUpdate(sql);
         stmt.close();
         
         stmt = connect.createStatement();
         ResultSet rs = stmt.executeQuery("SELECT last_insert_rowid()");
         long ret = rs.getLong(1);
         
         connect.commit();
         connect.setAutoCommit(true);
         return ret;
      }
      catch (SQLException e) {
         System.err.println(e.getClass().getName() + ": " + e.getMessage());
         throw new RuntimeException(e);
      }
   }
   
   /**
    * Removes a quiz destination
    * @param id is the id of the quiz destination
    */
   public void removeQuizDestination(long id) {
      try {
         Statement stmt = connect.createStatement();
         String sql = "DELETE FROM QUIZ_DESTINATION " +
                      "WHERE ID = " + Long.toString(id) + ";";
         stmt.executeUpdate(sql);
         stmt.close();
      }
      catch (Exception e) {
         System.err.println(e.getClass().getName() + ": " + e.getMessage());
         System.exit(0);
      }
   }
   
   /*
    * WARNING: Only use if your the navigator
    */
   /**
    * Removes all quizDestinations under a specific quiz page
    * @param q the quiz page
    */
   public void removeAllQuizDestinations(Page q) {
      try {
         Statement stmt = connect.createStatement();
         String sql = "DELETE FROM QUIZ_DESTINATION " +
                      "WHERE QUIZ_ID = " + Long.toString(q.pageID) + ";";
         stmt.executeUpdate(sql);
         stmt.close();
      }
      catch (Exception e) {
         System.err.println(e.getClass().getName() + ": " + e.getMessage());
         System.exit(0);
      }
   }
   
   /**
    * Edits a quiz and puts it into the table where the destination 
    * is from, the new destination, and the range of values of when
    * this new destination is used.
    * @param id is the id of the quiz destination
    * @param q is the QUiz it is based off
    * @param low is the low range
    * @param high is the high range
    * @param p is the page destination
    * @return is the ID of the quizDestination
    */
   public void editQuizDestination
      (long id, Quiz q, Integer low, Integer high, Page p) {
      try {
         Statement stmt = connect.createStatement();
         String sql = "UPDATE QUIZ_DESTINATION " +
                      "SET QUIZ_ID = " + Long.toString(q.pageID) +
                      ", LOW_SCORE = " + Integer.toString(low) + 
                      ", HIGH_SCORE = " + Integer.toString(high) +
                      ", PAGE_ID_DEST = " + Long.toString(p.pageID) +
                      " WHERE ID = " + Long.toString(id) + ";";
         stmt.executeUpdate(sql);
         stmt.close();
      }
      catch (Exception e) {
         System.err.println(e.getClass().getName() + ": " + e.getMessage());
         System.exit(0);
      }
   }
   
   /*
    * Uses CSTutors main unlike most database methods!!!
    */
   /**
    * Finds the quiz destination that should be used
    * @param id the id of the quiz
    * @param score the score the user got
    * @return the page to go to
    */
   public Page selectQuizDestination(long id, int score) {
      Page p = null;

      try {
         Statement stmt = connect.createStatement();
         String sql = "SELECT * FROM QUIZ_DESTINATION" +
                      "WHERE QUIZ_ID = " + Long.toString(id) + ";";
         ResultSet rs = stmt.executeQuery(sql);
         while (rs.next()) {
            if (rs.getInt("LOW_SCORE") <= score && 
                  rs.getInt("HIGH_SCORE") >= score) {
               p = CSTutor.getInstance().
                     getDBPage().selectPage(rs.getInt("PAGE_ID_DEST"));
               break;
            }
         }
         if (p == null) {
            Lesson temp = CSTutor.getInstance().
                  getDBPage().selectPage(id).parentLesson;
            p = CSTutor.getInstance().getDBLesson().findPages(temp).get(0);
         }
         rs.close();
         stmt.close();
         return p;
      }
      catch (Exception e) {
         System.err.println(e.getClass().getName() + ": " + e.getMessage());
         throw new RuntimeException();
      }
   }
}
