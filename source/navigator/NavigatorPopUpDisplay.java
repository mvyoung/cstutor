package navigator;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.tree.TreePath;

import main.CSTutor;
import roadmap.RoadmapPanel;
import course.Course;
import course.Lesson;
import course.Topic;

/**
 * This class creates the display when someone decides to add a 
 * course/topic/lesson. It does this by displaying a new window which
 * lets the user put in the name.
 * @author Raymond Pederson
 *
 */
public class NavigatorPopUpDisplay extends JPanel implements ActionListener {

   /**
    * The frame of the window
    */
   public JFrame frame;
   
   /**
    * The name of the created course/topic/lesson
    */
   public JTextField name;
   
   /**
    * The name in string form
    */
   public String savedName = "";
   
   /**
    * The type of node
    */
   private int nt;
   
   /**
    * The path of the node
    */
   private TreePath selPath;
   
   /**
    * The constructo of the window
    * @param str the prompt to the user
    * @param nodeType the type of node
    */
   public NavigatorPopUpDisplay(String str, int nodeType) {
      frame = new JFrame("Frame");
      nt = nodeType;
      
      JLabel stringLabel = new JLabel(str);
      JButton done = new JButton("Submit");
      
      ActionListener exit = new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            frame.setVisible(false);
            switch (nt) {
               case 1:  CSTutor.getInstance().
                           getDBCourse().addCourse(makeCourse());
                        break;
               case 2:  setTopic();
                        break;
               case 3:  setLesson();
                        break;
               default: break;
            }
            refresh();
         }
      };
      
      done.addActionListener(exit);
      name = new JTextField(5);
      name.addActionListener(this);
      name.addKeyListener(
               new KeyListener() {
                  @Override
                  public void keyTyped(KeyEvent e){}
                  @Override
                  public void keyPressed(KeyEvent e) {}
                  @Override
                  public void keyReleased(KeyEvent e) {
                     savedName = name.getText();
                  }
               }
            );
      
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.getContentPane().add(stringLabel, BorderLayout.NORTH);
      frame.getContentPane().add(name, BorderLayout.CENTER);
      frame.getContentPane().add(done, BorderLayout.SOUTH);

      frame.setSize(250, 100);
      frame.setVisible(true);
   }
   
   @Override
   public void actionPerformed(ActionEvent arg0) {
      savedName = name.getText();
   }
   
   /**
    * Makes the course to be added into the db
    * @return the new course to be added
    */
   public Course makeCourse() {
      return new Course(savedName, 
            CSTutor.getInstance().getDBCourse().findHighestOrdinance());
   }
   
   /**
    * Puts the topic into the db
    */
   public void setTopic() {
      int count = selPath.getPathCount();
      long courseID = ((NavigatorNode)selPath.
            getPathComponent(count - 2)).getID();
      CSTutor.getInstance().getDBTopic().addTopic(makeTopic(courseID));
   }
   
   /**
    * Puts the lesson into the db
    */
   public void setLesson() {
      int count = selPath.getPathCount();
      long topicID = ((NavigatorNode)selPath.
            getPathComponent(count - 2)).getID();
      CSTutor.getInstance().getDBLesson().addLesson(makeLesson(topicID));
   }
   
   /**
    * Makes the topic to be added into the db
    * @return the new topic to be added
    */
   public Topic makeTopic(long id) {
      return new Topic(CSTutor.getInstance().getDBCourse().selectCourse(id),
            savedName,
            CSTutor.getInstance().getDBTopic().findHighestOrdinance(id));
   }

   /**
    * Makes the lesson to be added into the db
    * @return the new lesson to be added
    */
   public Lesson makeLesson(long id) {
      return new Lesson(CSTutor.getInstance().getDBTopic().selectTopic(id),
            savedName,
            CSTutor.getInstance().getDBLesson().findHighestOrdinance(id));
   }
   
   /**
    * Gets the path of the node
    */
   public void setPath(TreePath sp) {
      selPath = sp;
   }
   
   /**
    * Refreshes the navigator
    */
   private void refresh() {
      ((NavigatorPanel)CSTutor.getInstance().
            panels.get(CSTutor.NAVIGATOR)).refreshNavigator();
      ((RoadmapPanel)CSTutor.getInstance().
            panels.get(CSTutor.ROADMAP)).setNavPanel();
   }
}
