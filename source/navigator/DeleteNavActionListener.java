package navigator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;

import main.CSTutor;
import roadmap.RoadmapPanel;
import course.Course;
import course.Lesson;
import course.Page;
import course.Topic;

/**
* This is the class which is used when you right click on a navgator Course/
* Topic/Lesson/Page. It allows you to delete them by calling the database 
* methods made in the db package.
*
* @author Raymond Pederson
*/
public class DeleteNavActionListener implements ActionListener {
   
   /**
    * The node to delete
    */
   private NavigatorNode nodeToDelete;
   
   /**
    * The listener for the deleting something from the navigator
    * @param selectedNode the node that is selected
    */
   public DeleteNavActionListener(NavigatorNode selectedNode) {
      super();
      nodeToDelete = selectedNode;
   }
   
   /**
    * Selects which type of node is being deleter
    * @param selectedNode the node that is selected
    */
   public void delete(NavigatorNode selectedNode) {
      switch (selectedNode.getDepth()) {
         case 1:  deleteCourse(selectedNode);
                  break;
         case 2:  deleteTopic(selectedNode);
                  break;
         case 3:  deleteLesson(selectedNode);
                  break;
         case 4:  if(!selectedNode.getQuiz()) {
                     deletePage(selectedNode);
                  }
                  else {
                     deletePage(selectedNode);
                  }
                  break;
         default: break;
      }
   }
   
   /**
    * Deletes a Course from the nav panel.
    * @param selectedNode the Node that is selected
    */
   public void deleteCourse(NavigatorNode selectedNode) {
      ArrayList<Course> newCourses = 
         CSTutor.getInstance().getDBCourse().selectAllCourses();
      Course tCourse;
      int i = -1;
      
      do i++; 
      while (selectedNode.getID() != newCourses.get(i).courseID);

      ArrayList<Topic> newTopics = CSTutor.getInstance().getDBCourse().
         findTopics(newCourses.get(i));
      for (int k = 0; k < newTopics.size(); k++) {
         deleteTopicSpecial(newTopics.get(k));
      }
      CSTutor.getInstance().getDBCourse().removeCourse(newCourses.get(i));
      newCourses.remove(i);
      
      for (i = 0; i < newCourses.size(); i++) {
         tCourse = newCourses.get(i);         
         tCourse.courseOrder = i + 1;
         CSTutor.getInstance().getDBCourse().editCourse(tCourse);
      }
      
      refresh();
   }
   
   /**
    * Deletes a Topic from the nav panel.
    * @param selectedNode the Node that is selected
    */
   public void deleteTopic(NavigatorNode selectedNode) {
      long id = CSTutor.getInstance().getDBTopic().
         selectTopic(selectedNode.getID()).topicID;
      ArrayList<Topic> newTopics = 
         CSTutor.getInstance().getDBTopic().selectAllTopics(id);
      Topic tTopic;
      int i = -1;
      
      do i++; 
      while (selectedNode.getID() != newTopics.get(i).topicID);

      ArrayList<Lesson> newLessons = CSTutor.getInstance().getDBTopic().
         findLessons(newTopics.get(i));
      for (int k = 0; k < newLessons.size(); k++) {
         deleteLessonSpecial(newLessons.get(k));
      }
      CSTutor.getInstance().getDBTopic().removeTopic(newTopics.get(i));
      newTopics.remove(i);
      
      for (i = 0; i < newTopics.size(); i++) {
         tTopic = newTopics.get(i);
         tTopic.topicOrder = i + 1;
         CSTutor.getInstance().getDBTopic().editTopic(tTopic);
      }
      
      refresh();
   }
   
   /**
    * Deletes a Lesson from the nav panel.
    * @param selectedNode the Node that is selected
    */
   public void deleteLesson(NavigatorNode selectedNode) {
      long id = CSTutor.getInstance().getDBLesson().
         selectLesson(selectedNode.getID()).lessonID;
      ArrayList<Lesson> newLessons = 
         CSTutor.getInstance().getDBLesson().selectAllLessons(id);
      Lesson tLesson;
      int i = -1;
      
      do i++; 
      while (selectedNode.getID() != newLessons.get(i).lessonID);

      ArrayList<Page> newPages = CSTutor.getInstance().getDBLesson().
            findPages(newLessons.get(i));
      for (int k = 0; k < newPages.size(); k++) {
         deletePageSpecial(newPages.get(k));
      }
      CSTutor.getInstance().getDBLesson().removeLesson(newLessons.get(i));
      newLessons.remove(i);
      
      for (i = 0; i < newLessons.size(); i++) {
         tLesson = newLessons.get(i);
         tLesson.lessonOrder = i + 1;
         CSTutor.getInstance().getDBLesson().editLesson(tLesson);
      }
      
      refresh();
   }
   
   /**
    * Deletes a Page from the nav panel.
    * @param selectedNode the Node that is selected
    */
   public void deletePage(NavigatorNode selectedNode) {
      long id = CSTutor.getInstance().getDBPage().
         selectPage(selectedNode.getID()).pageID;
      ArrayList<Page> newPages = 
         CSTutor.getInstance().getDBPage().selectAllPages(id);
      Page tPage;
      int i = -1;
      
      do i++; 
      while (selectedNode.getID() != newPages.get(i).pageID);

      CSTutor.getInstance().getDBQuizDestination().
            removeAllQuizDestinations(newPages.get(i));
      CSTutor.getInstance().getDBPage().removePage(newPages.get(i));
      newPages.remove(i);
      
      for (i = 0; i < newPages.size(); i++) {
         tPage = newPages.get(i);
         tPage.pageOrder = i + 1;
         CSTutor.getInstance().getDBPage().editPage(tPage);
      }
      
      refresh();
   }
   /**
    * Deletes a Topic from the nav panel called by deleting a course.
    * @param t the topic to be deleted
    */
   
   public void deleteTopicSpecial(Topic t) {
      ArrayList<Lesson> newLessons = CSTutor.getInstance().getDBTopic().
         findLessons(t);
      for (int k = 0; k < newLessons.size(); k++) {
         deleteLessonSpecial(newLessons.get(k));
      }
      CSTutor.getInstance().getDBTopic().removeTopic(t);
   }
   
   /**
    * Deletes a Lesson from the nav panel called by deleting a topic.
    * @param l the lesson to be deleted
    */
   public void deleteLessonSpecial(Lesson l) {
      ArrayList<Page> newPages = CSTutor.getInstance().getDBLesson().
            findPages(l);
      for (int k = 0; k < newPages.size(); k++) {
         deletePageSpecial(newPages.get(k));
      }
      CSTutor.getInstance().getDBLesson().removeLesson(l);
   }
   
   /**
    * Deletes a Page from the nav panel called by deleting a lesson.
    * @param p the page to be deleted
    */
   public void deletePageSpecial(Page p) {
      CSTutor.getInstance().getDBQuizDestination().removeAllQuizDestinations(p);
      CSTutor.getInstance().getDBPage().removePage(p);
   }

   @Override
   public void actionPerformed(ActionEvent e) {
      delete(nodeToDelete);
      //nodeToDelete.navRemove();
      refresh();
   }
   
   /**
    * Refreshes the Navigator panel and RoadMap panel when called
    */
   private void refresh() {
      ((NavigatorPanel)CSTutor.getInstance().
            panels.get(CSTutor.NAVIGATOR)).refreshNavigator();
      ((RoadmapPanel)CSTutor.getInstance().
            panels.get(CSTutor.ROADMAP)).setNavPanel();
   }
}
