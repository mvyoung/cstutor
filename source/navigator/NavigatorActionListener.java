package navigator;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.CSTutor;
import ui.MenuListener;

/**
* This is the class which is used when you click on  viewing the navigator.
* It allows this to happen when you click on a particulat menu option names
* "navigator".
*
* @author Raymond Pederson
*/
public class NavigatorActionListener extends MenuListener {
   public NavigatorActionListener() {
      super();
   }

   private void updatePanel() {
      CardLayout cl = (CardLayout) cs.cards.getLayout();
      cl.show(cs.cards, CSTutor.NAVIGATOR);
   }
   
   @Override
   public void actionPerformed(ActionEvent arg0) {
      System.out.println("Navigator clicked");
      updatePanel();
   }
}
