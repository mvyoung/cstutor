package navigator;

import javax.swing.tree.DefaultMutableTreeNode;

import course.Course;
import course.Lesson;
import course.Page;
import course.Topic;

/**
* This is the class which extends a {@link DefaultMutableTreeNode} that gives 
* extra fields to store more information
*
* @author Raymond Pederson
*/
public class NavigatorNode extends DefaultMutableTreeNode {
   
   /**
    * The depth of the node
    */
   private int depth;
   
   /**
    * The id of the node
    */
   private long id = -1;
   
   /**
    * Tells if it is an add node or not. Used with NavigatorIcons
    */
   private boolean add;
   
   /**
    * Tell if it is a quiz or not
    */
   private boolean quiz;
   
   /**
    * depth is 0 for the top exp: "My Classes" and is 1 greater 
    * for each row in.
    * 
    * add determines if you clicked an "Add Classes Icon"
    */
   public NavigatorNode(Object userObject, int d, boolean a) {
      super(userObject);
      depth = d;
      add = a;
      quiz = false;
   }
   
   public NavigatorNode(Object userObject, int d, boolean a, boolean q) {
      super(userObject);
      depth = d;
      add = a;
      quiz = q;
   }
   
   /**
    * A getter for the the depth.
    * @return returns the depth.
    */
   public int getDepth() {
      return depth;
   }
   
   /**
    * A getter if it is a add node.
    * @return returns if it is an add node or not.
    */
   public boolean getAddBool() {
      return add;
   }
   
   /**
    * A getter if it is a quiz.
    * @return returns if it is a quiz or not.
    */
   public boolean getQuiz() {
      return quiz;
   }
   
   /**
    * A getter for the ID.
    * @return returns the ID.
    */
   public long getID() {
      return id;
   }
   
   /**
    * Sets the ID.
    * @param identify the new ID.
    */
   public void setID(long identify) {
      id = identify;
   }
   
   /**
    * Was used when adding things to the navigator. Currently not
    * used because directly using the db class has replaced its model.
    */
   public void navAdd() {
      Course testCourse = new Course();
      Topic testTopic = new Topic();
      Lesson testLesson = new Lesson();
      
      switch (depth) {
         case 0:  
                  break;
         case 1:  
                  break;
         case 2:  testCourse.addTopic(null);
                  break;
         case 3:  testTopic.addLesson(null);      
                  break;
         case 4:  if (!quiz) {
                     testLesson.addPage(null);
                  }
                  break;
         default: 
                  break;
      }
   }
   
   /**
    * Was used when removing things to the navigator. Currently not
    * used because directly using the db class has replaced its model.
    */
   public void navRemove() {
      Course testCourse = new Course();
      Topic testTopic = new Topic();
      Lesson testLesson = new Lesson();
      
      switch (depth) {
         case 0:  
                  break;
         case 1:  
                  break;
         case 2:  testCourse.removeTopic(testTopic);
                  break;
         case 3:  testTopic.removeLesson(testLesson);      
                  break;
         case 4:  if (!quiz) {
             		  testLesson.addPage(null);
                  }
                  break;
         default: 
                  break;
      }
   }
   
   public boolean addNode() {
      if (add) {
         //navAdd();
         return true;
      }
      return false;
   }
   
   public boolean removeNode() {
      if (!add) {
         //navRemove();
         return true;
      }
      return false;
   }
   
   private boolean pageOrQuiz() {
      boolean ret;
      if (quiz) 
         ret = true;
      else
         ret = false;
      return ret;
   }
}
