package navigator;

import java.awt.CardLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.tree.TreePath;

import lesson.LessonPanel;
import main.CSTutor;
import quiz.Quiz;
import roadmap.RoadmapPanel;
import student.TutorialViewPanel;
import course.CodeSegment;
import course.Exercise;
import course.Page;
import course.TextSegment;

/**
* This class extends {@link MouseAdapter} to allow someone to click add course/
* topic/lesson/exercise/quiz which redraws the navigator as well as storing the
* info in the CSTutor.db which is used in the db package.
*
* @author Raymond Pederson
*/
public class NavigatorMouseListener extends MouseAdapter {
   
   /**
    * The tree visible at the moment
    */
   private JTree visibleTree;
   
   private NavigatorPopUpDisplay display;
   
   public NavigatorMouseListener(JTree tree) {
      visibleTree = tree;
   }
   
   private long lastAddedPageID = -1;
   
   private long getLastAddedPageID() {
	   long temp = lastAddedPageID;
	   lastAddedPageID = -1;
	   return temp;
   }
   
   private void setLastAddedPageID(long id) {
	   lastAddedPageID = id;
   }
   
   /**
    * Adds a node under a particular path.
    * @param selectedNode is what type of node being added
    * @param selPath allows the program to know what id the new node 
    * will contain
    */
   public void add(NavigatorNode selectedNode, TreePath selPath) {
      switch (selectedNode.getDepth()) {
         case 1:  addCourse(selectedNode);
                  break;
         case 2:  addTopic(selectedNode, selPath);
                  break;
         case 3:  addLesson(selectedNode, selPath);
                  break;
         case 4:  if(!selectedNode.getQuiz()) {
                     addExercise(selectedNode, selPath);
                  }
                  else {
                     addQuiz(selectedNode, selPath);
                  }
                  break;
         default: break;
      }
   }
   
   /** 
    * Will be used as stub for adding to database
    */
   public Exercise makeExercise(long id) {
      return new Exercise(Page.EXERCISE,
            CSTutor.getInstance().getDBLesson().selectLesson(id),
            "EXERCISE " + (int)(Math.random() * 100),
            new TextSegment("EXERCISE"),
            new CodeSegment("I AM CODE"),
            CSTutor.getInstance().getDBPage().findHighestPageOrdinance(id));
   }
   
   /**
    * Will be used as stub for adding to database 
    */
   public Quiz makeQuiz(long id) {
      return new Quiz(Page.QUIZ,
            CSTutor.getInstance().getDBLesson().selectLesson(id),
            "QUIZ " + (int)(Math.random() * 100),
            new TextSegment("QUIZ"),
            new CodeSegment("I AM CODE"),
            CSTutor.getInstance().getDBPage().findHighestPageOrdinance(id));
   }
   
   /**
    * Adds a course under a particular node. This is stored into the CSTutor.db
    * @param selectedNode the node the course is to be under.
    */
   public void addCourse(NavigatorNode selectedNode) {
      display = new NavigatorPopUpDisplay("Enter the name of the course", 1);
   }
   
   /**
    * Adds a topic under a particular node. This is stored into the CSTutor.db
    * @param selectedNode the node the course is to be under.
    * @param selPath the path to get the particular course above it.
    * Useful for the db package.
    */
   public void addTopic(NavigatorNode selectedNode, TreePath selPath) {
      display = new NavigatorPopUpDisplay("Enter the name of the topic", 2);
      display.setPath(selPath);
   }
   
   /**
    * Adds a lesson under a particular node. This is stored into the CSTutor.db
    * @param selectedNode the node the course is to be under.
    * @param selPath the path to get the particular topic above it.
    * Useful for the db package.
    */
   public void addLesson(NavigatorNode selectedNode, TreePath selPath) {
      display = new NavigatorPopUpDisplay("Enter the name of the lesson", 3);
      display.setPath(selPath);
   }
   
   /**
    * Adds a exercise under a particular node. This is stored into the CSTutor.db
    * @param selectedNode the node the course is to be under.
    * @param selPath the path to get the particular lesson above it.
    * Useful for the db package.
    */
   public void addExercise(NavigatorNode selectedNode, TreePath selPath) {
      int count = selPath.getPathCount();
      long lessonID = ((NavigatorNode)selPath.
            getPathComponent(count - 2)).getID();
      long l = CSTutor.getInstance().getDBPage()
    		  .addPage(makeExercise(lessonID));
      setLastAddedPageID(l);
   }
   
   /**
    * Adds a quiz under a particular node. This is stored into the CSTutor.db
    * @param selectedNode the node the course is to be under.
    * @param selPath the path to get the particular lesson above it.
    * Useful for the db package.
    */
   public void addQuiz(NavigatorNode selectedNode, TreePath selPath) {
      int count = selPath.getPathCount();
      long lessonID = ((NavigatorNode)selPath.
            getPathComponent(count - 2)).getID();
      long l = CSTutor.getInstance().getDBPage().addPage(makeQuiz(lessonID));
      setLastAddedPageID(l);
   }
   
   public void mousePressed(MouseEvent e) {
      int selRow = visibleTree.getRowForLocation(e.getX(), e.getY());
      TreePath selPath = visibleTree.getPathForLocation(e.getX(), e.getY());
      if (selRow != -1) { 
         if (SwingUtilities.isLeftMouseButton(e)) {
            if (e.getClickCount() == 2) {
               NavigatorNode selectedNode =
                     (NavigatorNode)(selPath.getLastPathComponent());
               boolean addSuccess = selectedNode.addNode();
               if (addSuccess) {
                  add(selectedNode, selPath);
                  //refresh();
                  if (selectedNode.getDepth() == 4) {
                	  long addedID = getLastAddedPageID();
                	  System.out.println("ADDED ID: " + addedID);
                	  LessonPanel lessons = (LessonPanel) CSTutor.getInstance()
                			  .panels.get(CSTutor.LESSONS);
                	  if (addedID != -1) {
                		  lessons.setPageById(addedID);
                	  }
                	  else {
                		  System.err.println("ID WAS -1, SOMETHING WRONG");
                	  }
                     CardLayout cl = 
                           (CardLayout) CSTutor.getInstance().cards.getLayout();
                     cl.show(CSTutor.getInstance().cards, 
                           CSTutor.getInstance().LESSONS);
                     refresh();
                  }
               }
               if (!addSuccess && selectedNode.getDepth() == 4
                     && !selectedNode.getQuiz()) {
                  JPanel card = CSTutor.getInstance().cards;
                  TutorialViewPanel tutorial = (TutorialViewPanel) CSTutor.getInstance().panels.get(CSTutor.TUTORIAL);
                  tutorial.setPageById(selectedNode.getID());
                  tutorial.maybeRebuild();
                  CardLayout cl = ((CardLayout)card.getLayout());
                  cl.show(card, CSTutor.TUTORIAL);
               }
               /* else if (addSuccess && selectedNode.getDepth() == 4
                     && !selectedNode.getQuiz()) {
                  JPanel card = CSTutor.getInstance().cards;
                  CardLayout cl = ((CardLayout)card.getLayout());
                  cl.show(card, CSTutor.LESSONS);
               } */
            }
         }
         else {
            if (e.getClickCount() == 1) {
               NavigatorNode selectedNode =
                     (NavigatorNode)(selPath.getLastPathComponent());
               NavigatorPopUpMenu menu = new NavigatorPopUpMenu(selectedNode);
               menu.show(e.getComponent(), e.getX(), e.getY());
            }
         }
      }
   }
   
   /**
    * Refreshes the navigator
    */
   private void refresh() {
      ((NavigatorPanel)CSTutor.getInstance().
            panels.get(CSTutor.NAVIGATOR)).refreshNavigator();
      ((RoadmapPanel)CSTutor.getInstance().
            panels.get(CSTutor.ROADMAP)).setNavPanel();
   }
}   
