package navigator;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.CSTutor;
import roadmap.RoadmapPanel;
import course.Course;
import course.Lesson;
import course.Topic;

public class EditNavActionListener implements ActionListener {

   private NavigatorNode nodeToEdit;
   private NavigatorEditDisplay display;

   
   public EditNavActionListener(NavigatorNode selectedNode) {
      super();
      nodeToEdit = selectedNode;
   }

   @Override
   public void actionPerformed(ActionEvent arg0) {
      edit(nodeToEdit);
      refresh();
   }
   
   public void edit(NavigatorNode selectedNode) {
      switch (selectedNode.getDepth()) {
         case 1:  editCourse(selectedNode);
                  break;
         case 2:  editTopic(selectedNode);
                  break;
         case 3:  editLesson(selectedNode);
                  break;
         case 4:  if(!selectedNode.getQuiz()) {
                  }
                  else {
                  }
                  break;
         default: break;
      }
   }
   
   public void editCourse(NavigatorNode selectedNode) {
      Course c =
            CSTutor.getInstance().getDBCourse().
               selectCourse(selectedNode.getID());
      display = new NavigatorEditDisplay
            ("Enter the new name of the course", 1, c.courseID);
   }
   
   public void editTopic(NavigatorNode selectedNode) {
      Topic t =
            CSTutor.getInstance().getDBTopic().
               selectTopic(selectedNode.getID());
      display = new NavigatorEditDisplay
            ("Enter the new name of the topic", 2, t.topicID);
   }
   
   public void editLesson(NavigatorNode selectedNode) {
      Lesson l =
            CSTutor.getInstance().getDBLesson().
               selectLesson(selectedNode.getID());
      display = new NavigatorEditDisplay
            ("Enter the new name of the lesson", 3, l.lessonID);
   }
   
   /**
    * Refreshes the Navigator panel and RoadMap panel when called
    */
   private void refresh() {
      ((NavigatorPanel)CSTutor.getInstance().
            panels.get(CSTutor.NAVIGATOR)).refreshNavigator();
      ((RoadmapPanel)CSTutor.getInstance().
            panels.get(CSTutor.ROADMAP)).setNavPanel();
   }
}
