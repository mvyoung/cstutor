package navigator;

import java.awt.Component;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
* This is the class which is used when you with Navigator to allow custom 
* png icons in the nav panel.
*
* @author Raymond Pederson
*/
public class NavigatorIcons extends DefaultTreeCellRenderer {

   private Icon testIcon = new ImageIcon(getClass().
         getResource("/images/Folder-Add.png"));
   
   public NavigatorIcons() {
      Icon closedIcon = new ImageIcon(getClass().
            getResource("/images/Folder-Closed.png"));
      Icon openIcon = new ImageIcon(getClass().
            getResource("/images/Folder-Open.png"));
      
      setClosedIcon(closedIcon);
      setOpenIcon(openIcon);
   }
   
   @Override
   public Component getTreeCellRendererComponent(JTree tree,
           Object value, boolean selected, boolean expanded,
           boolean isLeaf, int row, boolean focused) {
      Component c = super.getTreeCellRendererComponent(tree, value,
            selected, expanded, isLeaf, row, focused);
      if (((NavigatorNode) value).getAddBool())
         setIcon(testIcon);
      return c;
   }
}
