package navigator;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.tree.TreePath;

import main.CSTutor;
import roadmap.RoadmapPanel;
import course.Course;
import course.Lesson;
import course.Topic;

public class NavigatorEditDisplay extends JPanel implements ActionListener {

   public JFrame frame;
   public JTextField name;
   public String savedName = "";
   
   private int nt;
   private long id;
   
   public NavigatorEditDisplay(String str, int nodeType, long identify) {
      frame = new JFrame("Frame");
      nt = nodeType;
      id = identify;
      
      JLabel stringLabel = new JLabel(str);
      JButton done = new JButton("Submit");
      
      ActionListener exit = new ActionListener() {
         @Override
         public void actionPerformed(ActionEvent e) {
            frame.setVisible(false);
            switch (nt) {
               case 1:  setCourse();
                        break;
               case 2:  setTopic();
                        break;
               case 3:  setLesson();
                        break;
               default: break;
            }
            refresh();
         }
      };
      
      done.addActionListener(exit);
      name = new JTextField(5);
      name.addActionListener(this);
      name.addKeyListener(
               new KeyListener() {
                  @Override
                  public void keyTyped(KeyEvent e){}
                  @Override
                  public void keyPressed(KeyEvent e) {}
                  @Override
                  public void keyReleased(KeyEvent e) {
                     savedName = name.getText();
                  }
               }
            );
      
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      frame.getContentPane().add(stringLabel, BorderLayout.NORTH);
      frame.getContentPane().add(name, BorderLayout.CENTER);
      frame.getContentPane().add(done, BorderLayout.SOUTH);

      frame.setSize(250, 100);
      frame.setVisible(true);
   }
   
   @Override
   public void actionPerformed(ActionEvent arg0) {
      savedName = name.getText();
   }
   
   public void setCourse() {
      Course c = CSTutor.getInstance().getDBCourse().selectCourse(id);
      c.courseName = savedName;
      CSTutor.getInstance().getDBCourse().editCourse(c);
   }
   
   public void setTopic() {
      Topic t = CSTutor.getInstance().getDBTopic().selectTopic(id);
      t.topicName = savedName;
      CSTutor.getInstance().getDBTopic().editTopic(t);
   }
   
   public void setLesson() {
      Lesson l = CSTutor.getInstance().getDBLesson().selectLesson(id);
      l.lessonName = savedName;
      CSTutor.getInstance().getDBLesson().editLesson(l);
   }
   
   /**
    * Refreshes the navigator
    */
   private void refresh() {
      ((NavigatorPanel)CSTutor.getInstance().
            panels.get(CSTutor.NAVIGATOR)).refreshNavigator();
      ((RoadmapPanel)CSTutor.getInstance().
            panels.get(CSTutor.ROADMAP)).setNavPanel();
   }
}
