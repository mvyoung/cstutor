package navigator;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

/**
* This class extends {@link JPopupMenu} and allows you to delete things 
* from the navigator and databse by right clicking.
*
* @author Raymond Pederson
*/
public class NavigatorPopUpMenu extends JPopupMenu {

   private JMenuItem edit;
   private JMenuItem delete;
   
   
   public NavigatorPopUpMenu(NavigatorNode selectedNode) {
      edit = new JMenuItem("edit");
      delete = new JMenuItem("delete");

      delete.addActionListener(new DeleteNavActionListener(selectedNode));
      edit.addActionListener(new EditNavActionListener(selectedNode));

      
      add(edit);
      add(delete);
   }
}
