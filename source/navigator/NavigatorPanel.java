package navigator;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import main.CSTutor;
import roadmap.RoadmapActionListener;
import course.Course;
import course.Lesson;
import course.Page;
import course.Topic;

/**
* This class extends {@link JPanel} and implements {@link TreeSelectionListener}
* which gives a limited filing system. Mostly the gui information.
*
* @author Raymond Pederson
*/
public class NavigatorPanel extends JPanel implements TreeSelectionListener {

   /**
    * The JTree of the navigation.
    */
   private JTree tree;
   
   /**
    * The JPanel used holding the navigator.
    */
   private JScrollPane treeView;
   
   /**
    * The commonly used splitPane
    */
   private JSplitPane splitPane;
   
   /**
    * The head of the navigator
    */
   private NavigatorNode top;
   
   /**
    * The BottomLeft Panel used to get to RoadMap quickly
    */
   private JPanel bottomleft;
   
   //private ArrayList <Integer> paths;
   
   /**
    * The constructor for the navigator panel
    */
   public NavigatorPanel() {
      super(new BorderLayout());
      initTreeView();
      
      JSplitPane left = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
      bottomleft = new JPanel();
      
      JButton roadmap = new JButton("Roadmap");
      roadmap.addActionListener(new RoadmapActionListener());
      bottomleft.add(roadmap);

      bottomleft.setPreferredSize(new Dimension(10, 100));
      left.setTopComponent(treeView);
      left.setBottomComponent(bottomleft);
      
      splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

      splitPane.setLeftComponent(left);
      splitPane.setRightComponent(new JPanel());
            
      add(splitPane, BorderLayout.WEST);
   }
   
   /**
    * Refreshes the Navigator by retrieving new info from CSTutor.db
    */
   public void refreshNavigator() {
      remove(splitPane);
      //initTreeView();
      NavigatorNode newTop = updateTree();
      JTree newTree = new JTree(newTop);
      top = newTop;
            
      tree.setModel(newTree.getModel());
      //initPathState();
      for (int i = 0; i < tree.getRowCount(); i++) {
         //System.out.println("R " + i);
         tree.expandRow(i);
      }

      splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
      JSplitPane left = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
      treeView = new JScrollPane(tree, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

      bottomleft.setPreferredSize(new Dimension(10, 100));

      left.setTopComponent(treeView);
      left.setBottomComponent(bottomleft);

      splitPane.setLeftComponent(left);
      splitPane.setRightComponent(new JPanel());
      
      add(splitPane, BorderLayout.WEST);
      revalidate();
   }
   
   /**
    * Initates the Navigator based on the Tree
    * @return The panel to be used containing the tree
    */
   public JScrollPane initTreeView() {
      top = updateTree();
      
      tree = new JTree(top);
      
      tree.getSelectionModel().setSelectionMode
              (TreeSelectionModel.SINGLE_TREE_SELECTION);
      tree.addTreeSelectionListener(this);
      tree.addMouseListener(new NavigatorMouseListener(tree));
      changeIcons();
         
      treeView = new JScrollPane(tree, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, 
            JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
      treeView.setPreferredSize(new Dimension(200, 600)); 
      treeView.setMaximumSize(new Dimension(200, 600));
      
      return treeView;
   }
   
   @Override
   public void valueChanged(TreeSelectionEvent e) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode)
            tree.getLastSelectedPathComponent();

      if (node == null) return;
   }
   
   /** 
    * Old code used to make the tree. Only keeping it here in case people
    * Want to see a sample navigator.
    */
   public NavigatorNode MakeTree() {
      NavigatorNode top = 
            new NavigatorNode("My Classes", 0, false);
      NavigatorNode courseNames = 
            new NavigatorNode("Course Name", 1, false);
      NavigatorNode topicNames = 
            new NavigatorNode("Topic Name", 2, false);
      NavigatorNode lessonNames = 
            new NavigatorNode("Lesson Name", 3, false);
      
      NavigatorNode addCourse = 
            new NavigatorNode("Add Course", 1, true);
      NavigatorNode addTopic = 
            new NavigatorNode("Add Topic", 2, true);
      NavigatorNode addLesson = 
            new NavigatorNode("Add Lesson", 3, true);
      NavigatorNode addPage = 
            new NavigatorNode("Add Page", 4, true, false);
      NavigatorNode addQuiz = 
            new NavigatorNode("Add Quiz", 4, true, true);
      
      NavigatorNode sampleLesson = 
            new NavigatorNode("Defining Functions", 4, false, false);
      NavigatorNode sampleQuiz = 
            new NavigatorNode("Quiz #1", 4, false, true);
            
      top.add(courseNames);
      top.add(addCourse);
      courseNames.add(topicNames);
      courseNames.add(addTopic);
      topicNames.add(lessonNames);
      topicNames.add(addLesson);
      lessonNames.add(sampleLesson);
      lessonNames.add(sampleQuiz);
      lessonNames.add(addPage);
      lessonNames.add(addQuiz);
      
      return top;
   }
   
   /**
    * Updates the tree beginning with the head.
    * @return The head of the tree.
    */
   public NavigatorNode updateTree() {
      top = 
            new NavigatorNode("My Classes", 0, false);   
      addCoursesToNode(top);
      NavigatorNode addCourse = 
            new NavigatorNode("Add Course", 1, true);
            
      top.add(addCourse);
      
      return top;
   }
   
   /**
    * Adds Courses to the head
    * @param top is the head
    */
   public void addCoursesToNode(NavigatorNode top) {
      ArrayList<Course> fromDB = CSTutor.getInstance().
            getDBCourse().selectAllCourses();
      
      for (int i = 0; i < fromDB.size(); i++) {
         NavigatorNode temp = 
               new NavigatorNode(fromDB.get(i).courseName, 1, false);
         temp.setID(fromDB.get(i).courseID);
         addTopicsToCourses(temp, fromDB.get(i).courseID);
         temp.add(new NavigatorNode("Add Topic", 2, true));
         top.add(temp);
      }
   }
   
   /**
    * Adds Topics to a Course
    * @param c is the Course
    * @param id is the id to be given to the node
    */
   public void addTopicsToCourses(NavigatorNode c, long id) {
      ArrayList<Topic> fromDB = CSTutor.getInstance().
            getDBTopic().selectAllTopics();
      
      for (int i = 0; i < fromDB.size(); i++) {
         if (fromDB.get(i).parentCourse.courseID == id) {
            NavigatorNode temp =
                  new NavigatorNode(fromDB.get(i).topicName, 2, false);
            temp.setID(fromDB.get(i).topicID);
            addLessonsToTopics(temp, fromDB.get(i).topicID);
            temp.add(new NavigatorNode("Add Lesson", 3, true));
            c.add(temp);
         }
      }
   }
   
   /**
    * Adds Lessons to a Topic
    * @param c is the Topic
    * @param id is the id to be given to the node
    */
   public void addLessonsToTopics(NavigatorNode c, long id) {
      ArrayList<Lesson> fromDB = CSTutor.getInstance().
            getDBLesson().selectAllLessons();
      
      for (int i = 0; i < fromDB.size(); i++) {
         if (fromDB.get(i).parentTopic.topicID == id) {
            NavigatorNode temp = 
                  new NavigatorNode(fromDB.get(i).lessonName, 3, false);
            temp.setID(fromDB.get(i).lessonID);
            addPagesToLessons(temp, fromDB.get(i).lessonID);
            temp.add(new NavigatorNode("Add Page", 4, true, false));
            temp.add(new NavigatorNode("Add Quiz", 4, true, true));
            c.add(temp);
         }
      }
   }
   
   /**
    * Adds Page to a Lesson
    * @param c is the Lesson
    * @param id is the id to be given to the node
    */
   public void addPagesToLessons(NavigatorNode c, long id) {
      ArrayList<Page> fromDB = CSTutor.getInstance().
            getDBPage().selectAllPages();
      
      for (int i = 0; i < fromDB.size(); i++) {
         if (fromDB.get(i).parentLesson.lessonID == id) {
            NavigatorNode temp;
            //if (fromDB.get(i).type.equals(Page.EXERCISE)) {
               temp = 
                     new NavigatorNode(fromDB.get(i).pageName, 4, false, false);
            /*}
            else {
               temp = new NavigatorNode(fromDB.get(i).pageName, 4, false, true);
            }*/
            temp.setID(fromDB.get(i).pageID);
            c.add(temp);
         }
      }
   }
   
   /**
    * Changes the Icons in init
    */
   public void changeIcons() {
      tree.setCellRenderer(new NavigatorIcons());
   }
   
   /**
    * A getter for the Navigator Pane
    * @return The pane containing the navigator.
    */
   public JScrollPane getTreeView() {
      return treeView;
   }
}
