package course;
import java.util.ArrayList;

import main.CSTutor;

/**
* This is the class below the Course class of the course package which contains
* a collection of lessons. This is the second level of basically a filing 
* system which contains a set of IDs to identify different lessons under it.
*
* @author Raymond Pederson
* @author Sajjad Mirza
* @author Steven Tan
*/
public class Topic {
   /**
   * Contains the lessons
   */
   public ArrayList<Lesson> lessons;
   
   /**
   * The name of the Topic
   */
   public String topicName;
   
   /**
   * The ID of the Topic which is unique
   */
   public long topicID;
   
   /**
    * The parent course of the topic.
    * <p>
    * Generated when a new topic is added.
    */
   public Course parentCourse;
   
   /**
    * The order of the topic in the specified course.
    * <p>
    * Used for ordering in roadmap/navigator.
    */
   public int topicOrder;
   
   /**
    * On construction, the topic will have a unique ID (Current auto-incremented in the DB)
    */
   public Topic(){
      
   }
   
   /**
    * On construction, the topic will have a unique ID (Current auto-incremented in the DB)
    */
   public Topic(Course pc, String tn, int to){
      parentCourse = pc;
      topicName = tn;
      topicOrder = to;
   }
   
   /**
    * On construction, the topic will have a unique ID (Current auto-incremented in the DB)
    */
   public Topic(long ti, Course pc, String tn, int to){
      topicID = ti;
      parentCourse = pc;
      topicName = tn;
      topicOrder = to;
   }
   
   /**
   * Adds a Lesson into the lessons Set
   * <p>
   * @param lesson The Lesson to be added
   */
   public void addLesson(Lesson lesson) {
      
	  ArrayList <Lesson> test;
	  test = CSTutor.getInstance().getDBLesson().selectAllLessons();
	  test.add(lesson);
	  for(int i = 0; i < test.size(); i++) {
	     test.get(i).lessonOrder = i + 1;
	     CSTutor.getInstance().getDBLesson().addEditLesson(test.get(i));
      }
   }
   /**
    * Adds a lesson into the lesson set
    * <p>
    * @param lesson the lesson to be added
    * @param where where it is to be added
    */
   public void addLesson(Lesson lesson, int where)
   {
      System.out.println("Course.addLesson added at" + where);
      
	  ArrayList <Lesson> test;
	  test = CSTutor.getInstance().getDBLesson().selectAllLessons();
	  test.add(where, lesson);
	  for(int i = 0; i < test.size(); i++) {
	     test.get(i).lessonOrder = i + 1;
	     CSTutor.getInstance().getDBLesson().addEditLesson(test.get(i));
      }
   }
   
   
   /**
    * Removes a lesson in the lesson Set
    * <p>
    * @param The lesson to be removed
    */
   public void removeLesson(Lesson lesson) {
      System.out.println("Course.removeLesson by lesson");
      
	  ArrayList <Lesson> test;
	  test = CSTutor.getInstance().getDBLesson().selectAllLessons();
	  test.remove(lesson);
	  for(int i = 0; i < test.size(); i++) {
	     test.get(i).lessonOrder = i + 1;
	     CSTutor.getInstance().getDBLesson().addEditLesson(test.get(i));
      }
   }
    
   /**
    * Removes a lesson in the lessons Set by lessonID
    * <p>
    * @param The lessonID to be removed
    */
   public void removeLesson(long lessonId) {
      System.out.println("Course.removeLesson by lessonID");
      

      CSTutor.getInstance().getDBLesson().removeLesson(lessonId);
      
	  ArrayList <Lesson> test;
	  test = CSTutor.getInstance().getDBLesson().selectAllLessons();
	  
	  for(int i = 0; i < test.size(); i++) {
	     test.get(i).lessonOrder = i + 1;
	     CSTutor.getInstance().getDBLesson().addEditLesson(test.get(i));
      }
   }
     
    /**
     * Removes a lesson in the lessons Set by lessonName
     * <p>
     * @param The lessonName to be removed
     */
   public void removeLesson(String name) {
      System.out.println("Course.removeLesson by lessonName");
      
	  ArrayList <Lesson> test;
	  test = CSTutor.getInstance().getDBLesson().selectAllLessons();        
      for(int i = 0; i< lessons.size(); i++) {
      	 if(lessons.get(i).lessonName == name) {
      		lessons.remove(lessons.get(i));
      		CSTutor.getInstance().getDBLesson().removeLesson(lessons.get(i));
      	 }
      }
         
	  for(int i = 0; i < test.size(); i++) {
		 test.get(i).lessonOrder = i + 1;
		 CSTutor.getInstance().getDBLesson().addEditLesson(test.get(i));
	  }
   }
   
   /**
   * Returns the lessons set
   * <p>
   * @return The lessons set of this class
   */
   public ArrayList<Lesson> getLessons() {
	  this.lessons = CSTutor.getInstance().getDBLesson().selectAllLessons();
      return lessons;
   }
   
   /**
   * Returns a specific Lesson based on the param
   * <p>
   * This is used so you may access a specific Lesson for the edit method
   * <p>
   * @param The lessonID of the Lesson to be recieved
   * @return The actual Lesson which has the param as lessonID
   */
   public Lesson getLesson(long lID) {
	   Lesson l = CSTutor.getInstance().getDBLesson().selectLesson(lID);
	   return l;
   }
  
   /**
    * Gets the parent course.
    * <p>
    * This is used to retrieve the course the topic is under.
    * <p>
    * @return The parent Course
    */
   
   public Course getCourse(){
	   return parentCourse;
   }
}
