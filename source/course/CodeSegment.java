package course;

/**
 * Represents the portion of a page that contains the student's source code,
 * as well as any predefined code.
 *
 * <p>The format of the code segment is determined by XML. As an example:
 *
 * <code>
 * <xml>
 *    # Make this function return the square of its argument
 *    # You may not use the '*' (multiplication) operator.
 *    def f(a):
 *       return <editable lines=1 columns=10 />
 * </xml>
 * </code>
 *
 * <p>In this example, the normal text is predefined code which the student will
 * be unable to edit. The <editable/> tag is rendered into a text box where the
 * student can input code. The instructor is not required to provide predefined
 * code; the entire XML document may be a single <editable/> element.
 *
 * @author Raymond Pederson
 * @author Sajjad Mirza
 */
public class CodeSegment {

   /**
    * The raw XML string defining the code segment.
    */
   public String xml;
   
   public CodeSegment() {
      
   }
   
   public CodeSegment(String s) {
      xml = s;
   }

   /**
    * Replaces the current XML string with the supplied XML string.
    */      
   public void replaceText(String xml) {
      this.xml = xml;
   }

   /**
    * Deletes the current XML string entirely.
    */
   public void clearText() {
      xml = "";
   }

   /**
    * Retrieves the XML string.
    */
   public String getText() {
      return xml;
   }
}
