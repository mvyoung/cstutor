package course;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import main.CSTutor;
import db.PageDatabase;

/**
* This class represents either an exercise or a quiz and its different panels.
* <p>
* It includes a ID to easily identify pages, a TextSegment for the lesson
* information, and a CodeSegment for the code to be used as an example or
* a quiz on the right pane. There is no variable for the output or the compiler
* because they work the same because of the scope of this project.
*
* @author Raymond Pederson
* @author Sajjad Mirza
* @author Steven Tan
*/
public class Page {
   public static final String EXERCISE = "exercise", QUIZ = "quiz";
   /**
   * The ID of the Page which is unique
   */
   public long pageID;
   
   /**
    * The ID of the Page which is unique
    */
//    public String type;
   
   /**
    * The parent course of the page.
    * <p>
    * Generated when a new page is added.
    */
   public Lesson parentLesson;
   
   /**
   * The name of the Lesson
   */
   public String pageName;
   
   /**
   * The left pane text which either tells documentation or the instructions
   */
   public TextSegment text;
   
   /**
   * The right pane text which either has code with editable spaces or multiple
   * choice spaces.
   * <p>
   * This is used to test examples or used for grading quizzes if the page is
   * a Quiz.
   */
   public CodeSegment code;
   
   /**
    * The order of the page in each lesson.
    * <p>
    * Used in roadmap/navigator for ordering when display.
    */
   public int pageOrder;
   
   /**
    * Is this page a quiz or just a lesson
    * <p>
    * true for lesson, false for quiz
    */
   public String pageType;
   
   /**
    * On construction, the page will have a unique ID (Current auto-incremented in the DB)
    */
   public Page(){
      
   }
   
   /**
    * On construction, the page will have a unique ID (Current auto-incremented in the DB)
    */
   public Page(String ty, Lesson pl, String pn, TextSegment t, CodeSegment c, int po, String type){
      type = ty;
      parentLesson = pl;
      pageName = pn;
      text = t;
      code = c;
      pageOrder = po;
      pageType = type;
   }
   
   /**
    * On construction, the page will have a unique ID (Current auto-incremented in the DB)
    */
   public Page(long pi, String ty, Lesson pl, String pn, TextSegment t, CodeSegment c, int po, String type){
      pageID = pi;
      type = ty;
      parentLesson = pl;
      pageName = pn;
      text = t;
      code = c;
      pageOrder = po;
      pageType = type;
   }
   
   /**
   * Runs the results of code. Used to produce output in stdout.
   */
   /*@
   requires
   (code != null && code.getText() != null && !code.getText().equals(""))
   ;
   ensures
   (old(code).equals(code))
   ;
   @*/
   public List<String> run() throws IOException {
	   return null;
   }
   
   /**
    * Sends the string of code to TextSegment
    * @param s The string to be sent
    */
   
   public void setLessonContent(String s) {
	   text.html = s;
   }
   
   /**
    * Sends the string of code to CodeSegment
    * @param s The string to be sent
    */
   
   public void setQuizContent(String s) {
	   code.xml = s;
   }
   
   /**
    * Sets the page type (Lesson or Code)
    * @param type The type of the string
    */
   public void setPageType(String s) {
	   pageType = s;
   }
   
   public String getPageType(){
	   return pageType;
   }
   
   /**
    * Goes to the next page. Goes back to home if the end of the list.
    * <p>
    * @return The next Page
    */
   public Page next(PageDatabase pdb) {
	   List<Page> pages = pdb.selectAllPages();
	   List<Page> nextCandidates = new ArrayList<>();
	   
	   for (Page p : pages) {
		   if (p.parentLesson.lessonID == this.parentLesson.lessonID) {
			   if (p.pageOrder > this.pageOrder) {
				   nextCandidates.add(p);
			   }
		   }
	   }

	   Comparator<Page> c = new Comparator<Page>() {
		   @Override
		   public int compare(Page a, Page b) {
			   return a.pageOrder - b.pageOrder;
		   }
	   };
	   
	   Collections.sort(nextCandidates, c);
	   
	   if (nextCandidates.size() > 0) {
		   return nextCandidates.get(0);
	   }
	   
	   return null;
   }
   
   /**
    * Goes to the previous page. Invalid if the beginning of the list.
    * <p>
    * @return The previous Page
    */
   public Page prev(PageDatabase pdb) {
	   List<Page> pages = pdb.selectAllPages();
	   List<Page> prevCandidates = new ArrayList<>();
	   
	   for (Page p : pages) {
		   if (p.parentLesson.lessonID == this.parentLesson.lessonID) {
			   if (p.pageOrder < this.pageOrder) {
				   prevCandidates.add(p);
			   }
		   }
	   }

	   Comparator<Page> c = new Comparator<Page>() {
		   @Override
		   public int compare(Page a, Page b) {
			   return a.pageOrder - b.pageOrder;
		   }
	   };
	   
	   Collections.sort(prevCandidates, c);
	   
	   if (prevCandidates.size() > 0) {
		   return prevCandidates.get(prevCandidates.size() - 1);
	   }
	   
	   return null;
   }
      
}
