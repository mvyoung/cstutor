package course;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import main.CSTutor;
import python.PythonRuntime;
import python.PythonService;

/**
* This class represents an exercise which is a non-graded page with a lesson on
* the right pane.
* <p>
* This class overrides a few methods because of the differences between a
* Exercise or Quiz page.
*
* @author Raymond Pederson
* @author Sajjad Mirza
*/
public class Exercise extends Page {

   public Exercise() {
      super();
      // TODO Auto-generated constructor stub
   }

   public Exercise(long pi, String ty, Lesson pl, String pn, TextSegment t,
         CodeSegment c, int po) {
      super(pi, ty, pl, pn, t, c, po, "exercise");
      // TODO Auto-generated constructor stub
   }

   public Exercise(String ty, Lesson pl, String pn, TextSegment t,
         CodeSegment c, int po) {
      super(ty, pl, pn, t, c, po, "exercise");
      // TODO Auto-generated constructor stub
   }
   
   /**
   * Runs the results of code. Used to produce output in stdout.
   */
   public List<String> run() throws IOException {
      System.out.println("Exercise.run");
      
      String pythonCode = code.xml;
      
      PythonService service = CSTutor.getInstance().getPythonService();
      PythonRuntime python = service.getPythonInterpreter();
      
      python.startWriteStream();
      
      python.send(pythonCode);
      
      python.endWriteStream();
      
      ArrayList<String> output = new ArrayList<String>(); 
      
      String line = null;
      while ((line = python.readLine()) != null) {
         output.add(line + '\n');
      }
      
      return output;
   }
}
