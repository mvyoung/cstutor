package course;
import java.util.ArrayList;
import java.util.Set;

import main.CSTutor;

import course.Page;

/**
* This is the class below the Topic class of the course package which contains
* a collection of pages. This is the third level of basically a filing 
* system which contains a set of IDs to identify different pages under it.
*
* @author Raymond Pederson
* @author Sajjad Mirza
* @author Steven Tan
*/
public class Lesson {
   /**
   * Contains the pages
   */
   public ArrayList<Page> pages;
   
   /**
   * The name of the Lesson
   */
   public String lessonName;
   
   /**
   * The ID of the Lesson which is unique
   */
   public long lessonID;
   
   /**
    * The parent course of the lesson.
    * <p>
    * Generated when a new lesson is added.
    */
   public Topic parentTopic;
   
   /**
    * The order of the lesson in each topic.
    * <p>
    * Used in roadmap/navigator for ordering when display.
    */
   public int lessonOrder;
   
   /**
    * The current page
    */
   int currentPageNum;
   
   /**
    * On construction, the lesson will have a unique ID (Current auto-incremented in the DB)
    */
   public Lesson(){
      
   }
   
   /**
    * On construction, the lesson will have a unique ID (Current auto-incremented in the DB)
    */
   public Lesson(Topic pt, String ln, int lo){
      parentTopic = pt;
      lessonName = ln;
      lessonOrder = lo;
   }
   
   /*@
    * Requires
    * li == unique
    * pt != null
    * ln != null
    * lo == in order
    * ;
    * ensure
    * lesson == valid
    */
   /**
    * On construction, the lesson will have a unique ID (Current auto-incremented in the DB)
    */
   public Lesson(long li, Topic pt, String ln, int lo){
      lessonID = li;
      parentTopic = pt;
      lessonName = ln;
      lessonOrder = lo;
   }
   
   /**
   * Adds a Page into the pages Set to the end
   * <p>
   * @param The Page to be added
   */  
   public void addPage(Page page) {
      ArrayList <Page> test;
      test = CSTutor.getInstance().getDBPage().selectAllPages();
      test.add(page);
      for(int i = 0; i < test.size(); i++) {
         test.get(i).pageOrder = i + 1;
         CSTutor.getInstance().getDBPage().addEditPage(test.get(i));
      }
   }
   
   /**
    * Returns Page
    * @param p the page to be returned
    * @return p
    */
   public Page addPage1(Page p){
	   return p;
   }
   
   
   /**
   * Adds a Page into the pages Set in "where"
   * <p>
   * @param where Where the page is to be added
   * @param page The Page to be added
   */  
   public void addPage(Page page, int where) {
	  System.out.println("Page added at " + where);
	  
      ArrayList <Page> test;
      test = CSTutor.getInstance().getDBPage().selectAllPages();
      test.add(where, page);
      for(int i = 0; i < test.size(); i++) {
         test.get(i).pageOrder = i + 1;
         CSTutor.getInstance().getDBPage().addEditPage(test.get(i));
      }
   }
   /*@
    * requires
    * page != null
    * ;
    * ensure 
    * set(lesson(page)) == null
    * set(lesson).pageOrder == increment in order.  
    */
   /**
    * Removes a page in the pages Set
    * <p>
    * @param The page to be removed
    */
   public void removePage(Page p) {
      System.out.println("remove Page by Page");
      
      ArrayList <Page> test;
      test = CSTutor.getInstance().getDBPage().selectAllPages();
      test.remove(p);
      for(int i = 0; i < test.size(); i++) {
         test.get(i).pageOrder = i + 1;
         CSTutor.getInstance().getDBPage().addEditPage(test.get(i));
      }
   }
   /*@
    * requires
    * pageId == valid
    * ;
    * ensure 
    * set(lesson(page(pageId))) == null
    * set(lesson).pageOrder == increment in order.  
    */
   /**
    * Removes a page in the pages Set by pageID
    * <p>
    * @param The page to be removed
    */
   public void removePage(long pageId) {
      System.out.println("Course.removePage by PageID");
      
      CSTutor.getInstance().getDBPage().removePage(pageId);
      ArrayList <Page> test;
      test = CSTutor.getInstance().getDBPage().selectAllPages();

      for(int i = 0; i < test.size(); i++) {
         test.get(i).pageOrder = i + 1;
         CSTutor.getInstance().getDBPage().addEditPage(test.get(i));
      }
   }
   /*@
    * requires
    * name != null && set(lesson) !- null
    * ;
    * ensure
    * set(lesson(page(name))) == null
    * set(lesson).pageOrder == increment in order.  
    */
    /**
     * Removes a Page in the Page Set by pageName
     * <p>
     * @param The page to be removed
     */
   public void removePage(String name) {
      System.out.println("Lesson.removePage");
      ArrayList <Page> test;
      test = CSTutor.getInstance().getDBPage().selectAllPages();
      
      for(int i = 0; i< pages.size(); i++) {
      	 if(pages.get(i).pageName == name) {
      		pages.remove(pages.get(i));
      		CSTutor.getInstance().getDBPage().removePage(pages.get(i));
      	 }
      }

      for(int i = 0; i < test.size(); i++) {
         test.get(i).pageOrder = i + 1;
         CSTutor.getInstance().getDBPage().addEditPage(test.get(i));
      }
   }
   
   /*@
    * requires
    * (set(page) != null
    * ;
    * ensures
    * set(page) returned
    */
   /**
   * Returns the pages set
   * <p>
   * @return The pages set of this class
   */
   public ArrayList<Page>getPages() {
	  this.pages = CSTutor.getInstance().getDBPage().selectAllPages();
      return pages;
   }
   /*@
    * requires
    * (page != null)
    * ;
    * ensures
    * this(page) returned
    */
   /**
   * Returns a specific Page based on the param
   * <p>
   * This is used so you may access a specific Page for the edit method
   * <p>
   * @param The pageID of the Page to be recieved
   * @return The actual Page which has the param as pageID
   */
   public Page getPage(long pageID) {
	  Page p = CSTutor.getInstance().getDBPage().selectPage(pageID);
	  return p;
   }
   /**
    * Makes a new page and returns it
    * @param pageID pageID needed
    * @return p the page returned
    */
   public Page getPageAll(){
	   Page p = new Page();
	   return p;
   }
   /**
    * Switches the given page with it's next page.
    * @param p the page to be switched
    */
   
   public void switchNextPage(Page p){
   
      System.out.println("Switched Page with next");
      ArrayList <Page> test;
      test = CSTutor.getInstance().getDBPage().selectAllPages();
      Page tempA = null;
      Page tempB = null;
      int aInx = 0;
      int bInx = 0;
      boolean found = false;

      for (int i = 0; i < test.size(); i++) {
         System.out.println("BEFORE: " + test.get(i).pageID);
      }

      for(int i = 0; i < test.size(); i++) {
    	  if(test.get(i).parentLesson.lessonID == p.parentLesson.lessonID) {
    		  if(p.pageID == test.get(i).pageID){
    			 tempA = test.get(i);
    			 aInx = i;
    			 found = true;
    		  } else if(found){
              System.out.println("PAGEID: " + test.get(i).pageID);
    			  tempB = test.get(i);
    			  bInx = i;
              break;
    		  }
    	  }
      }
      test.set(aInx, tempB);
      test.set(bInx, tempA);

      for(int i = 0; i < test.size(); i++) {
         System.out.println("AFTER: " + test.get(i).pageID);
         test.get(i).pageOrder = i + 1;
          CSTutor.getInstance().getDBPage().addEditPage(test.get(i));
      }
      
	  
   }
   /**
    * Switches the given page with it's pre page.
    * @param p the page to be switched
    */
   
   public void switchPrePage(Page p){
	   
      System.out.println("Switched Page with prev");
      ArrayList <Page> test;
      test = CSTutor.getInstance().getDBPage().selectAllPages();
	  Page tempA = null;
      Page tempB = null;
      int aInx = 0;
      int bInx = 0;
      boolean found = false;

      for(int i = 0; i < test.size(); i++) {
    	  if(test.get(i).parentLesson.lessonID == p.parentLesson.lessonID) {
    		  if(p.pageID == test.get(i).pageID){
    			 tempA = test.get(i);
    			 aInx = i;
    			 break;
    		  } else {
    			  tempB = test.get(i);
    			  bInx = i;
    		  }
    	  }
      }
      test.set(aInx, tempB);
      test.set(bInx, tempA);

      for(int i = 0; i < test.size(); i++) {
         System.out.println("AFTER: " + test.get(i).pageID);
         test.get(i).pageOrder = i + 1;
         CSTutor.getInstance().getDBPage().addEditPage(test.get(i));
      }
      
   }
   
   /**
    * Goes to the next page. Goes back to home if the end of the list.
    * <p>
    * @param The next Page
    */
    /*@
    requires
    (next != null)
    ;
    ensures
    (old(next) == next && old(prev) == prev && \result == next)
    ;
    @*/
    public Page next(){
       currentPageNum++;
 	   return pages.get(currentPageNum);
    }
    
    /**
     * Goes to the previous page. Invalid if the beginning of the list.
     * <p>
     * @param The previous Page
     */
     /*@
     requires
     (prev != null)
     ;
     ensures
     (old(prev) == prev && old(next) == prev && \result == prev)
     ;
     @*/
    public Page prev() {
 	   currentPageNum--;
       return pages.get(currentPageNum);
    }
}
