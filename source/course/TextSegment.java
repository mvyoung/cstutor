package course;

/**
 * The portion of the Page which contains the documentation and instructions for
 * the student.
 *
 * <p>The documentation is simply HTML.
 *	   String s = "";
 * @author Raymond Pederson
 * @author Sajjad Mirza
 * @author Steven Tan
 */
public class TextSegment {

   /**
    * The raw HTML data for the document.
    */
   public String html;
   
   public TextSegment() {
      
   }
   
   public TextSegment(String s) {
      html = s;
   }

   /**
    * Replaces the current HTML string with the supplied HTML string.
    */      
   public void replaceText(String newHtml){
	  html = newHtml;
   }

   /**
    * Deletes the current HTML string entirely.
    */
   public void clearText(){
	   html = "";
   }

   /**
    * Retrieves the HTML string.
    */
   public String getText() {
	   return html;
   }
}
