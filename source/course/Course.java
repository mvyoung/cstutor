package course;
import java.util.ArrayList;
import main.*;

/**
* This is the top-level class of the course package which contains a 
* collection of topics. This is pretty much acting as the top of a limited
* filing system which contains a set of IDs to identify different topics
* under it.
*
* @author Raymond Pederson
* @author Sajjad Mirza
* @author Steven Tan
*/
public class Course {
   /**
   * Contains the topics under the course Class
   */
   public ArrayList<Topic> topics;
   
   /**
   * The name of the Course
   */
   public String courseName;
   
   /**
   * The ID of the Course which is unique
   */
   public long courseID;
   
   /**
    * The Course number order;
    */
   public int courseOrder;
   
   /**
    * On construction, the course will have a unique ID (Current auto-incremented in the DB)
    */
   public Course(){
	   
   }
   
   /**
    * On construction, the course will have a unique ID (Current auto-incremented in the DB)
    */
   public Course(String cn, int co){
      courseName = cn;
      courseOrder = co;
   }
   
   /**
    * On construction, the course will have a unique ID (Current auto-incremented in the DB)
    */
   public Course(long cid, String cn, int co){
      courseID = cid;
      courseName = cn;
      courseOrder = co;
   }
   
   /**
    * Adds a topic into the end of the topics Set
    * <p>
    * @param The topic to be added
    */
   
   public void addTopic(Topic topic) {
      System.out.println("Course.addTopic");
      ArrayList <Topic> test;
      test = CSTutor.getInstance().getDBTopic().selectAllTopics();
      test.add(topic);
      
      for(int i = 0; i < test.size(); i++) {
    	  test.get(i).topicOrder = i + 1;
    	  CSTutor.getInstance().getDBTopic().addEditTopic(test.get(i));
      }
   }
   
   /**
    * Adds a topic into the topics Set
    * <p>
    * @param The topic to be added
    * @param Where it is added to
    */
   
   
   public void addTopic(Topic topic, int where)
   {
      System.out.println("Course.addTopic added at" + where);
      
	  //Reorder the topic number
	  ArrayList <Topic> test;
	  test = CSTutor.getInstance().getDBTopic().selectAllTopics();
	  test.add(where, topic);
	  for(int i = 0; i < test.size(); i++) {
	     test.get(i).topicOrder = i + 1;
	     CSTutor.getInstance().getDBTopic().addEditTopic(test.get(i));
      }
   }
   
   /**
   * Removes a topic in the topics Set
   * <p>
   * @param The topic to be removed
   */
   public void removeTopic(Topic topic) {
      System.out.println("Course.removeTopic by topic");
      
	  //Reorder the topic number
	  ArrayList <Topic> test;
	  test = CSTutor.getInstance().getDBTopic().selectAllTopics();
	  test.remove(topic);
	  for(int i = 0; i < test.size(); i++) {
	     test.get(i).topicOrder = i + 1;
	     CSTutor.getInstance().getDBTopic().addEditTopic(test.get(i));
      }
   }
   
   /**
    * Removes a topic in the topics Set by topicID
    * <p>
    * @param The topic to be removed
    */
    public void removeTopic(long topicId) {
      System.out.println("Course.removeTopic by topicID");
      
      CSTutor.getInstance().getDBTopic().removeTopic(topicId);
       
	  //Reorder the topic number
	  ArrayList <Topic> test;
	  test = CSTutor.getInstance().getDBTopic().selectAllTopics();
	  for(int i = 0; i < test.size(); i++) {
	     test.get(i).topicOrder = i + 1;
	     CSTutor.getInstance().getDBTopic().addEditTopic(test.get(i));
      }
    }
    
    /**
     * Removes a topic in the topics Set by topicName
     * <p>
     * @param The topic to be removed
     */
    public void removeTopic(String name) {
      System.out.println("Course.removeTopic by topicName");
       
  	  ArrayList <Topic> test;
	  test = CSTutor.getInstance().getDBTopic().selectAllTopics();
      for(int i = 0; i< topics.size(); i++) {
     	 if(topics.get(i).topicName == name) {
     		topics.remove(topics.get(i));
     		CSTutor.getInstance().getDBTopic().removeTopic(topics.get(i));
     	 }
      }
        
	  for(int i = 0; i < test.size(); i++) {
	     test.get(i).topicOrder = i + 1;
	     CSTutor.getInstance().getDBTopic().addEditTopic(test.get(i));
      }
     }
   
   /**
   * Replaces the topic in the topic set with the param topic
   * <p>
   * This basically allows you to edit a topic, finds the old topic in the 
   * topic set, and it replaced it.
   * <p>
   * @param The new topic which replaces the old topic
   */
   //Legacy code.
   /*public void edit(Topic topic) {
      
   }*/
   
   /**
   * Returns the topics set
   * <p>
   * @return The topic set of the Course object
   */
   public ArrayList<Topic> getTopics() {
	  this.topics = CSTutor.getInstance().getDBTopic().selectAllTopics();
      return topics;
   }
   
   /**
   * Returns a specific topic based on the param
   * <p>
   * This is used so you may access a specific Topic for the edit method
   * <p>
   * @param The topicID of the topic to be recieved
   * @return The actual Topic which has the param as a topicID
   */
   public Topic getTopic(long topicID) {
	   Topic t;
	   t = CSTutor.getInstance().getDBTopic().selectTopic(topicID);
	   return t;
   }
}
