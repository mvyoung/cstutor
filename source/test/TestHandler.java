package test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import test.course.*;

public class TestHandler {

   public TestHandler() {
      System.out.println("Loading Tests...");
   }

   public void printResults(String test, Result result) {

      System.out.println(test + " Tests...");
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
      System.out.println("Passed: " + result.wasSuccessful());
   }
   
   public void runTests() {
      /**
       * Result variable for the results of tests
       */
      Result result;
      
      System.out.println("Starting Tests...");
      
      /**
       * Begin Assessment Tests
       */
      System.out.println("--------------- Assessment Tests ---------------"); 
      result = JUnitCore.runClasses(AssessmentConstructorTest.class);
      printResults("Constructor",result);
      result = JUnitCore.runClasses(AssessmentHandlerTest.class);
      printResults("Handler",result);
      /**
       * End Assessment Tests
       */
      
       /**
       * Begin Communication Tests
       */
      System.out.println("--------------- Communication Tests ---------------");
      result = JUnitCore.runClasses(ChatTest.class);
      printResults("Chat",result);
      result = JUnitCore.runClasses(OnlineListTest.class);
      printResults("OnlineList",result);
      /**
       * End Communication Tests
       */
      
      /**
       * Begin CourseDatabase Tests
       */
      System.out.println("--------------- CourseDatabase Tests ---------------");
      result = JUnitCore.runClasses(CourseDatabaseTest.class);
      printResults("",result);
      result = JUnitCore.runClasses(TopicDatabaseTest.class);
      printResults("",result);
      /**
       * End CourseDatabase Tests
       */
      
      /**
       * Begin Course Test
       */
      System.out.println("--------------- Course Tests ---------------");
      result = JUnitCore.runClasses(LessonTest.class);
      printResults("",result);
      result = JUnitCore.runClasses(QuizTest.class);
      printResults("",result);
      
      result = JUnitCore.runClasses(ExerciseTest.class);
      printResults("", result);
      /**
       * End Course Test
       */
       
      /**
       * Begin Roadmap Test
       */
      System.out.println("--------------- Roadmap Tests ---------------");
//      result = JUnitCore.runClasses(CourseSelectorActionListenerTest.class);
//      printResults("",result);
      result = JUnitCore.runClasses(RoadmapTest.class);
      printResults("",result);
//      result = JUnitCore.runClasses(RoadmapNodeTest.class);
//      printResults("",result);
      /**
       * End Roadmap Test
       */    

      System.out.println("Ending Tests...");
   }

}
