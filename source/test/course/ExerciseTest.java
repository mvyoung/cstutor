package test.course;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import course.CodeSegment;
import course.Exercise;
import course.Lesson;
import course.Page;
import db.PageDatabase;

/**
 * Tests the Exercise class.
 * @author smirza
 *
 */
public class ExerciseTest extends Exercise {
	
	class MockPageDatabase extends PageDatabase {
		public ArrayList<Page> data = new ArrayList<>();
		private int count;

		@Override
		public long addPage(Page p) {
			int ordinal = findHighestPageOrdinance(0) + 1;
			p.pageID = ++count;
			p.pageOrder = ordinal;
			data.add(p);
			return p.pageID;
		}

		@Override
		public ArrayList<Page> selectAllPages() {
			return data;
		}

		@Override
		public int findHighestPageOrdinance(long id) {
			int ordinal = 0;
			
			for (Page p : data) {
				if (p.pageOrder > ordinal) {
					ordinal = p.pageOrder;
				}
			}
			
			return ordinal;
		}
		
		
		
	}
	
	
	@Before
	public void setUp() {
	}
	
	@After
	public void tearDown() {
	}

	/**
	 * Tests the next() method.
	 * 
	 * Test	
	 * Case      Input             Output                    Remarks
	 * ===============================================================
	 * 1         two pages         pages in insertion order  standard case
	 * 2         one page          no next page              when there is no next
	 */

	@Test
	public void testNext() {
		testNext1();
		testNext2();
	}
	
	void testNext1() {
		MockPageDatabase pdb = new MockPageDatabase();
		Exercise e = new Exercise();
		
		
		Exercise nextExercise = new Exercise();
		e.parentLesson = new Lesson();
		e.parentLesson.lessonID = 1;
		nextExercise.parentLesson = e.parentLesson;
		
		pdb.addPage(e);
		long id = pdb.addPage(nextExercise);
		
		Page next = e.next(pdb);
		
		assertEquals(id, next.pageID);
	}
	
	void testNext2() {
		MockPageDatabase pdb = new MockPageDatabase();
		Exercise e = new Exercise();
		
		pdb.addPage(e);
		
		
		Page next = e.next(pdb);
		
		assertNull(next);
	}
	
	/**
	 * Tests the prev() method.
	 * 
	 *  Test	
	 * Case      Input             Output                    Remarks
	 * ===============================================================
	 * 1         two pages         pages in insertion order  standard case
	 * 2         one page          no prev page              when there is no prev
	 */
	@Test
	public void testPrev() {
		testPrev1();
		testPrev2();
		
	}
	
	void testPrev1() {
		MockPageDatabase pdb = new MockPageDatabase();
		Exercise e  = new Exercise();
		
		Exercise prevExercise = new Exercise();
		
		e.parentLesson = new Lesson();
		e.parentLesson.lessonID = 1;
		prevExercise.parentLesson = e.parentLesson;
		
		pdb.addPage(prevExercise);
		long id = pdb.addPage(e);
		
		Page prev = e.prev(pdb);
		assertEquals(id, prev.pageID);
	}
	
	void testPrev2() {
		MockPageDatabase pdb = new MockPageDatabase();
		Exercise e = new Exercise();
		
		pdb.addPage(e);
		
		
		Page prev = e.prev(pdb);
		
		assertNull(prev);
	}
	
	/**
	 * Tests the run method.
	 * 
	 * Test	
	 * Case      Input             Output                    Remarks
	 * ===============================================================
	 * 1         valid python      hello world               nothing should be bad
	 */
	@Test
	public void testRun() {
		testRun1();
	}
	
	void testRun1() {
		Exercise e = new Exercise();
		CodeSegment cs = new CodeSegment();
		cs.xml = "print 'hello world'\n";
		e.code = cs;
		
		try {
			List<String> output = e.run();
			assertEquals(1, output.size());
			
			String line = output.get(0);
			assertEquals("hello world", line);
		}
		catch (IOException ex) {
			fail("Running the Python code resulted in an exception");
		}
	}
}
