package test;

import main.CSTutor;
import org.junit.Test;
import static org.junit.Assert.*;

import course.*;

import java.lang.Exception;
import java.util.ArrayList;

public class LessonTest {

   /*-*
    * LessonTest for Lesson Class constructor and a few of it's method
    * 
    * ---Lesson---
    * Phase 1 : Constructor
    * Phase 2 : Remove Method
    * Phase 3 : GetAll method 
    *
    */
	
	/**
	 * Unit test the constructor by building serveral Pages to added to a single lesson parent
	 * Test case valid params 
	 * 
	 *  Test
     *  Case    Input            Output                 Remarks
     * ====================================================================
     *   1      t, "CSC101" ,1  (Lesson has been created)
	 */
	
	@Test
	public void testLesson(){
		Topic t = new Topic();
		Lesson l = new Lesson(t, "CSC101", 1);
		assertTrue(l.lessonName != null);
		assertTrue(l.lessonOrder != 0);
		assertTrue(l.parentTopic != null);
		
	}
	/**
	 * UnitTest Add method and make sure it is the correct order
	 * 
	 *  Test
     *  Case    Input            Output                 Remarks
     * ====================================================================
     *   1      two              Added to Page List
     *   2      one              Added to Page List
     *
	 */
	
	@Test
	public void testAdd(){
		Lesson parentlesson = new Lesson();
		ArrayList<Page> pages = new ArrayList<Page>();
		TextSegment t = new TextSegment();
		CodeSegment c = new CodeSegment();
		Page one = new Page(1 ,"excercise",parentlesson,"PageOne",t,c, 1, "excercise");
		Page two = new Page(2 ,"quiz", parentlesson,"PageTwo",t,c, 2,"quiz");
		Page three = null;
		
		Page test = parentlesson.addPage1(one);
		Page test2 = parentlesson.addPage1(two);
		
		assertTrue(test.pageID == 1);
		assertTrue(test.pageName == "PageOne");
		assertTrue(test.pageType == "excercise");
		assertTrue(test.pageOrder == 1);
		assertTrue(test2.pageID == 2);
		assertTrue(test2.pageName == "PageTwo");
		assertTrue(test2.pageType == "quiz");
		assertTrue(test2.pageOrder == 2);
		
	}
	
	/**
	 * Unit test the getAll method
	 * Make sure it returns a valid arrayList
	 * 
	 *  Test
     *  Case    Input            Output                 Remarks
     * ====================================================================
     *   1                       Correct                Called on a nonemptyList;
     *   2                       Correct                Called on an empty arrayList
	 */
	
	@Test
	public void testGetAll(){
		Lesson parentlesson = new Lesson();
		Lesson emptylesson = new Lesson();
		TextSegment t = new TextSegment();
		CodeSegment c = new CodeSegment();
		Page one = new Page(1 ,"excercise",parentlesson,"PageOne",t,c, 1, "excercise");
		Page two = new Page(2 ,"quiz", parentlesson,"PageTwo",t,c, 2,"quiz");
		
		Page p = parentlesson.getPageAll();
		
		assertTrue(p != null);
	}
    
}
