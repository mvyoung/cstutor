package test.course;

import course.*;

import org.junit.Test;
import quiz.*;

import static org.junit.Assert.*;

/****
 * Test the three constructor for Quiz Class
 *
 * @author Steven Tan (stan02@calpoly.edu)
 */

public class QuizTest {

   /**
    * Unit test first constructor by creating the Quiz.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      					Correct				   Successfully created a quiz
    *
    */
   @Test
   public void testQuiz(){
      Quiz q = new Quiz();
      assertTrue(q != null);
   }
   
   /**
    * Unit test 2nd constructor by creating the Quiz.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      					Correct				   Successfully created a quiz
    *
    */
   @Test
   public void testQuiz1(){
	   Lesson parentlesson = new Lesson();
	   TextSegment t = new TextSegment();
	   CodeSegment c = new CodeSegment();
	   Quiz q = new Quiz(1, "Quiz",parentlesson, "safe", t, c, 1);
	   
	   assertTrue(q.parentLesson != null);
	   assertTrue(q.code != null);
	   assertTrue(q.text != null);
	   assertTrue(q.pageName != null);
	   assertTrue(q.pageType != null);
   }

   /**
    * Unit test third constructor by creating the Quiz.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      					Correct				   Successfully created a quiz
    *
    */
   @Test
   public void testQuiz2(){
	   Lesson parentlesson = new Lesson();
	   TextSegment t = new TextSegment();
	   CodeSegment c = new CodeSegment();
	   Quiz q = new Quiz("Quiz",parentlesson, "safe", t, c, 1);
	   
	   assertTrue(q.parentLesson != null);
	   assertTrue(q.code != null);
	   assertTrue(q.text != null);
	   assertTrue(q.pageName != null);
	   assertTrue(q.pageType != null);
   }
}
