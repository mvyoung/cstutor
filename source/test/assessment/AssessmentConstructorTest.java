package test;

import assessment.*;
import org.junit.Test;
import static org.junit.Assert.*;

/****
 *
 * Class AssessmentHandler is the testing class to test the interaction with the assessment database. <br>
 * It implements the following module test plan: <br>
 *                                                                         <ul>
 *                                                                      <p><li>
 *     Phase 1: Unit test the constructor.
 *                                                                     
 */
public class AssessmentConstructorTest {
   
   /**
     * Unit test the constructor by building an Assessment object.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      null             assessment object  Overloaded Constructor  
     *   2      null             assessment object  Default Constructor
     */
   
   @Test
   public void testConstructor() {
      Assessment a = new Assessment("Joey","Classes","00:36","Import",new Integer(1),new Float(82.3));
      assertTrue(a.userName.equals("Joey"));   
      assertTrue(a.tutorial.equals("Classes"));   
      assertTrue(a.time.equals("00:36"));   
      assertTrue(a.quiz.equals("Import"));   
      assertTrue(a.attempts.equals(new Integer(1)));   
      assertTrue(a.score.equals(new Float(82.3)));   
      
      Assessment b = new Assessment();
      b.userName = "Tony";
      b.tutorial = "Inheritance";
      b.time = "00:45";
      b.quiz = "Shapes";
      b.attempts = new Integer(3);
      b.score = new Float(89.7);
      assertTrue(b.userName.equals("Tony"));   
      assertTrue(b.tutorial.equals("Inheritance"));   
      assertTrue(b.time.equals("00:45"));   
      assertTrue(b.quiz.equals("Shapes"));   
      assertTrue(b.attempts.equals(new Integer(3)));   
      assertTrue(b.score.equals(new Float(89.7)));
      assertTrue(true);   
   }
}
