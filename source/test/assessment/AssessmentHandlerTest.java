package test;

import org.junit.Test;
import static org.junit.Assert.*;

import assessment.*;
import java.util.ArrayList;

/****
 *
 * Class AssessmentHandler is the testing class to test the interaction with the assessment database. <br>
 * It implements the following module test plan: <br>
 *                                                                         <ul>
 *                                                                      <p><li>
 *     Phase 1: Unit test the simple access methods getAssessments and getAssessment.
 *                                                                      <p><li>
 *     Phase 2: Unit test the constructive method addAssessment
 *                                                                        </ul>
 */
public class AssessmentHandlerTest {
   
   AssessmentHandler handler = new AssessmentHandler();
   
   /**
     * Unit test the get by name method by building an Assessment object, adding it to the database,
     * then making sure that the assessment that was added is in the return list.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output                     Remarks
     * ====================================================================
     *   1      1 Assessment     Proper get by name done    Depends on add working correctly
     *   2      1 Assessment     All fields have name       Depends on add working correctly
     */
   @Test
   public void testGetByName() {
      Assessment a = new Assessment("Joey","Variables","00:57","Integers",new Integer(4),new Float(87.9));
      ArrayList<Assessment> assessmentList;
      handler.addAssessment(a);
      assessmentList = handler.getAssessmentByName("Joey");
      assertTrue(assessmentList.contains(a));
      
      for (int i = 0; i < assessmentList.size(); i++) {
         assertTrue(assessmentList.get(i).userName.equals("Joey"));
      }
   }
   
   /**
     * Unit test the get by tutorial method by building an Assessment object, adding it to the database,
     * then making sure that the assessment that was added is in the return list.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      1 Assessment     Proper get by tutorial done    Depends on add working correctly
     *   2      1 Assessment     All fields have tutorial       Depends on add working correctly
     */
   @Test
   public void testGetByTutorial() {
      Assessment a = new Assessment("Joey","Variables","00:57","Integers",new Integer(4),new Float(87.9));
      ArrayList<Assessment> assessmentList;
      handler.addAssessment(a);
      assessmentList= handler.getAssessmentByTutorial("Variables");
      assertTrue(assessmentList.contains(a));
      
      for (int i = 0; i < assessmentList.size(); i++) {
         assertTrue(assessmentList.get(i).tutorial.equals("Variables"));
      }
   }
   
   /**
     * Unit test the get by quiz method by building an Assessment object, adding it to the database,
     * then making sure that the assessment that was added is in the return list.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      1 Assessment     Proper get by quiz done    Depends on add working correctly
     *   2      1 Assessment     All fields have quiz       Depends on add working correctly
     */
   @Test
   public void testGetByQuiz() {
      Assessment a = new Assessment("Joey","Variables","00:57","Integers",new Integer(4),new Float(87.9));
      ArrayList<Assessment> assessmentList;
      handler.addAssessment(a);
      assessmentList= handler.getAssessmentByQuiz("Integers");
      assertTrue(assessmentList.contains(a));
      
      for (int i = 0; i < assessmentList.size(); i++) {
         assertTrue(assessmentList.get(i).quiz.equals("Integers"));
      }
   }
   
   /**
     * Unit test the get all method by building three a Assessment objects, adding them to the database,
     * then making sure that all the assessments that were added are in the return list.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output                 	Remarks
     * ====================================================================
     *   1      3 Assessments    Proper get all done    Depends on add working correctly
     *
     */
   @Test
   public void testGetAll() {
      Assessment a = new Assessment("Joey","Loops","00:14","Basic Loop",new Integer(3),new Float(78.6));
      Assessment b = new Assessment("John","Conditionals","00:27","If",new Integer(5),new Float(89.2));
      Assessment c = new Assessment("Sara","Functions","00:43","Def",new Integer(2),new Float(94.3));
      ArrayList<Assessment> assessmentList;
      handler.addAssessment(a);
      handler.addAssessment(b);
      handler.addAssessment(c);
      assessmentList= handler.getAssessments();
      assertTrue(assessmentList.contains(a));
      assertTrue(assessmentList.contains(b));
      assertTrue(assessmentList.contains(c));
   }
   
   /**
     * Unit test the add method by building an Assessment object, adding it to the database,
     * then making sure that the assessment that was added is in the return list.
     *                                                                    <pre>
     *  Test
     *  Case    Input            Output             Remarks
     * ====================================================================
     *   1      1 Assessment     Proper add done    Depends on get working correctly
     *   2      null             Add not done       Checks the null
     */
   @Test
   public void testAdd() {
      Assessment a = new Assessment("Joey","Classes","00:36","Import",new Integer(1),new Float(82.3));
      ArrayList<Assessment> assessmentList;
      handler.addAssessment(a);
      assessmentList = handler.getAssessments();
      assertTrue(assessmentList.contains(a));
      
      Assessment b = new Assessment(null,null,null,null,null,null);
      int pre = handler.getAssessments().size();
      handler.addAssessment(b);
      int post = handler.getAssessments().size();
      assertTrue(pre == post);
   }
}
