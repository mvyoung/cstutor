package test;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import course.Course;
import db.CourseDatabase;
import db.DatabaseService;

/**
 * Phase 1: Initiating the dbservice
 * Phase 2: Initiating the database
 * Phase 3: Testing adding a course
 * Phase 4: Testing removing a course
 * Phase 5: Testing finding a course with the highest ordinance
 * @author Raymond Pederson
 *
 */
public class CourseDatabaseTest {

   private DatabaseService dbService = new DatabaseService("CSTutorTest");
   private CourseDatabase handler = new CourseDatabase(dbService.getConnect());
   
   /**
    * Test
    *   Case      Input         Output                 Remarks
    * ===============================================================
    *     1      Course            true       It was added properly
    *            With not-null
    *            values
    */
   @Test
   public void testAddEditCourse() {
      Course a = new Course("COURSE " + (int)(Math.random() * 100),
           handler.findHighestOrdinance());
      ArrayList<Course> courseList;
      a.courseID = handler.addEditCourse(a);
      
      a.courseName = "TEST";
      handler.addEditCourse(a);
      
      courseList = handler.selectAllCourses();
      
      for (int i = 0; i < courseList.size(); i++) {
            if (a.courseID == courseList.get(i).courseID &&
                a.courseName.equals(courseList.get(i).courseName) &&
                a.courseOrder == courseList.get(i).courseOrder) {
               
               handler.removeCourse(a.courseID);
               assertTrue(true);
               return;
            }
      }
      handler.removeCourse(a.courseID);
      assertTrue(false);
   }
   
   /**
    * Test
    *   Case      Input         Output                 Remarks
    * ===============================================================
    *     1      Course            true       It was removed from 
    *            With not-null                database properly
    *            values
    */
   @Test
   public void testRemoveCourse() {
      Course a = new Course("COURSE " + (int)(Math.random() * 100),
           handler.findHighestOrdinance());
      ArrayList<Course> courseList;
      handler.addEditCourse(a);
      handler.removeCourse(a);
      
      courseList= handler.selectAllCourses();
      assertTrue(!courseList.contains(a));
   }
   
   /**
    * Test
    *   Case      Input                  Output            Remarks
    * ========================================================================
    *     1    2 Courses added            true       It found the proper
    *          into the database                     highest ordinal
    *          With not-null
    *          values
    */
   @Test
   public void testFindHighestOrdinance() {
      Course a = new Course("COURSE " + (int)(Math.random() * 100), 0);
      Course b = new Course("COURSE " + (int)(Math.random() * 100), 1);
      int ordinal;
      handler.addEditCourse(a);
      handler.addEditCourse(b);

      ordinal = handler.findHighestOrdinance();
      
      handler.removeCourse(b);
      handler.removeCourse(a);
      assertTrue(ordinal == 1);
   }
}
