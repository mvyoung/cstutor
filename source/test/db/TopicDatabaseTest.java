package test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import course.Course;
import course.Topic;
import db.CourseDatabase;
import db.TopicDatabase;
import db.DatabaseService;

/**
 * Phase 1: Initiating the dbservice
 * Phase 2: Initiating the databases
 * Phase 3: Testing adding a topic
 * Phase 4: Testing removing a topic
 * Phase 5: Testing finding a topic with the highest ordinance
 * @author Raymond Pederson
 *
 */
public class TopicDatabaseTest {

   private DatabaseService dbService = new DatabaseService("CSTutorTest");
   private CourseDatabase handler = new CourseDatabase(dbService.getConnect());
   private TopicDatabase handlerT = new TopicDatabase(dbService.getConnect());
   
   /**
    * Test
    *   Case      Input                  Output            Remarks
    * =======================================================================
    *     1   Course and topic            true       It was added properly
    *         with not-null
    *         values
    */
   @Test
   public void testAddTopic() {
      Course a = new Course("COURSE " + (int)(Math.random() * 100),
           handler.findHighestOrdinance());
      a.courseID = handler.addEditCourse(a);
      
      Topic  t = new Topic(a, "TOPIC " + (int)(Math.random() * 100),
            handler.findHighestOrdinance());
      t.topicID = handlerT.addEditTopic(t);
      
      t.topicName = "TEST";
      handlerT.addEditTopic(t);
      
      ArrayList<Topic> topicList;
      topicList= handlerT.selectAllTopics(handler);
      
      for (int i = 0; i < topicList.size(); i++) {
         if (t.topicID == topicList.get(i).topicID &&
             t.topicName.equals(topicList.get(i).topicName) &&
             t.topicOrder == topicList.get(i).topicOrder) {
            
            handlerT.removeTopic(t.topicID);
            handler.removeCourse(a.courseID);
            assertTrue(true);
            return;
         }
      }
      handlerT.removeTopic(t.topicID);
      handler.removeCourse(a.courseID);
      assertTrue(false);
   }
   
   /**
    * Test
    *   Case      Input                   Output          Remarks
    * ======================================================================
    *     1    Course and topic            true       The topic  was removed 
    *          with not-null                          from the database properly
    *          values
    */
   @Test
   public void testRemoveTopic() {
      Course a = new Course("COURSE " + (int)(Math.random() * 100),
            handler.findHighestOrdinance());
      Topic t = new Topic(a, "TOPIC " + (int)(Math.random() * 100),
            handler.findHighestOrdinance());
      ArrayList<Topic> topicList;
      a.courseID = handler.addEditCourse(a);
      t.topicID = handlerT.addEditTopic(t);
      handlerT.removeTopic(t);
      handler.removeCourse(a);

      topicList= handlerT.selectAllTopics();
      assertTrue(!topicList.contains(t));
   }
   
   /**
    * Test
    *   Case      Input                  Output            Remarks
    * ========================================================================
    *     1    1 Course added            true       It found the proper
    *          into the database                    highest ordinal for the
    *          With not-null                        topics.
    *          values and two
    *          topics with non-null
    *          values
    */
   @Test
   public void testFindHighestOrdinance() {
      Course a = new Course("COURSE " + (int)(Math.random() * 100), 0);
      Topic t1 = new Topic(a, "TOPIC " + (int)(Math.random() * 100), 0);
      Topic t2 = new Topic(a, "TOPIC " + (int)(Math.random() * 100), 1);
      
      int ordinal;
      a.courseID = handler.addEditCourse(a);
      t1.topicID = handlerT.addEditTopic(t1);
      t2.topicID = handlerT.addEditTopic(t2);

      ordinal = handlerT.findHighestOrdinance();
      assertTrue(ordinal == 1);
      handlerT.removeTopic(t2);
      handlerT.removeTopic(t1);
      handler.removeCourse(a);
   }
}
