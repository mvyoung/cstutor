package test;

import authentication.User;
import communication.ChatMessage;
import org.junit.Test;
import static org.junit.Assert.*;

import authentication.*;
import communication.*;

import java.lang.Exception;
import java.util.ArrayList;
import java.util.Date;

/****
 * Test class for Chat
 *
 * The Chat class will be tested using the following procedure:
 * Phase 1: Test the constructor
 * Phase 2: Test the sendMessage method
 * Phase 3: Test the putMessage method
 * Phase 4: Repeat steps 1-3
 *
 * @author Aaron Pramana (apramana@calpoly.edu)
 */
public class ChatTest {

   public static ChatClient client = new ChatClient("", 55555);

   /**
    * Unit test the constructor by building several Chat objects.
    * Test edge cases, such as no users or an invalid room ID.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      users, 1         Creates the object     No exceptions
    *   2      users, 1         None                   currentUser included
    *   3      users, -1        Throws exception
    *   4      users, 1         Throws exception       No users added
    *
    */
   @Test
   public void testChat(){
      ArrayList<User> users = new ArrayList<User>();
      User bob = new User("Bob", 0, 0);
      User juan = new User("Juan", 1, 0);
      User jon = new User("Jon", 2, 0);
      users.add(bob);
      users.add(juan);
      users.add(jon);

      User curUser = bob;
      OnlineList list = new OnlineList(curUser, client);
      Chat chat = new Chat(users, -1, curUser, list);
      assertTrue(chat != null);

      assertTrue(chat.users.contains(curUser));

      assertTrue(users.size() != 0 && chat.id >= 0 && users.contains(curUser));

      try {
         chat = new Chat(null, 1, curUser, list);
         assertTrue(false);
      }
      catch (RuntimeException e){
         assertTrue(true);
      }
   }

   /**
    * Unit test the sendMessage method with various strings.
    * Test edge cases, such as no text.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      null             Throws exception
    *   2      ""               Throws exception
    *   3      "How are you?"   Correct
    *
    */
   @Test
   public void testSendMessage(){
      ArrayList<User> users = new ArrayList<User>();
      User bob = new User("Bob", 0, 0);
      User juan = new User("John", 1, 0);
      User jon = new User("Jon", 2, 0);
      users.add(bob);
      users.add(juan);
      users.add(jon);

      User curUser = bob;
      Chat chat = null;
      OnlineList list = new OnlineList(curUser, client);
      chat = new Chat(users, 1, curUser, list);

      try {
         chat.sendMessage(null);
         assertTrue(false);
      }
      catch(RuntimeException e){
         assertTrue(true);
      }

      try {
         chat.sendMessage("");
         assertTrue(false);
      }
      catch(RuntimeException e){
         assertTrue(true);
      }

      try {
         chat.sendMessage("How are you?");
         assertTrue(true);
      }
      catch(RuntimeException e){
         assertTrue(false);
      }
   }

   /**
    * Unit test the sendMessage method with various strings.
    * Test edge cases, such as no text.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      null             Throws exception
    *   2      null message params (Exception)
    *   2      (valid members)  Updates message        Correctly formatted
    *
    */
   @Test
   public void testPutMessage(){
      ArrayList<User> users = new ArrayList<User>();
      User bob = new User("Bob", 0, 0);
      User juan = new User("John", 1, 0);
      User jon = new User("Jon", 2, 0);
      users.add(bob);
      users.add(juan);
      users.add(jon);

      User curUser = bob;
      OnlineList list = new OnlineList(curUser, client);
      Chat chat = new Chat(users, 1, curUser, list);

      try{
         chat.putMessage(null);
         assertTrue(false);
      }
      catch (RuntimeException e){
         assertTrue(true);
      }

      ChatMessage cm = new ChatMessage(ChatMessage.MESSAGE, 1, null, null, null);

      try{
         chat.putMessage(cm);
         assertTrue(false);
      }
      catch (RuntimeException e){
         assertTrue(true);
      }

      cm = new ChatMessage(ChatMessage.MESSAGE, 1, curUser, "Hello world", new Date());

      try {
         chat.putMessage(cm);
         assertTrue(true);
      }
      catch (RuntimeException e){
         assertTrue(false);
      }

   }

   /**
    * Not a test. Ends the server connection
    */
   /*
   @Test
   public void testEnd(){
      client.stop();
      assertTrue(true);
   }*/
}
