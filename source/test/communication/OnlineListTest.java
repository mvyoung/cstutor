package test;

import authentication.User;
import communication.OnlineList;
import org.junit.Test;
import static org.junit.Assert.*;

import communication.*;

import java.util.ArrayList;
import java.util.Date;

/****
 * Test class for OnlineList
 *
 * The OnlineList class will be tested using the following procedure:
 * Phase 1: Test the constructor
 * Phase 2: Test the getter method
 * Phase 3: Test the startChat method
 * Phase 4: Repeat steps 1-3
 *
 * @author Aaron Pramana (apramana@calpoly.edu)
 */
public class OnlineListTest {

   public static ChatClient client = new ChatClient("", 55555);

   /**
    * Unit test the constructor by creating the Online Users list.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      (None)           (List has been created)
    *
    */
   @Test
   public void testOnlineList(){
      User curUser = new User("Bob", 0, 0);
      OnlineList ol = new OnlineList(curUser, client);
      assertTrue(ol.currentUser != null && ol.users != null && ol.activeChats != null);
   }

   /**
    * Unit test the Chat getter by feeding various ids.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      -2               (Exception)            Invalid ID.
    *   2      1                null                   OK. The Chat doesn't exist.
    *   3      0                Chat object            OK. The Chat exists.
    *   4      (None)           (Exception)            activeChats wasn't initialized properly.
    *
    */
   @Test
   public void testGetChatById(){
      User curUser = new User("Bob", 0, 0);
      OnlineList ol = new OnlineList(curUser, client);

      ArrayList<User> users = new ArrayList<User>();
      User bob = new User("Bob", 0, 0);
      User juan = new User("Juan", 1, 0);
      User jon = new User("Jon", 2, 0);
      users.add(bob);
      users.add(juan);
      users.add(jon);
      OnlineList list = new OnlineList(curUser, client);
      Chat chat = new Chat(users, 1, curUser, list);

      ol.activeChats = new ArrayList<Chat>();
      ol.activeChats.add(chat);

      assertTrue(ol.getChatById(1).equals(chat));

      chat = null;

      try {
         chat = ol.getChatById(-2);
         assertTrue(false);
      }
      catch(Exception e){
         assertTrue(true);
      }

      chat = ol.getChatById(2);
      assertTrue(chat == null);


      ol.activeChats = null;
      try {
         chat = ol.getChatById(2);
         assertTrue(false);
      }
      catch(Exception e){
         assertTrue(true);
      }


   }

   /**
    * Unit test startChat with different Users.
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      null            (Exception)            Invalid User.
    *   2      Valid User       None
    *
    */
   @Test
   public void testStartChat(){
      User curUser = new User("Bob", 0, 0);
      OnlineList ol = new OnlineList(curUser, client);

      try{
         ol.startChat(null);
         assertTrue(false);
      }
      catch (RuntimeException e){
         assertTrue(true);
      }

      User bob = new User("Bob", 0, 0);

      try{
         ol.startChat(bob);
         assertTrue(true);
      }
      catch (RuntimeException e){
         assertTrue(false);
      }
   }
}
