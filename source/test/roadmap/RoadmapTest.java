package test;

import java.lang.reflect.Array;
import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import org.junit.Test;
import static org.junit.Assert.*;

import course.Course;
import course.Lesson;
import course.Page;
import course.Topic;
import main.CSTutor;
import roadmap.*;
import roadmap.RoadmapNode;
import roadmap.RoadmapPanel;

/***
 * Test class for Roadmap
 *
 * The Roadmap class will be tested using the following procedure:
 * Phase1: Test the constructor
 * Phase2: Test the setView method
 * Phase3: Test the clearCourses method
 * Phase4: Test the cancelSelection method
 * Phase5: Test the changeSelectedCouse method
 * Phase6: Test the checkSelectedCourse method
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class RoadmapTest {
   /***
    * Unit test the constructor by checking that all the variables are created and start empty
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1                       Creates the object     All variables are not null and empty
    *   2      new Course       None                   Added course shows up in courseList
    *
    */
   @Test
   public void testRoadmap() {
      Roadmap r = new Roadmap(1);
      Course c = new Course();

      assertTrue(r.lookup != null && r.lookup.isEmpty());
      assertTrue(r.nodes != null && r.nodes.isEmpty());
      assertTrue(r.courseMap != null && r.courseMap.isEmpty());
      assertTrue(r.courseList != null && r.courseList.isEmpty());
      assertTrue(r.selectedCourse == null);

      r.courseList.add(c);
      assertTrue(r.courseList.get(0) == c);
   }

   /***
    * Unit test the setView method by calling it and checking to see if the RoadmapPanel is set and the selectedNode is null
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      new RoadmapPanel None                   selectedNode starts null and view is the same as the RoadmapPanel passed in
    *
    */
   public void testSetView() {
      Roadmap r = new Roadmap(1);
      RoadmapPanel panel = new RoadmapPanel(1);

      r.setView(panel);
      assertTrue(r.view == panel && r.selectedNode == null);
   }

   /***
    * Unit test the cancelSelection method by making sure selectedNode is null after
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      None             None                   selectedNode is null
    *
    */
   @Test
   public void testCancelSelection() {
      Roadmap r = new Roadmap(1);

      r.cancelSelection();
      assertTrue(r.selectedNode == null);
   }

   /***
    * Unit test the clearCourses method by making sure the arraylist and hashmap are empty after
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      None             None                   courseList and courseMap are both empty
    *
    */
   @Test
   public void testClearCourses() {
      Roadmap r = new Roadmap(1);

      r.clearCourses();
      assertTrue(r.courseList.isEmpty() && r.courseMap.isEmpty());
   }

   /***
    * Unit test the changeSelectedCourse method by putting in a new Course and checking to see if it is selected
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1      new Course       None                   selectedCourse is equal to the new course that was added
    *
    */
   @Test
   public void testChangeSelectedCourse() {
      Roadmap r = new Roadmap(1);
      r.courseMap.put("test", new Course("test", 0));

      r.changeSelectedCourse("test");
      assertTrue(r.selectedCourse.courseName.equals("test"));
   }

   /***
    * Unit test the checkSelectedCourse method by making sure no courses available is detected and
    *
    *  Test
    *  Case    Input            Output                 Remarks
    * ====================================================================
    *   1                       False                  Returns false because courseList is empty
    *   2      new Course       True                   Sets a default selectedCourse if it is null
    *
    */
   @Test
   public void testCheckSelectedCourse() {
      Roadmap r = new Roadmap(1);

      assertTrue(r.checkSelectedCourse() == false);
      r.courseList.add(new Course());
      assertTrue(r.checkSelectedCourse() == true);
   }
}