package menu;

import javax.swing.*;

public class File {
   private JMenu file;

   public File(JFrame frame, JPanel cards) {
      this.file = new JMenu("File");
      JMenuItem importClass = new JMenuItem("Import Class");
      JMenuItem importTutorial = new JMenuItem("Import Tutorial");
      file.add(importClass);
      file.add(importTutorial);
   }

   public JMenu getFile() {
      return this.file;
   }
}
