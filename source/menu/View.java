package menu;

import javax.swing.*;

import main.CSTutor;
import navigator.NavigatorActionListener;
import roadmap.RoadmapActionListener;
import communication.OnlineListActionListener;

public class View {
   private JMenu view;

   public View(JFrame frame, JPanel cards) {
      this.view = new JMenu("View");
      JMenuItem studentMode = new JMenuItem("Student Mode");
      JMenuItem instructorMode = new JMenuItem("Instructor Mode");
      JMenuItem navigator = new JMenuItem("Navigator");
      JMenuItem roadmap = new JMenuItem("Roadmap");
      JMenuItem chat = new JMenuItem("Chat");
      navigator.addActionListener(new NavigatorActionListener());
      roadmap.addActionListener(new RoadmapActionListener());
      chat.addActionListener(new OnlineListActionListener());
      view.add(studentMode);
      view.add(instructorMode);
      view.add(navigator);
      view.add(roadmap);
      view.add(chat);
   }

   public JMenu getView() {
      return this.view;
   }
}
