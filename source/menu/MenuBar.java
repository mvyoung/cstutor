package menu;

import assessment.AssessmentActionListener;
import communication.OnlineListActionListener;
import navigator.NavigatorActionListener;
import roadmap.RoadmapActionListener;
import main.CSTutor;

import javax.swing.*;
import java.awt.*;

public class MenuBar {
   private JMenuBar menubar;

   public MenuBar(CSTutor cs, JFrame frame, JPanel cards) {
      addMenus(cs, frame, cards);
   }

   public JMenuBar getMenuBar() {
      return this.menubar;
   }

   void addMenus(CSTutor cs, JFrame frame, JPanel cards) {
      this.menubar = new JMenuBar();

      JMenu file = new File(frame, cards).getFile();
      JMenu edit = new Edit(frame, cards).getEdit();
      JMenu view = new View(frame, cards).getView();
      JMenu tools = new Tools(cs, frame, cards).getTools();

      JMenu[] menus = { file, edit, view, tools };
      for (JMenu m : menus) {
         this.menubar.add(m);
      }
   }
}