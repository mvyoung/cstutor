package menu;

import javax.swing.*;

public class Edit {
   private JMenu edit;

   public Edit(JFrame frame, JPanel cards) {
      this.edit = new JMenu("Edit");
      JMenuItem preferences = new JMenuItem("Preferences");
      this.edit.add(preferences);
   }

   public JMenu getEdit() {
      return this.edit;
   }
}
