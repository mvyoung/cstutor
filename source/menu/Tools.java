package menu;

import assessment.AssessmentActionListener;
import lesson.LessonActionListener;
import main.CSTutor;

import javax.swing.*;

public class Tools {
   private JMenu tools;

   public Tools(CSTutor cs, JFrame frame, JPanel cards) {
      this.tools = new JMenu("Tools");
      JMenuItem createClass = new JMenuItem("Create Class");
      JMenuItem createQuiz = new JMenuItem("Create Quiz");
      JMenuItem createLesson = new JMenuItem("Create Lesson");
      JMenuItem viewAssessments = new JMenuItem("View Assessments");
      viewAssessments.addActionListener(new AssessmentActionListener());
      createLesson.addActionListener(new LessonActionListener());
      createQuiz.addActionListener(new LessonActionListener());
      tools.add(createClass);
      tools.add(createQuiz);
      tools.add(createLesson);
      tools.add(viewAssessments);
   }

   public JMenu getTools() {
      return this.tools;
   }
}
