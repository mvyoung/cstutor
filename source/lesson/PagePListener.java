package lesson;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import course.Page;

/**
 * Calls the Page.prev method when invoked.
 * @author Steven
 *
 */
public class PagePListener implements ActionListener {
	
   LessonPanel panel;
   
   public PagePListener(LessonPanel panel) {
      this.panel = panel;
   }

   @Override
   public void actionPerformed(ActionEvent event) {
      Page current = panel.getPage();
      System.out.println("Moved to pre");
      
   }
}

