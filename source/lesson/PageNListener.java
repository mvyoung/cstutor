package lesson;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import course.Page;

/**
 * Calls the Page.next method when invoked.
 * @author Steven
 *
 */
public class PageNListener implements ActionListener {

   LessonPanel panel;
   
   public PageNListener(LessonPanel panel) {
      this.panel = panel;
   }
   
   @Override
   public void actionPerformed(ActionEvent arg0) {
      Page current = panel.getPage();
      System.out.println("Moved to next");
   }

}
