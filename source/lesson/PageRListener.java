package lesson;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Calls the Page.run method when invoked.
 * @author Steven
 *
 */
public class PageRListener implements ActionListener {

   LessonPanel panel;
   
   public PageRListener(LessonPanel panel) {
      this.panel = panel;
   }
   
   @Override
   public void actionPerformed(ActionEvent event) {
      //panel.getPage().run();
   }

}
