package lesson;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import ui.MenuListener;

// our imports
import main.CSTutor;

/**
 * Lesson Action Listener to load the GUI on menu press.
 * @author Stan02
 *
 */
public class LessonActionListener extends MenuListener {
   private LessonPanel panel;

   public LessonActionListener() {
      super();
      this.panel = (LessonPanel)cs.panels.get(CSTutor.LESSONS);
   }

   private void switchToPanel() {
      CardLayout cl = (CardLayout) cs.cards.getLayout();
      cl.show(cs.cards, CSTutor.LESSONS);
   }

   public void actionPerformed(ActionEvent e) {
       switchToPanel();
   }
}
