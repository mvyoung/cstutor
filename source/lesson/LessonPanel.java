package lesson;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import main.CSTutor;
import course.Page;
import course.TextSegment;
import db.PageDatabase;
/**
 * Panel for lessons for creation.
 * 
 * @author Steven
 *
 */
public class LessonPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   
   public JPanel text, code, out;
   public JScrollPane textScroll;
   public JSplitPane vertical, horizontal;
   
   public JButton prev, home, next, run, quiz, lesson, save;
   
   private course.Page page;
   private JTextArea textArea;
   
   protected PageDatabase pdb;
   
   /**
    * Accesses the underlying page of the panel.
    * @return
    */
   public course.Page getPage() {
      return page;
   }
   
   /**
    * Replaces the underlying page of the panel.
    * @param page
    */
   public void setPage(Page page) {
      this.page = page;
   }
   
   public void setPageById(long id) {
	   Page p = pdb.selectPage(id);
	   if (p == null) {
		   
	   }
	   if (p.text == null) {
		   p.text = new TextSegment("");
	   }
	   setPage(p);
   }
   
   public void rebuild() {
	   textArea.setText(page.text.getText());
   }
   
   /**
    * Constructs the panel and initializes it.
    */
   public LessonPanel() {
      super(new BorderLayout());
      pdb = CSTutor.getInstance().getDBPage();
      initialize();
   }
   
   private void initialize() {
	      String docs = "Enter Lesson Here";
	      String pageType = "Lesson";
	      
	      textArea = new JTextArea(docs);
	      textArea.setLineWrap(true);
	      textArea.setWrapStyleWord(true);
	      textScroll = new JScrollPane();
	      textScroll.setBorder(BorderFactory.createTitledBorder(pageType));
	      textScroll.setViewportView(textArea);
	      textScroll.setPreferredSize(new Dimension(800, 600));
	      
	      Box buttonBox = Box.createHorizontalBox();
	      prev = new JButton("Back");
	      home = new JButton("Home");
	      next = new JButton("Next");
	      run = new JButton("Run");
	      save = new JButton("Save");
	      save.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				page.text = new TextSegment(textArea.getText());
				pdb.editPage(page);
			}
		});
	      buttonBox.add(prev);
	      buttonBox.add(home);
	      buttonBox.add(next);
	      buttonBox.add(run);
	      buttonBox.add(save);
	      
	      text = new JPanel(new BorderLayout());
	      text.add(textScroll, BorderLayout.CENTER);
	      text.add(buttonBox, BorderLayout.PAGE_END);
	      
	      vertical = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
	      horizontal = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
	      
	      vertical.setTopComponent(code);
	      vertical.setBottomComponent(out);
	      
	      horizontal.setLeftComponent(text);

	      
	      add(horizontal, BorderLayout.WEST);
	   }
   /**
    * Grabs the current string in the text box
    * @return The user-typed String
    */
   public String grabCurrentText(){
	   return textArea.getText();
   }
   /**
    * Save the string and sends it to the DB
    */
   public void saveString(){
	   System.out.println("Saved");
   }
   
   /**
    * Adds a listener to the "previous" button.
    * @param listener
    */
   public void addPrevListener(ActionListener listener) {
      prev.addActionListener(listener);
   }
   /**
    * Adds a listener to the "next" button.
    * @param listener
    */
   public void addNextListener(ActionListener listener) {
      next.addActionListener(listener);
   }
   
   /**
    * Adds a listener to the "run" button.
    * @param listener
    */
   public void addRunListener(ActionListener listener) {
      run.addActionListener(listener);
   }
   
}