package lesson;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 * Calls the Page.next method when invoked.
 * @author Steven
 *
 */
public class PageListener implements ActionListener {

   public PageListener() {
      
   }
   
   public void actionPerformed(ActionEvent arg0) {
	   System.out.println("Switching to Page Creation");
   }
}