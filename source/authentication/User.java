package authentication;

/**
 * Description of a User.
 * @author Aaron Pramana (apramana@calpoly.edu) and Joey Wilson
 */
public class User implements java.io.Serializable {

    /**
     * Constant for instructor to be used in permission.
     */
    final int INSTRUCTOR = 1;

    /**
     * Constant for student to be used in permission.
     */
    final int STUDENT = 0;

    /**
     * Username of the User.
     */
    public String username;

    /**
     * Id of the User.
     */
    public int id;

    /**
     * Permission level of the User.
     */
    public int permission;

    public User(String u, int i, int perm){
        username = u;
        id = i;
        permission = perm;
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + id;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (!username.equals(user.username)) return false;

        return true;
    }

    @Override
    public String toString(){
        return username;
    }
}
