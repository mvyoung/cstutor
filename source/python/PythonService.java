package python;

import java.io.IOException;

/**
 * Service class that handles access to the Python interpreters.
 * @author smirza
 *
 */
public class PythonService {
	
	/**
	 * Returns a PythonRuntime object suitable executing student-provided Python code.
	 * 
	 * Repeated calls to this method are guaranteed to return independent instances of
	 * the Python interpreter.
	 * 
	 * @return
	 */
   public PythonRuntime getPythonInterpreter() {
      return new PythonRuntime();
   }
   
   /**
    * Stops the Python interpreter.
    * 
    * This method ensures that the Python interpreter will receive the appropriate
    * commands to gracefully halt. The method does NOT guarantee that the process will actually
    * halt by the time the method returns. The process is only guaranteed to eventually halt.
    * 
    * @param python
    * @throws IOException
    */
   public void stopPythonInterpreter(PythonRuntime python) throws IOException {
      if (!python.writeStreamIsOpen()) {
         python.startWriteStream();
      }
      python.stop();
      python.endWriteStream();
   }
}
