package python;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

/**
 * Object that encapsulates a Python interpreter.
 * 
 * NOTE: This class has a package-scope constructor. DO NOT attempt
 * to allocate a PythonRuntime object manually, use the PythonService
 * for that.
 * 
 * @author smirza
 *
 */
public class PythonRuntime {
   private Process interpreter;
   
   private BufferedWriter writer; // this is sometimes null
   private final BufferedReader reader; // this is always not null
   
   private boolean writeStreamOpen = false;
   private boolean stopOrderGiven = false;
   
   /**
    * Returns true if the interpreter is open for writing, false otherwise.
    * 
    * @return
    */
   public boolean writeStreamIsOpen() {
      return writeStreamOpen;
   }
   
   /**
    * Constructs a new Python runtime.
    */
   PythonRuntime() {
      ProcessBuilder pb = new ProcessBuilder("python").redirectErrorStream(true);
      try {
         interpreter = pb.start();
         reader = new BufferedReader(new InputStreamReader(interpreter.getInputStream()));
      }
      catch (IOException e) {
         interpreter.destroy();
         throw new RuntimeException(e);
      }
   }
   
   /**
    * Returns the java.lang.Process object for the Python interpreter process.
    * @return
    */
   Process getInterpreter() {
      return interpreter;
   }
   
   /**
    * Opens the interpreter for writing.
    */
   public void startWriteStream() {
      writer = new BufferedWriter(new OutputStreamWriter(interpreter.getOutputStream()));
      writeStreamOpen = true;
   }
   
   /**
    * Closes the interpreter for writing.
    * 
    * Empirical observation suggests that it is necessary to call this
    * method to ensure that any data is actually written to the interpreter.
    * 
    * Will throw a RuntimeException if unable to close the interpreter.
    */
   public void endWriteStream() {
      try {
         if (writer != null) {
            writer.close();
            writer = null;
         }
      }
      catch (IOException e) {
         System.err.println("ERROR: Unable to close Writer for python interpreter.");
         throw new RuntimeException(e);
      }
   }
   
   /**
    * Sends the given string to the interpreter.
    * 
    * The string should be properly formatted Python source code. It may contain
    * multiple logical lines, each separated by the '\n' character.
    * For more information on Python syntax, visit http://docs.python.org/2/
    * 
    * This method throws an IllegalStateException if it is called when the
    * write stream is closed.
    * 
    * @param code
    * @throws IOException
    */
   public void send(String code) throws IOException {
      if (!writeStreamIsOpen()) {
         throw new IllegalStateException("CANNOT SEND TO INTERPRETER WHEN STREAM IS NOT OPEN");
      }
      
      writer.write(code);
   }
   
   /**
    * Sends the given string to the interpreter.
    * 
    * The string should be properly formatted Python source code. It must
    * contain only a single logical line with no '\n' character.
    * For more information on Python syntax, visit http://docs.python.org/2/
    * 
    * This method throws an IllegalStateException if it is called when the
    * write stream is closed.
    * 
    * @param code
    * @throws IOException
    */
   public void sendLine(String code) throws IOException {
      if (!writeStreamIsOpen()) {
         throw new IllegalStateException("CANNOT SEND TO INTERPRETER WHEN STREAM IS NOT OPEN");
      }
      
      writer.write(code);
      writer.write('\n');
   }
   
   /**
    * Halts the Python interpreter.
    * @throws IOException
    */
   void stop() throws IOException {
      sendLine("exit()");
   }
   
   /**
    * Reads a single line from the output of the Python interpreter.
    * 
    * The strings returned by this method may originate from either the
    * standard output or error output of the interpreter. The two streams
    * are mixed.
    * 
    * @return
    * @throws IOException
    */
   public String readLine() throws IOException {
      return reader.readLine();
   }
}
