package quiz;

/**
 * This class represents the result of a quiz.
 *
 * @author Raymond Pederson
 * @author Sajjad Mirza
 * @author Steven Tan
 */
public class QuizResult {

   /**
    * Identifies the quiz this result is for.
    */
   String quizID;

   /**
    * Identifies the student who earned this result.
    */
   String studentID;

   /**
    * This value represents the numeric score earned on the quiz.
    */
   int score;

   /**
    * This value represents whether the student passed the quiz.
    */
   Boolean passed;
}
