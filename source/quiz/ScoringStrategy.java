package quiz;
import java.util.Random;

import db.ScoringDatabase;

/**
 * Represents an algorithm used to score a specific quiz.
 * @author Raymond Pederson
 * @author Sajjad Mirza
 * @author Steven Tan
 */
public class ScoringStrategy {
	
	/**
	 * Returns a pseudo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimum value
	 * @param max Maximum value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	public static int randInt(int min, int max) {

	    // NOTE: Usually this should be a field rather than a method
	    // variable so that it is not re-seeded every call.
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
   /**
    * The database to deposit the computed scores in.
    * This databse not implemented
    */
   ScoringDatabase db;

   /**
    * Computes the score in the form of a QuizResult object given the QuizData.
    *
    * <p>Also stores the QuizResult in the ScoringDatabase.
    */
   QuizResult computeResult(QuizData data)
   {
	   QuizResult q = new QuizResult();
	   q.score = randInt(0,100);
	   
	   int studentid = randInt(0,1000);
	   
	   StringBuilder sb = new StringBuilder();
	   sb.append("");
	   sb.append(studentid);
	   q.studentID = sb.toString();
	   
	   if(data.pass == 1) {
		   q.passed = true;
	   } else {
		   q.passed = false;
	   }
	   
	   return q;
   }
}
