package quiz;
/**
 * Represents a range for a quiz
 * @author Stan02
 *
 */
public class Range {
	
	/**
	 * High range of the quiz
	 */
	int high;
	/**
	 * Low range of the quiz
	 */
	int low;
	
	public Range (int h ,int l) {
		this.high = h;
		this.low = l;
	}
	
}
