package quiz;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import main.CSTutor;

import course.CodeSegment;
import course.Lesson;
import course.Page;
import course.TextSegment;
/**
 * A quiz is a page for which a student can receive a grade.
 *
 * <p>A quiz is scored according to its ScoringStrategy and a student's progress
 * is gated according to the ProgressStrategy.
 *
 * @author Raymond Pederson
 * @author Sajjad Mirza
 * @author Steven Tan
 */
public class Quiz extends Page {

	ArrayList<Long> quizDestination;
	
	
   /**
	* Six hard coded ranges that a user can pick from
    */
	
	Range r100to75 = new Range(100,75);
	Range r75to50 = new Range(75,50);
	Range r100to0 = new Range(100,0);
	Range r0to0 = new Range(0,0);
	Range r90to80 = new Range(90,80);
	Range r50to0 = new Range(50,0);
	
   /**
    * The algorithm used to determine the score for the quiz.
    * WILL BE HARD CODED.
    */
   ScoringStrategy scoringStrategy;

   /**
    * The algorithm used to determine the possible paths the student can take
    * after taking the quiz.
    */
   ProgressStrategy progressStrategy;
	/**
	 * Constructs a quiz object.
	 */
   public Quiz() {
      super();
      this.scoringStrategy = new ScoringStrategy();
   }

   /**
	 * Constructs a quiz object.
	 */
   public Quiz(long pi, String ty, Lesson pl, String pn, TextSegment t,
         CodeSegment c, int po) {
      super(pi, ty, pl, pn, t, c, po, "quiz");
      this.scoringStrategy = new ScoringStrategy();
   }

   /**
	 * Constructs a quiz object.
	 */
   public Quiz(String ty, Lesson pl, String pn, TextSegment t, CodeSegment c,
         int po) {
      super(ty, pl, pn, t, c, po, "quiz");
      this.scoringStrategy = new ScoringStrategy();
   }
   
   /**
    * Adds a destination to the current quiz
    * @param p destination
    * @param r range
    */
	public void addLocation(Page p, Range r){
		long e = CSTutor.getInstance().getDBQuizDestination().addQuizDestination(this, r.low, r.high, p);
		quizDestination.add(e);
	}
	
	/**
	 * Removes all quiz destination from p
	 * @param p page
	 */
	public void removeAllQuiz(Page p){
		CSTutor.getInstance().getDBQuizDestination().removeAllQuizDestinations(p);
	}
	
	/**
	 * Id of the quiz destination
	 * @param id of the quiz destination
	 */
	public void removeQuiz(long id){
		CSTutor.getInstance().getDBQuizDestination().removeQuizDestination(id);
	}
	/**
	 * Get the quiz location destination using id and a score
	 * @param id quiz destination id
	 * @param score score of the quiz
	 * @return 
	 */
	public Page getLocation(long id, int score){
		return CSTutor.getInstance().getDBQuizDestination().selectQuizDestination(id, score);
	}

   /**
    * Runs the student's code.
    *
    * <p>Compile- and runtime-output to standard out and standard error are
    * redirected to the output pane of the program.
    *
    * <p>A Quiz may define the run() method such that there are a limited number
    * of times it may be called, or other such restrictions.
    */
   public List<String> run() throws IOException {
	  //Sajadd's python code here.
      return null;
   }
}
