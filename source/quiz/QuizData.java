package quiz;

import java.util.Random;

/**
 * This class represents the data received from a quiz.
 * 
 * <p>The class is intentionally vague presently because the exact nature
 * of the data required to compute a score for a quiz is presently undetermined.
 *
 * @author Raymond Pederson
 * @author Sajjad Mirza
 * @author Steven Tan
 */
public class QuizData {
	
	public static int randInt(int min, int max) {

	    // NOTE: Usually this should be a field rather than a method
	    // variable so that it is not re-seeded every call.
	    Random rand = new Random();

	    // nextInt is normally exclusive of the top value,
	    // so add 1 to make it inclusive
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	String code;
	
	int pass = randInt(0,1);
	
}
