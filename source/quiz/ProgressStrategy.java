package quiz;
import java.util.Map;

import main.CSTutor;
import course.Page;
/**
 * Represents the algorithm used for decide what a student can do after
 * taking a quiz.
 *
 * <p>Different implementations may exist, for example, a 
 * FlexibleProgressStrategy might allow a student to go to any page in the
 * tutorial, regardless of result..
 *
 * @author Raymond Pederson
 * @author Sajjad Mirza
 * @author Steven Tan
 */
public class ProgressStrategy {

   /**
    * Determines the valid pages for a student to visit after achieving a
    * certain result on a quiz. 
    *
    * <p>The String component of each Map.Entry is
    * what should be displayed to the user to describe that particular path.
    */

	
	Map<String,Page> getAllowedDestinations(QuizResult result){
	   return null;
	}
}
