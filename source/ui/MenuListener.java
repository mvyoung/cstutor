package ui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JPanel;

import main.CSTutor;

/**
 * Abstract base class for all listeners that want to change what panel is being displayed.
 * @author smirza
 *
 */
public abstract class MenuListener implements ActionListener {
   protected JFrame frame;
   protected CSTutor cs = CSTutor.getInstance();
   
   /**
    * Constructs the listener.
    */
   public MenuListener() {
      this.frame = cs.frame;
   }
   
   /**
    * Redeclaring the method.
    */
   public abstract void actionPerformed(ActionEvent event);
}
