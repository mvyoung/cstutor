package communication;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import authentication.User;
import main.CSTutor;

import java.util.*;

/****
 *
 * OnlineListUI manages the view for the list of online users.
 *
 *
 * @author Aaron Pramana (apramana@calpoly.edu)
 *
 */
public class OnlineListUI {
   /**
    * The frame for this list
    */
   public JFrame frame;

   /**
    * The list UI
    */
   public JList list;

   /**
    * The swing-specific model
    */
   public DefaultListModel listModel;

   /**
    * The data model
    */
   public OnlineList olModel;

   /**
    * Constructs the view
    * @param ol The model for this view
    * @param parent The CSTutor frame
    */
   public OnlineListUI(OnlineList ol, JFrame parent){
      olModel = ol;
      //Create and set up the window.
      this.frame = new JFrame("List of Students Online");

      JLabel emptyLabel = new JLabel("");
      emptyLabel.setPreferredSize(new Dimension(250, 400));
      frame.getContentPane().add(emptyLabel, BorderLayout.CENTER);
      frame.setLocationRelativeTo(parent);

      //Display the window.
      frame.pack();
      frame.setVisible(true);

      buildList();
   }

   /**
    * Updates the list pane whenever the users list is updated.
    */
   public void updateListModel(){
      listModel = new DefaultListModel();
      for(User u : CSTutor.getInstance().chatClient.users){
         listModel.addElement(u);
      }
   }

   /**
    * Builds the UI elements.
    */
   public void buildList(){
      updateListModel();
      //Create the list and put it in a scroll pane.
      list = new JList(listModel);
      list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
      list.setSelectedIndex(0);
      list.setVisibleRowCount(5);
      JScrollPane listScrollPane = new JScrollPane(list);

      JButton chatButton = new JButton("Start Chat");
      chatButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
               User selected = (User)list.getSelectedValue();
               if(selected != null) {
                  Chat chat = olModel.startChat(selected);
                  chat.makeView();
               }
               else{
                  JOptionPane.showMessageDialog(null, "You must select a user.");
               }
            }
        });

      //Create a panel that uses BoxLayout.
      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new BoxLayout(buttonPane,
            BoxLayout.LINE_AXIS));
      buttonPane.add(chatButton);
      buttonPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
      frame.add(listScrollPane, BorderLayout.CENTER);
      frame.add(buttonPane, BorderLayout.PAGE_END);
   }
}