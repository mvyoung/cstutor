package communication;

import java.util.*;
import authentication.User;

/****
 *
 * The OnlineList class manages the properties and methods associated with the list displaying online users.
 * It creates Chat sessions when the "Start Chat" button is clicked.
 * It interacts with the ChatClient class to send and receive server commands.
 *
 *
 * @author Aaron Pramana (apramana@calpoly.edu)
 *
 */
public class OnlineList {
   /**
    * All logged-in users as provided by the chat server
    */
   public ArrayList<User> users;

   /**
    * The current user
    */
   public User currentUser;

   /**
    * The view displaying the list
    */
   public OnlineListUI view;

   /**
    * All the Chat objects created by this list.
    */
   public ArrayList<Chat> activeChats;

   /**
    * ChatClient for this session
    */
   public ChatClient client;

   /**
    * Constructs the object
    */
   /*@
      ensures
         currentUser != null &&
         users != null &&
         activeChats != null;
   @*/
   public OnlineList(User u, ChatClient c){
      client = c;
      currentUser = u;
      ArrayList<User> userList = new ArrayList<User>();
      userList.add(currentUser);
      users = userList;
      activeChats = new ArrayList<Chat>();
      client.setList(this);
   }

   /**
    * Returns the chat given its unique identifier. Returns null if not found.
    * @param id The chat ID
    * @return The corresponding chat object, or null if not found
    */
   /*@
      requires
         id >= 0 &&
         activeChats != null;
      ensures
         result == null ||
         result instanceof Chat;
   @*/
   public Chat getChatById(int id){
      if(activeChats == null || id < 0)
         throw new RuntimeException();
      for(int i = 0; i < activeChats.size(); i++){
         if(activeChats.get(i).id == id)
            return activeChats.get(i);
      }
      return null;
   }

   /**
    * Sets the view
    * @param ui The view that corresponds to this model
    */
   public void setView(OnlineListUI ui){
      view = ui;
   }

   /**
    * Creates a new Chat model/view pair and adds it to the activeChats
    * @param selected The User to start a new chat with
    */
   /*@
      requires
         selected != null &&
         activeChats != null &&
         activeChats.size() >= 0;
      ensures
         activeChats.size() == old(activeChats).size() + 1;
   @*/
   public Chat startChat(User selected){
      if(!(selected != null && activeChats != null && activeChats.size() >= 0))
         throw new RuntimeException();
      ArrayList<User> chatUsers = new ArrayList<User>();
      chatUsers.add(selected);
      chatUsers.add(currentUser);
      Chat chat = null;
      chat = new Chat(users, 1, currentUser, this);
      activeChats.add(chat);
      return chat;
   }
}
