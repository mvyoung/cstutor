package communication;

import java.net.*;
import java.io.*;

/****
 *
 * ChatClientThread manages the thread for this connection.
 *
 *
 * @author Bert G. Wachsmuth. Adapted by Aaron Pramana (apramana@calpoly.edu)
 * http://pirate.shu.edu/~wachsmut/Teaching/CSAS2214/Virtual/Lectures/chat-client-server.html
 *
 */
public class ChatClientThread extends Thread {
   private Socket socket = null;
   private ChatClient client = null;
   private ObjectInputStream streamIn = null;

   public ChatClientThread(ChatClient _client, Socket _socket) {
      client = _client;
      socket = _socket;
      open();
      start();
   }

   /**
    * Opens the input stream
    */
   public void open() {
      try {
         streamIn = new ObjectInputStream(socket.getInputStream());
         System.out.println("Hello");
      } catch (IOException ioe) {
         System.out.println("Error getting input stream: " + ioe);
         client.stop();
      }
   }

   /**
    * Closes the input stream
    */
   public void close() {
      try {
         if (streamIn != null) streamIn.close();
      } catch (IOException ioe) {
         System.out.println("Error closing input stream: " + ioe);
      }
   }

   /**
    * Runs loop that takes in data from the stream.
    */
   public void run() {
      while (true) {
         try {
            try{
               client.handle(streamIn.readObject());
            }
            catch(ClassNotFoundException e){
               System.out.println("Class not found.");
            }
         } catch (IOException ioe) {
            System.out.println("Listening error: " + ioe.getMessage());
            client.stop();
            return;
         }
      }
   }
}