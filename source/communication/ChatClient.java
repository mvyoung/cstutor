package communication;

import java.net.*;
import java.io.*;
import java.util.*;
import authentication.User;
import main.CSTutor;

import javax.swing.*;

/****
 *
 * ChatClient sends and receives serialized ChatMessages for server data synchronization.
 * This class will be instantiated once when CSTutor starts up and lives until the program is closed.
 *
 *
 * @author Bert G. Wachsmuth. Adapted by Aaron Pramana (apramana@calpoly.edu)
 * http://pirate.shu.edu/~wachsmut/Teaching/CSAS2214/Virtual/Lectures/chat-client-server.html
 *
 */
public class ChatClient implements Runnable {

   /**
    * The network socket
    */
   private Socket socket = null;

   /**
    * The generic process thread
    */
   private Thread thread = null;

   /**
    * The stream from which sent objects are read.
    */
   private ObjectOutputStream streamOut = null;

   /**
    * The chat client thread
    */
   private ChatClientThread client = null;

   /**
    * The list to be updated with users
    */
   public OnlineList olList;

   /**
    * The list of users received from the server
    */
   public ArrayList<User> users;

   /**
    * Test mode
    */
   public boolean testing;

   /**
    * Constructs the object by creating the connection to the server.
    * @param serverName
    * @param serverPort
    */
   public ChatClient(String serverName, int serverPort) {
      if(serverName.equals("")){
         testing = true;
      }
      else {
         System.out.println("Establishing connection. Please wait ...");
         testing = false;
         try {
            socket = new Socket(serverName, serverPort);
            System.out.println("Connected: " + socket);
            start();
         } catch (UnknownHostException uhe) {
            System.out.println("Host unknown: " + uhe.getMessage());
            JOptionPane.showMessageDialog(null, "The program could not connect to the chat server. Please ensure your" +
                  " internet connection is functioning properly and contact the project team if this problem persists.");
         } catch (IOException ioe) {
            System.out.println("Unexpected exception: " + ioe.getMessage());
            JOptionPane.showMessageDialog(null, "The program could not connect to the chat server. Please ensure your" +
                  " internet connection is functioning properly and contact the project team if this problem persists.");
         }
      }

      olList = null;
      users = null;
   }

   /**
    * Sets the list
    * @param ol The list model
    */
   public void setList(OnlineList ol){
      olList = ol;
   }

   /**
    * Writes a ChatMessage to the stream
    * @param cm The message to be written to the stream
    */
   public void send(ChatMessage cm){
      if(!testing) {
         try {
            System.out.println(cm.command);
            streamOut.writeObject(cm);
            streamOut.flush();
         } catch (IOException ioe) {
            System.out.println("Sending error: " + ioe.getMessage());
            //stop();
         }
      }
   }

   /**
    * Not implemented.
    * Polling is not needed. The model will invoke send()
    */
   public void run() {
      /*while (thread != null) {
         try {
            streamOut.writeObject(scanner.nextLine());
            streamOut.flush();
         } catch (IOException ioe) {
            System.out.println("Sending error: " + ioe.getMessage());
            stop();
         }
      }*/
   }

   /**
    * Receives raw objects from the server and routes them according to type (and command if it is a ChatMessage.)
    * @param obj The object received from the server
    */
   public void handle(Object obj) {
      if(obj instanceof ArrayList){
         users = (ArrayList<User>)obj;
         System.out.println(users);
      }
      if(obj instanceof ChatMessage){
         ChatMessage cm = (ChatMessage)obj;
         System.out.println("THE CM: "+cm.command+", "+cm.user.username);
         if(olList == null){
            OnlineList model = null;
            try {
               model = new OnlineList(CSTutor.currentUser, CSTutor.chatClient);
               OnlineListUI listUI = new OnlineListUI(model, CSTutor.getInstance().frame);
               model.setView(listUI);
            }
            catch (Exception e){
               System.out.println("Online Users List could not be created.");
            }
         }
         Chat chat = null;
         try {
            chat = olList.getChatById(cm.roomID);
         }
         catch(Exception e){
            System.out.println("Chat not found.");
         }
         if(chat == null){
            ArrayList<User> chatUsers = new ArrayList<User>();
            chatUsers.add(CSTutor.currentUser);
            try {
               chat = new Chat(chatUsers, cm.roomID, CSTutor.currentUser, olList);
            }
            catch (Exception e){
               System.out.print("Chat not created.");
            }
            olList.activeChats.add(chat);
            chat.makeView();
         }
         if(cm.command == ChatMessage.ENTER_ROOM){
            if(!chat.users.contains(cm.user)){
               chat.users.add(cm.user);
            }
         }
         else if(cm.command == ChatMessage.EXIT_ROOM){
            if(chat.users.contains(cm.user)){
               chat.users.remove(cm.user);
            }
         }
         else if(cm.command == ChatMessage.MESSAGE){
            try {
               chat.putMessage(cm);
            }
            catch (Exception e){
               System.out.print("No chat found.");
            }
         }
      }
   }

   /**
    * Once a connection has been established, create streams and threads.
    * @throws IOException
    */
   public void start() throws IOException {  //console   = new DataInputStream(System.in);
      //scanner = new Scanner(System.in);
      streamOut = new ObjectOutputStream(socket.getOutputStream());
      if (thread == null) {
         client = new ChatClientThread(this, socket);
         thread = new Thread(this);
         thread.start();
      }
      send(new ChatMessage(ChatMessage.LOGIN, 0, CSTutor.getInstance().currentUser, null, null));
   }

   /**
    * Stops threads and closes streams
    */
   public void stop() {
      if (thread != null) {
         thread.interrupt();
         thread = null;
      }
      try {  //if (console   != null)  console.close();
         if (streamOut != null) streamOut.close();
         if (socket != null) socket.close();
      } catch (IOException ioe) {
         System.out.println("Error closing ...");
      }
      client.close();
      return;
   }
}
