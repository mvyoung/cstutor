package communication;

import authentication.User;

import java.util.Collection;
import java.util.Date;

/**
 * Manages the properties and methods associated with each chat message.
 * Includes both actual messages AND commands for managing users.
 */
public class ChatMessage implements java.io.Serializable {
   /**
    * The command type of the message
    */
   public int command;

   /**
    * The chat room this message goes to
    */
   public int roomID;

   /**
    * The user that this message describes.
    * (Not necessarily the user who sent the message)
    */
   public User user;

   /**
    * If it is a message command, contains the string with the message
    */
   public String text;

   /**
    * If it is a message command, time of the message
    */
   public Date datetime;

   /**
    * Command types
    */
   public static final int LOGIN = 0,
         ENTER_ROOM = 1,
         EXIT_ROOM = 2,
         MESSAGE = 3;

   /**
    * Constructs the ChatMessage
    * @param c The command
    * @param ID The room ID
    * @param u The User for whom this message refers to
    * @param t The message text
    * @param dt The date/time of this message
    */
   public ChatMessage(int c, int ID, User u, String t, Date dt){
      command = c;
      roomID = ID;
      user = u;
      text = t;
      datetime = dt;
   }
}
