package communication;

import authentication.User;

import java.text.SimpleDateFormat;
import java.util.*;

/****
 *
 * The Chat class manages the properties and methods associated with the transmission and processing of chat messages.
 * A new Chat object is created for each conversation the user is having with other users.
 * It interacts both with its view and the ChatClient created in the main CSTutor class.
 *
 *
 * @author Aaron Pramana (apramana@calpoly.edu)
 *
 */
public class Chat {
   /**
    * Unique identifier of the chat session
    */
   public int id;

   /**
    * Users in the chat.
    */
   public Collection<User> users;

   /**
    * The current user.
    */
   public User currentUser;

   /**
    * The view for this Chat.
    */
   public ChatUI view;

   /**
    * List model
    */
   public OnlineList list;

   /**
    * Constructs the Chat.
    * @param all users to be included in the Chat
    * @param roomID an existing id from another user or -1 if it does not exist.
    */
   /*@
    requires
      u != null &&
       roomID >= -1 &&
       all.size() > 0;
    ensures
       users.size() > 0 &&
       users.contains(currentUser) &&
       id >= 0;
   @*/
   public Chat(Collection<User> all, int roomID, User u, OnlineList ol) {
      if(all == null || all.size() == 0){
         throw new RuntimeException();
      }
      users = all;
      currentUser = u;
      list = ol;
      if(roomID != -1)
         id = roomID;
      else
         id = (int)(Math.random()*1000000);
      propagateUsers();

      //System.out.println("Start chat clicked");
   }

   /**
    * Sends out all the users to be included in the conversation.
    */
   public void propagateUsers() {
      // take this, send it to the server
      //System.out.println(users);
      for(User u : users) {
         ChatMessage cm = new ChatMessage(ChatMessage.ENTER_ROOM, id, u, null, null);
         list.client.send(cm);
      }
   }

   /**
    * Sets the view.
    */
   public void makeView(){
      ChatUI chatUI = new ChatUI(this, main.CSTutor.getInstance().frame);
      view = chatUI;
   }

   /**
    * Wraps a message and sends it to the server.
    * @param s the message text to be wrapped and sent
    */
   /*@
    requires
       s != null &&
       !s.trim().isEmpty() &&
       s.length() < 1000;
   @*/
   public void sendMessage(String s) {
      //System.out.println("Send clicked");

      if(s == null || s.isEmpty() || s.length() >= 1000)
         throw new RuntimeException();

      ChatMessage cm = new ChatMessage(ChatMessage.MESSAGE, id, currentUser, s, new Date());

      list.client.send(cm);
   }

   /**
    * Sends a received message's username and text to the view.
    * @param cm The message to be sent to the server.
    */
   /*@
      requires
         cm.user != null &&
         cm.datetime != null &&
         cm.text != null;
      ensures
         view.messageText != null &&
         old(view.messageText).length() < view.messageText.length();
    */
   public void putMessage(ChatMessage cm){
      if(!(cm.user != null && cm.datetime != null && cm.text != null)){
         throw new RuntimeException();
      }

      if(view != null)
         view.appendMessage(cm.user.username, new SimpleDateFormat("HH:mm a").format(cm.datetime), cm.text);
   }

   /**
    * Starts a remote session with the other user.
    * (Not yet implemented.)
    */
   public void startRemote() {
      //System.out.println("Remote clicked");
      //send something to the server
      //launch UI
   }

   /**
    * Closes the current chat session.
    */
   public void endChat() {
      //System.out.println("End chat clicked");

      //tell the server to end chat
      ChatMessage cm = new ChatMessage(ChatMessage.EXIT_ROOM, id, currentUser, null, null);
      list.client.send(cm);
      list.activeChats.remove(this);
   }
}