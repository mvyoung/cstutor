package communication;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/****
 *
 * ChatUI manages the view for each Chat created.
 *
 *
 * @author Aaron Pramana (apramana@calpoly.edu)
 *
 */
public class ChatUI {

   /**
    * The frame
    */
   public JFrame frame;

   /**
    * The model
    */
   public DefaultListModel listModel;

   /**
    * The model of the chat
    */
   public Chat chat;

   /**
    * Displays the messages
    */
   JEditorPane editorPane;

   /**
    * Collects message from the user
    */
   JTextField textField;

   /**
    * Internal representation of HTML to be passed to the editor pane
    */
   public String messageText;

   /**
    * Constructs the UI
    * @param c The chat model for this view
    * @param parent The CSTutor frame
    */
   public ChatUI(Chat c, JFrame parent){
      this.chat = c;

      //Create and set up the window.
      this.frame = new JFrame("Chat");

      frame.addWindowListener(new java.awt.event.WindowAdapter() {
         @Override
         public void windowClosing(java.awt.event.WindowEvent windowEvent) {
            chat.endChat();
         }
      });

      messageText = "";

      JLabel emptyLabel = new JLabel("");
      emptyLabel.setPreferredSize(new Dimension(300, 300));
      frame.getContentPane().add(emptyLabel, BorderLayout.CENTER);
      frame.setLocationRelativeTo(parent);

      //Display the window.
      frame.pack();
      frame.setVisible(true);

      buildChat();
   }

   /**
    * Add a message to the editor pane
    * @param user The user who sent the message
    * @param time The time the message was sent
    * @param text The message text
    */
   public void appendMessage(String user, String time, String text){
      messageText += "<b>" + user + " [" + time + "]: </b>" + text + "<br>";
      editorPane.setText(messageText);
   }

   /**
    * Build and attach all the UI elements
    */
   public void buildChat(){
      editorPane = new JEditorPane("text/html", null);
      editorPane.setEditable(false);

      //Put the editor pane in a scroll pane.
      JScrollPane editorScrollPane = new JScrollPane(editorPane);
      editorScrollPane.setVerticalScrollBarPolicy(
            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
      editorScrollPane.setPreferredSize(new Dimension(250, 230));
      editorScrollPane.setMinimumSize(new Dimension(10, 10));

      textField = new JTextField(20);
      textField.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            try {
               chat.sendMessage(textField.getText());
               textField.setText("");
            }
            catch(Exception ex){
               System.out.print("Message sending failed.");
            }
         }
      });
      JPanel textBoxPane = new JPanel();
      textBoxPane.add(textField);

      JButton sendButton = new JButton("Send");
      sendButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
               try {
                  chat.sendMessage(textField.getText());
                  textField.setText("");
               }
               catch(Exception ex){
                  System.out.println(ex.getMessage());
               }
            }
        });
      JButton remoteButton = new JButton("Remote Connection");
      remoteButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
               chat.startRemote();
            }
        });
      JButton endButton = new JButton("End Chat");
      endButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
               frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            }
        });

      JPanel buttonPane = new JPanel();
      buttonPane.setLayout(new BoxLayout(buttonPane,
            BoxLayout.LINE_AXIS));
      buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
      buttonPane.add(sendButton);
      buttonPane.add(remoteButton);
      buttonPane.add(endButton);
      buttonPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

      frame.add(editorScrollPane, BorderLayout.PAGE_START);
      frame.add(textBoxPane, BorderLayout.CENTER);
      frame.add(buttonPane, BorderLayout.PAGE_END);
   }
}