package communication;

import main.CSTutor;
import ui.MenuListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/****
 *
 * OnlineListActionListener listens for invocation from the top-level UI and creates OnlineList and its view.
 *
 *
 * @author Aaron Pramana (apramana@calpoly.edu)
 *
 */
public class OnlineListActionListener extends MenuListener {

   /**
    * Constructor
    */
   public OnlineListActionListener() {
      super();
   }

   /**
    * Creates the model and view for the online list
    */
   private void updatePanel() {
      //not a panel, launch the window

      OnlineList model = null;
      try {
         model = new OnlineList(CSTutor.currentUser, CSTutor.chatClient);
         OnlineListUI listUI = new OnlineListUI(model, CSTutor.getInstance().frame);
         model.setView(listUI);
      }
      catch (Exception e){
         System.out.println("Online Users List could not be created.");
      }
   }

   /**
    * Handles the action received
    * @param e The ActionEvent
    */
   public void actionPerformed(ActionEvent e) {
      System.out.println("Chat clicked");
      updatePanel();
   }
}
