package roadmap;

import main.CSTutor;
import ui.MenuListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The CourseSelectorActionListener class listens for a change in the JComboBox selection and updates the course selection
 * in Roadmap
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class CourseSelectorActionListener implements ActionListener {
   public RoadmapPanel roadmapPanel;

   public CourseSelectorActionListener(RoadmapPanel roadmapPanel) {
      this.roadmapPanel = roadmapPanel;
   }

   public void changeCourse(String course) {
      this.roadmapPanel.model.changeSelectedCourse(course);
   }

   @Override
   public void actionPerformed(ActionEvent e) {
      JComboBox<String> combo = (JComboBox<String>) e.getSource();
      String course = (String) combo.getSelectedItem();

      System.out.println("course selection updated to " + course);
      changeCourse(course);

      this.roadmapPanel.populate();
   }
}