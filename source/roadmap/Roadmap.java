package roadmap;

import java.util.*;

import course.Course;
import course.Lesson;
import course.Page;
import course.Topic;
import main.CSTutor;

/**
 * The Roadmap class contains the data structures that the RoadmapPanel requires
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class Roadmap {
   /**
    * Instance of main CSTutor class
    */
   public CSTutor main;

   /**
    * Instance of RoadmapPanel
    */
   public RoadmapPanel view;

   /**
    * Map of RoadmapNodes to their respective names
    */
   public HashMap<String, RoadmapNode> lookup;

   /**
    * ArrayList of RoadmapNodes
    */
   public ArrayList<RoadmapNode> nodes;

   /**
    * Currently selected RoadmapNode
    */
   public RoadmapNode selectedNode;

   /***
    * Hashmap of all courses
    */
   public HashMap<String, Course> courseMap;

   /***
    * Arraylist of all courses
    */
   public ArrayList<Course> courseList;

   /***
    * Course to display in roadmap panel
    */
   public Course selectedCourse;

   /***
    * Constructs roadmap object
    */
   /*@
    requires

    ensures
       lookup != null && lookup.isEmpty()
       nodes != null && nodes.isEmpty()
       courseMap != null && courseMap.isEmpty()
       courseList != null && courseList.isEmpty()
   @*/
   public Roadmap() {
      /***
       * initializes lists
       */
      this.main = CSTutor.getInstance();
      this.lookup = new HashMap<String, RoadmapNode>();
      this.nodes = new ArrayList<RoadmapNode>();
      this.courseMap = new HashMap<String, Course>();
      this.courseList = new ArrayList<Course>();

      /***
       * set selectedCourse
       */
      refreshCourses();
      if (courseList.isEmpty()) {
         this.selectedCourse = null;
      } else {
         this.selectedCourse = this.courseList.get(0);
      }
   }

   public Roadmap(int test) {
      this.lookup = new HashMap<String, RoadmapNode>();
      this.nodes = new ArrayList<RoadmapNode>();
      this.courseMap = new HashMap<String, Course>();
      this.courseList = new ArrayList<Course>();

      if (courseList.isEmpty()) {
         this.selectedCourse = null;
      }
   }

   /**
    * Initializes the RoadmapPanel object
    * @param panel
    */
   /*@
    requires

    ensures
       view == panel
       selectedNode == null
   @*/
   public void setView(RoadmapPanel panel) {
      this.view = panel;
      this.selectedNode = null;
   }

   /**
    * Unselects currently selected node
    */
   /*@
    requires

    ensures
       selectedNode == null
   @*/
   public void cancelSelection() {
      this.selectedNode = null;
   }

   /***
    * Clears arraylist of courses
    */
   /*@
    requires

    ensures
       courseMap != null && courseMap.isEmpty()
       courseList != null && courseList.isEmpty()
   @*/
   public void clearCourses() {
      courseMap = new HashMap<String, Course>();
      courseList = new ArrayList<Course>();
   }

   /***
    * Change the currently selected course
    * @param course
    */
   /*@
    requires
       courseMap contains course
    ensures
       selectedCourse is updated to new course
   @*/
   public void changeSelectedCourse(String course) {
      this.selectedCourse = courseMap.get(course);
      System.out.println(" NEW SELECTED COURSE: " + this.selectedCourse.courseName);
   }

   /***
    * Checks to see if courseList is empty and assigns selectedCourse a default course if courseList is not empty and selectedCourse is null
    *
    */
   /*@
    requires

    ensures
       if selectedCourse is null, it is set to a default value or return false if courseList is empty
   @*/
   public boolean checkSelectedCourse() {
      if (selectedCourse == null) {
         if (courseList.isEmpty()) {
            return false;
         }
         selectedCourse = courseList.get(0);
      }

      return true;
   }

   /***
    * Updates arraylist of courses
    */
   public void refreshCourses() {
      clearCourses();
      System.out.println("NUMBER OF COURSE: " + CSTutor.getInstance().getAllCourses().size());

      /***
       * Adds all courses into courseMap and courseList
       */
      for (Course c : CSTutor.getInstance().getAllCourses()) {
         courseMap.put(c.courseName, c);
         courseList.add(c);
      }
   }

   /**
    * Updates list of RoadmapNodes
    */
   public void refreshNodes() {
      /***
       * clears list of roadmap nodes
       */
      RoadmapNode prev = null;
      this.nodes = new ArrayList<RoadmapNode>();
      this.lookup = new HashMap<String, RoadmapNode>();

      /**
       * Creates RoadmapNodes from all courses
       */
      if (this.selectedCourse != null) {
         System.out.println("SELECTED COURSE: " + selectedCourse.courseName);
         for (Topic t : this.selectedCourse.getTopics()) {
            System.out.println("TOPIC ID: " + t.topicID);
            if (t.parentCourse.courseID == this.selectedCourse.courseID) {
               System.out.println("TOPIC: " + t.topicName);
               for (Lesson l : t.getLessons()) {
                  if (l.parentTopic.topicID == t.topicID) {
                     System.out.println("LESSON: " + l.lessonName);
                     for (Page p : l.getPages()) {
                        if (p.parentLesson.lessonID == l.lessonID) {
                           RoadmapNode newNode;
                           System.out.println("ORDER: " + p.pageID);

                           if (p.getPageType().equals("quiz")) {
//                              System.out.println ("QUIZ !!!");
                              newNode = new RoadmapQuizNode(p, prev, null, this.view);
                           } else {
                              //System.out.println ("EXERCISE !!!");
                              newNode = new RoadmapExerciseNode(p, prev, null, this.view);
                           }

                           if (prev != null) {
                              prev.next = newNode;
                              nodes.add(prev);
//                              System.out.println(p.pageName + " ADDED");
                           }
                           prev = newNode;
                        }
                     }
                  }
               }
            }
         }

         if (prev != null) {
            nodes.add(prev);
         }
      }
   }
}