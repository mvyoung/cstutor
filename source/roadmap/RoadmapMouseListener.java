package roadmap;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

/**
 * The RoadmapMouseListener
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class RoadmapMouseListener extends MouseAdapter {

   private JTree visibleTree;

   RoadmapMouseListener(JTree tree) {
      visibleTree = tree;
   }

   public void mousePressed(MouseEvent e) {
      int selRow = visibleTree.getRowForLocation(e.getX(), e.getY());
      TreePath selPath = visibleTree.getPathForLocation(e.getX(), e.getY());
      if (selRow != -1) {
         if (e.getClickCount() == 1) {
            DefaultMutableTreeNode selectedNode = (DefaultMutableTreeNode)(selPath.getLastPathComponent());
         }
//         else if (e.getClickCount() == 2) {
//         }
      }
   }
}
