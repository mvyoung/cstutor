package roadmap;

import main.CSTutor;
import ui.MenuListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The RoadmapCancelActionListeners class listens for the cancel button to be clicked and displays an empty bottom pane
 * in roadmap view
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class RoadmapCancelActionListener implements ActionListener {
   RoadmapPanel roadmapPanel;
   JPanel panel;

   public RoadmapCancelActionListener(RoadmapPanel roadmapPanel, JPanel panel) {
      this.roadmapPanel = roadmapPanel;
      this.panel = panel;
   }

   @Override
   public void actionPerformed(ActionEvent arg0) {
      this.roadmapPanel.model.cancelSelection();
      this.roadmapPanel.displayNoSelection(this.panel);
      this.roadmapPanel.displayCourseSwitcher(this.panel);
   }
}