package roadmap;

import main.CSTutor;
import course.*;
import ui.MenuListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * The RoadmapShiftActionListener class listens for a shift button to be clicked and updates the selected node's
 * previous or next fields
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class RoadmapShiftActionListener implements ActionListener {
   private RoadmapPanel roadmapPanel;
   private JPanel panel;
   private String direction;

   public RoadmapShiftActionListener(RoadmapPanel roadmapPanel, JPanel panel, String direction) {
      this.roadmapPanel = roadmapPanel;
      this.panel = panel;
      this.direction = direction;
   }

   public void reacquireSelectedNode() {
      Page p = roadmapPanel.model.selectedNode.page;

      roadmapPanel.model.refreshNodes();
      for (RoadmapNode n : roadmapPanel.model.nodes) {
         if (p.pageID == n.page.pageID) {
            roadmapPanel.model.selectedNode = n;
         }
      }
      roadmapPanel.displayNoSelection(roadmapPanel.bottomright);
      roadmapPanel.displaySelected(roadmapPanel.bottomright);
//      roadmapPanel.displayCourseSwitcher(roadmapPanel.bottomright);
   }

   public void changePosition() {
      Page p = roadmapPanel.model.selectedNode.page;

      if (direction.equals("left")) {
         if (roadmapPanel.model.selectedNode.prev != null) {
            p.parentLesson.switchPrePage(p);
            reacquireSelectedNode();
            roadmapPanel.populate();
         }
      } else if (direction.equals("right")) {
         if (roadmapPanel.model.selectedNode.next != null) {
            p.parentLesson.switchNextPage(p);
            reacquireSelectedNode();
            roadmapPanel.populate();
         }
      }
   }

   @Override
   public void actionPerformed(ActionEvent arg0) {
      changePosition();
   }
}