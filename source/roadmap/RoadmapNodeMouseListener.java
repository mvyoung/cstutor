package roadmap;

import main.CSTutor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * The RoadmapNodeMouseListener class listens for a mouse click on the RoadmapNode and either marks the node selected or
 * switches to tutorial view
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 *
 */
public class RoadmapNodeMouseListener extends MouseAdapter {
   public RoadmapPanel panel;
   public RoadmapNode node;

   public RoadmapNodeMouseListener(RoadmapPanel panel, RoadmapNode node) {
      this.panel = panel;
      this.node = node;
   }

   /**
    * Selects node on single click, opens tutorial on double click
    */
   public void mousePressed(MouseEvent e) {
      if (SwingUtilities.isLeftMouseButton(e)) {
         if (e.getClickCount() == 1) {
            this.panel.model.selectedNode = this.node;
            this.node.selected();
         } else if (e.getClickCount() == 2) {
            JPanel card = CSTutor.getInstance().cards;
            CardLayout cl = ((CardLayout)card.getLayout());
            cl.show(card, CSTutor.TUTORIAL);
         }
      }
   }
}