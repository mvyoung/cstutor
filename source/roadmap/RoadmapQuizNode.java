package roadmap;

import course.Page;
import main.CSTutor;

import javax.swing.*;
import java.awt.*;

/**
 * The RoadmapQuizNode class represent a specific quiz RoadmapNode
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class RoadmapQuizNode extends RoadmapNode {
   public int maxScore;
   public int retries;
   public boolean optional;

   public RoadmapQuizNode(Page page, RoadmapNode prev, RoadmapNode next, RoadmapPanel panel) {
      super(page, prev, next, panel);
      this.icon = new ImageIcon(getClass().getResource("/images/quiz.png"));
      this.maxScore = 0;
      this.retries = 0;
      this.optional = false;
   }

   /**
    * Sets number of retries for quiz
    * @param retries
    */
   public void setRetries(int retries) {
      this.retries = retries;
   }

   /**
    * Makes quiz optional
    */
   public void makeOptional() {
      this.optional = true;
   }
}