package roadmap;

import java.awt.CardLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import main.CSTutor;
import ui.MenuListener;

/**
 * The RoadmapActionListener class brings the Roadmap panel to the screen
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class RoadmapActionListener extends MenuListener {
   public RoadmapActionListener() {
      super();
   }

   /**
    * Brings roadmap panel to the screen
    */
   private void updatePanel() {
      CardLayout cl = (CardLayout) cs.cards.getLayout();
      cl.show(cs.cards, CSTutor.ROADMAP);
      ((RoadmapPanel)CSTutor.getInstance().roadmap.view).displayCourseSwitcher(CSTutor.getInstance().roadmap.view.bottomright);
   }

   @Override
   public void actionPerformed(ActionEvent arg0) {
      ((RoadmapPanel)CSTutor.getInstance().panels.get(CSTutor.ROADMAP)).setNavPanel();
      updatePanel();
   }

}