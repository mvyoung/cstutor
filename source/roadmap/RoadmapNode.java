package roadmap;

import course.*;
import main.CSTutor;

import javax.swing.*;
import java.awt.*;

/**
 *
 * The RoadmapNode class represents an object on the roadmap
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 *
 */
public class RoadmapNode {
   public Page page;
   public String name;
   public RoadmapNode prev;
   public RoadmapNode next;
   public RoadmapPanel panel;
   public Icon icon;

   public RoadmapNode(Page page, RoadmapNode prev, RoadmapNode next, RoadmapPanel panel) {
      this.page = page;
      this.name = page.pageName;
      this.prev = prev;
      this.next = next;
      this.panel = panel;
   }

   /**
    * Places the current node on the roadmap
    */
   /*@
    ensure
       This node is added to the roadmap panel
   @*/
   public void display(RoadmapNodeMouseListener listener) {
      System.out.println("node placed on roadmap");
      JLabel label = new JLabel(this.icon);
      label.addMouseListener(listener);
      this.panel.topright.add(label);
   }
//
//   /**
//    * Sets the previous node
//    */
//   /*@
//    ensure
//       this.next points to the next RoadmapNode
//   @*/
//   public void setPrev() {
//      this.prev = this.panel.model.prevNode;
//      System.out.println("prev node set");
//   }
//
//   /**
//    * Sets the next node
//    */
//   /*@
//    ensure
//       this.next points to the next RoadmapNode
//   @*/
//   public void setNext() {
//      this.next = this.panel.model.nextNode;
//      System.out.println("next node set");
//   }

   /**
    * Displays the currently selected node on the
    */
   /*@
    ensure
       name of node appears on the bottom panel as selected node
       selected node in Roadmap class is this node
   @*/
   public void selected() {
      this.panel.displayNoSelection(this.panel.bottomright);
      this.panel.displaySelected(this.panel.bottomright);
      System.out.println(this.name + " node selected");
   }
}