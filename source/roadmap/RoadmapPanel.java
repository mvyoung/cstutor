package roadmap;

import java.awt.*;
import java.awt.event.*;
import java.net.*;
import java.util.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.text.*;
import javax.swing.tree.*;

import main.CSTutor;
import navigator.*;
import course.*;

/**
 *
 * The RoadmapPanel class manages the view for the roadmap view
 * RoadmapPanel initializes and adds nodes to the JPanel
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 *
 */
public class RoadmapPanel extends JPanel implements TreeSelectionListener {
   /**
    * JTree for updating navigation panel
    */
   private JTree tree;

   /**
    * split pane
    */
   private JSplitPane splitPane;

   /**
    * right split pane
    */
   public JSplitPane right;

   /**
    * top right scroll pane
    */
   public JScrollPane topright;

   /**
    * bottom right panel
    */
   public JPanel bottomright;

   /**
    * Roadmap instance
    */
   public Roadmap model;
   
   public NavigatorPanel navCopy;

   public RoadmapPanel() {
      super(new BorderLayout());
      initialize();
   }

   public RoadmapPanel(int test) {

   }

   /**
    * Initializes the roadmap panel
    */
   public void initialize() {
      this.right = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
      this.topright = new JScrollPane();
      this.bottomright = new JPanel();
      this.splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);

      bottomright.setPreferredSize(new Dimension(10, 10));
      this.right.setTopComponent(topright);
      this.right.setBottomComponent(bottomright);

      splitPane.setLeftComponent(new JPanel());
      splitPane.setRightComponent(right);
      
      navCopy = new NavigatorPanel();

      add(splitPane, BorderLayout.WEST);
   }

   /**
    * Places all nodes on the roadmap
    */
   public void populate() {
      /**
       * Refresh list of nodes
       */
      this.model.refreshNodes();
      JPanel containGrid = new JPanel();
      GridLayout grid = new GridLayout();
      grid.setColumns(this.model.nodes.size());
      containGrid.setLayout(grid);

      /**
       * Add each node to grid
       */
      int size = this.model.nodes.size();
      System.out.println("NUMBER OF ROADMAPNODES: " + size);
      if (size == 0) {
         JLabel noPage = new JLabel("No course selected", JLabel.CENTER);
         containGrid.add(noPage);
      } else {
         for (RoadmapNode n : this.model.nodes) {
            JLabel page = new JLabel(n.name, new ImageIcon(getClass().getResource("/images/lesson.png")), JLabel.CENTER);
            page.setVerticalTextPosition(JLabel.BOTTOM);
            page.setHorizontalTextPosition(JLabel.CENTER);
            page.addMouseListener(new RoadmapNodeMouseListener(this, n));
            containGrid.add(page);
            if (--size > 0) {
               JLabel arrow = new JLabel(new ImageIcon(getClass().getResource("/images/arrow.png")));
               containGrid.add(arrow);
            }
         }
      }

      /**
       * Add grid with nodes to top right panel
       */
      topright.getViewport().removeAll();
      topright.getViewport().add(containGrid);
      topright.setPreferredSize(new Dimension(1600, 900));
      topright.revalidate();
      refreshNavigator();
   }

   /**
    * Saves a reference to the Roadmap class
    */
   public void setModel(Roadmap model) {
      this.model = model;
      displaySelected(bottomright);
      populate();
   }

   public void displayCourseSwitcher(JPanel panel) {
      JComboBox<String> courses = new JComboBox<String>();
      int index = 0;

      model.refreshCourses();
      if (model.checkSelectedCourse() == true) {
         for (Course c : model.courseList) {
            courses.addItem(c.courseName);
            if (c.courseName.equals(model.selectedCourse.courseName)) {
               courses.setSelectedIndex(index);
            }
            index += 1;
         }
      }

      courses.setPreferredSize(new Dimension(50, 50));
      courses.addActionListener(new CourseSelectorActionListener(this));
      displayNoSelection(panel);
      panel.add(courses, BorderLayout.CENTER);
      panel.revalidate();
   }

   /**
    * Removes panel's components
    * @param panel
    */
   public void displayNoSelection(JPanel panel) {
      panel.removeAll();
      panel.updateUI();
//      displayCourseSwitcher(panel);
   }

   /**
    * Refreshes the panel to update the currently selected Node
    * @param panel
    */
   public void displaySelected(JPanel panel) {
      JButton shiftLeft = new JButton("< Shift left");
      JButton shiftRight = new JButton("Shift right >");
      JButton cancel = new JButton("cancel");
      JPanel mid = new JPanel();
      JPanel bottom = new JPanel();

      /**
       * if there is no selected node, display an empty bottom panel
       */
      if (this.model.selectedNode == null) {
         displayNoSelection(panel);
         return;
      }

      /**
       * Adds action listeners to the bottom roadmap panel
       */
      shiftLeft.addActionListener(new RoadmapShiftActionListener(this, panel, "left"));
      shiftRight.addActionListener(new RoadmapShiftActionListener(this, panel, "right"));
      cancel.addActionListener(new RoadmapCancelActionListener(this, panel));

      /**
       * Adds selected node label to bottom panel
       */
      panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
      panel.add(new JLabel("Selected node: " + this.model.selectedNode.name), BorderLayout.CENTER);

      /**
       * Adds JComboBox to bottom panel
       */
      mid.setLayout(new BoxLayout(mid, BoxLayout.Y_AXIS));
      if (model.selectedNode.prev != null) {
         mid.add(new JLabel("Previous node: " + this.model.selectedNode.prev.name), BorderLayout.WEST);
      } else {
         mid.add(new JLabel("Previous node: N/A"), BorderLayout.WEST);
      }
      if (model.selectedNode.next != null) {
         mid.add(new JLabel("Next node: " + this.model.selectedNode.next.name), BorderLayout.EAST);
      } else {
         mid.add(new JLabel("Next node: N/A"), BorderLayout.EAST);
      }
      panel.add(mid, BorderLayout.CENTER);

      /**
       * Adds save and cancel button to bottom panel
       */
      bottom.setLayout(new BoxLayout(bottom, BoxLayout.X_AXIS));
      bottom.add(shiftLeft, BorderLayout.CENTER);
      bottom.add(cancel, BorderLayout.CENTER);
      bottom.add(shiftRight, BorderLayout.CENTER);
      panel.add(bottom, BorderLayout.CENTER);

      /**
       * Refresh panel
       */
      panel.revalidate();
   }

   /**
    * Imports a copy of the navigation panel and modifies it for the roadmap panel
    */
   public void setNavPanel() {
      /**
       * Vertically split left pane
       */
      JSplitPane splitPaneLeft = new JSplitPane(JSplitPane.VERTICAL_SPLIT);

      /**
       * Copy of the navigation pane
       */
      navCopy.refreshNavigator();
      JScrollPane copy = navCopy.getTreeView();

      /**
       * New JPanel for bottom left pane
       */
      JPanel bottomLeft = new JPanel();

      /**
       * New JButton for navigation view
       */
      JButton navigator = new JButton("Navigation View");

      /**
       * Add action listener to button and add button to panel
       */
      navigator.addActionListener(new NavigatorActionListener());
      bottomLeft.add(navigator);
      bottomLeft.setPreferredSize(new Dimension(10, 100));

      /**
       * Place panels in their respective scroll panes
       */
      splitPaneLeft.setTopComponent(copy);
      splitPaneLeft.setBottomComponent(bottomLeft);
      this.splitPane.setLeftComponent(splitPaneLeft);
      revalidate();
   }

   /**
    * Refreshes the navigator pane
    */
   private void refreshNavigator() {
      ((NavigatorPanel)CSTutor.getInstance().panels.get(CSTutor.NAVIGATOR)).refreshNavigator();
      setNavPanel();
   }

   @Override
   public void valueChanged(TreeSelectionEvent e) {
      DefaultMutableTreeNode node = (DefaultMutableTreeNode)
              tree.getLastSelectedPathComponent();

      if (node == null) return;
   }

   public DefaultMutableTreeNode MakeTree() {
      DefaultMutableTreeNode top =
              new DefaultMutableTreeNode("My Classes");
      DefaultMutableTreeNode courseNames =
              new DefaultMutableTreeNode("Course Name");
      DefaultMutableTreeNode topicNames =
              new DefaultMutableTreeNode("Topic Name");
      DefaultMutableTreeNode lessonNames =
              new DefaultMutableTreeNode("Lesson Name");
      DefaultMutableTreeNode addPage =
              new DefaultMutableTreeNode("Add Page");

      DefaultMutableTreeNode addCourse =
              new DefaultMutableTreeNode("Add Course");
      DefaultMutableTreeNode addTopic =
              new DefaultMutableTreeNode("Add Topic");
      DefaultMutableTreeNode addLesson =
              new DefaultMutableTreeNode("Add Lesson");

      top.add(courseNames);
      top.add(addCourse);
      courseNames.add(topicNames);
      courseNames.add(addTopic);
      topicNames.add(lessonNames);
      topicNames.add(addLesson);
      lessonNames.add(addPage);

      return top;
   }
}
