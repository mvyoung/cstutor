package roadmap;

import course.Page;
import main.CSTutor;

import javax.swing.*;
import java.awt.*;

/**
 * The RoadmapExerciseNode Class represents a specific exercise RoadmapNode
 *
 * @author Miles Young (mvyoung@calpoly.edu)
 */
public class RoadmapExerciseNode extends RoadmapNode {
   public RoadmapExerciseNode(Page page, RoadmapNode prev, RoadmapNode next, RoadmapPanel panel) {
      super(page, prev, next, panel);
      this.icon = new ImageIcon(getClass().getResource("/images/lesson.png"));
   }
}