package main;

import java.awt.*;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.util.*;

import javax.swing.*;

import assessment.*;
import authentication.*;
import communication.*;
import course.*;
import db.*;
import lesson.*;
import menu.*;
import navigator.*;
import python.*;
import roadmap.*;
import student.*;
import test.*;

// REMOVE ONCE YOU GET THE TEST CLASS WORKING
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;


/**
 * The main class which serves as the entry point to the program.
 *
 */
public class CSTutor {
   
   public static final String ASSESSMENT = "assessment", 
         NAVIGATOR = "navigator", 
         ROADMAP = "roadmap",
         START = "start",
         TUTORIAL = "tutorial",
         LESSONS = "lesson",
         QUIZ = "quiz";

   public static final String SERVER_ADDRESS = "162.243.144.203";
   public static final int SERVER_PORT = 55555;

   public JFrame frame;
   public JPanel cards;
   public Map<String, JPanel> panels;

   private static CSTutor cstutor;

   public Roadmap roadmap;
//   public ArrayList<Course> courses;

   public static User currentUser;

   public static ChatClient chatClient;
   
   private DatabaseService dbService;
   private CourseDatabase dbCourse;
   private TopicDatabase dbTopic;
   private LessonDatabase dbLesson;
   private PageDatabase dbPage;
   private QuizDestinationDatabase dbQDestination;


   private PythonService pythonService;
   
   /**
    * Returns the Python serivce.
    * @return
    */
   public PythonService getPythonService() {
      return pythonService;
   }

   /**
    * Returns the database service.
    * @return
    */
   public DatabaseService getDBService() {
      return dbService;
   }
   
   /**
    * Returns the course database.
    * @return
    */
   public CourseDatabase getDBCourse() {
      return dbCourse;
   }
   
   /**
    * Returns the topic database.
    * @return
    */
   public TopicDatabase getDBTopic() {
      return dbTopic;
   }
   
   /**
    * Returns the lesson database.
    * @return
    */
   public LessonDatabase getDBLesson() {
      return dbLesson;
   }
   
   /**
    * Returns the page database.
    * @return
    */
   public PageDatabase getDBPage() {
      return dbPage;
   }
   
   /**
    * Returns the quiz destination database.
    * @return
    */
   public QuizDestinationDatabase getDBQuizDestination() {
      return dbQDestination;
   }
   
   /**
    * This method returns the sole instance of this class which contains the ultimate
    * state of the program.
    * 
    * @return The singleton CSTutor object.
    */
   public static CSTutor getInstance() {
      return cstutor;  
   }

   /**
    * Constructor that initializes all the internal state.
    */
   CSTutor() {
      pythonService = new PythonService();
   }

   /**
    * The entry point to the program.
    * @param args
    */
   public static void main(String[] args) {
      boolean headless = false;
      if (args.length > 0) {
         if (args[0].equals("--headless")) {
            headless = true;
         }
         if (args[0].equals("--test")) {
            TestHandler th = new TestHandler();
            th.runTests();
            return;
         }
      }

      cstutor = new CSTutor();
      cstutor.initDB();

      cstutor.initUser();

      cstutor.initChat();

      if (!headless) {
         cstutor.initUI();
      }
      
      cstutor.checkPythonInterpreter();

      System.out.println(chatClient);

      System.out.println(cstutor);
   }

   /**
    * Verifies that the Python interpreter is working.
    */
   private void checkPythonInterpreter() {
 /*     System.out.println();
      System.out.println("CHECKING PYTHON RUNTIME");

      
      Process python = null;
      try {
         System.out.println("CREATED PYTHON INTERPRETER");
         
         System.out.println("WRITING TO PYTHON PROCESS");
              
         System.out.println("WROTE TO PYTHON PROCESS");
         
         
         
         System.out.println("OUTPUT FROM PYTHON PROCESS:");
         
         System.out.println("PYTHON OUTPUT COMPLETE");
         System.out.println();
      }
      catch (IOException e) {
         throw new RuntimeException(e);
      }
      finally {
         python.destroy();
      }
*/
   }

   /**
    * Initializes all the databases.
    */
   private void initDB() {
      dbService = new DatabaseService();
      dbCourse = new CourseDatabase();
      dbTopic = new TopicDatabase();
      dbLesson = new LessonDatabase();
      dbPage = new PageDatabase();
      dbQDestination = new QuizDestinationDatabase();
      
      Connection c = dbService.getConnect();
      Statement stmt = null;
      try {
         /*stmt = c.createStatement();
         String checkIfTableExists =
               "select name"
               + "from sqlite_master"
               + "where type='table'"
               + "and name='USERS'";
         ResultSet rs = stmt.executeQuery(checkIfTableExists);
         rs.*/
         System.out.println("Trying to create a table");
         stmt = c.createStatement();
         String createTable = 
               "create table if not exists USERS ("
                     + "POLY_ID text primary key not null,"
                     + "USER_TYPE text not null"
                     + ")";
         stmt.executeUpdate(createTable);
         stmt.close();
         System.out.println("Should have created a table");
      }
      catch (SQLException e) {
         e.printStackTrace();
         System.exit(-1);
      }
   }

   /**
    * Initializes the current user.
    */
   private void initUser(){
      // TODO: set valid user
      currentUser = new User("Steve Jobs"+((int)(Math.random()*1000000)), (int)(Math.random()), 0);
      System.out.println("ChatClient instance started.");
   }

   /**
    * Initializes the chat system.
    */
   private void initChat() {
      chatClient = new ChatClient(SERVER_ADDRESS, SERVER_PORT);

      Runtime.getRuntime().addShutdownHook(new Thread() {
         public void run() {
            try {
               chatClient.stop();
            } catch (Exception e) {

            }
            System.out.println("CSTutor ended.");
         }
      });

      System.out.println("ChatClient instance started.");
   }
   
   /**
    * Initializes the UI.
    */
   private void initUI() {   
      frame = new JFrame("CS TUTOR | " + currentUser);
      frame.setMinimumSize(new Dimension(800, 600));
      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

      panels = new HashMap<String, JPanel>();

      initStartPanel();
      initNavigatorPanel();
      initRoadmapPanel();
      initAssessmentPanel();
      initTutorialPanel();
      initCreateLesson();
      initCreateQuiz();

      initCardLayout();
      initMenuBar();

      CardLayout cl = (CardLayout) cards.getLayout();
      cl.show(cards, NAVIGATOR);
   
      frame.pack();
      frame.setVisible(true);
   }

   /**
    * Initializes the card layout.
    */
   public void initCardLayout() {
      JPanel cards = new JPanel(new CardLayout());

      for (Map.Entry<String, JPanel> e : panels.entrySet()) {
         String name= e.getKey();
         System.out.println("Adding card: " + name);
         cards.add(e.getValue(), name);
      }

      frame.getContentPane().add(cards, BorderLayout.CENTER);

      this.cards = cards;
   }

   /**
    * Initializes the roadmap panel.
    */
   private void initRoadmapPanel() {
      RoadmapPanel rp = new RoadmapPanel();
      Roadmap r = new Roadmap();
      r.setView(rp);
      rp.setModel(r);
      this.roadmap = r;

      this.panels.put(ROADMAP, rp);
   }

   /**
    * Initializes the navigator panel.
    */
   private void initNavigatorPanel() {
      panels.put(NAVIGATOR, new NavigatorPanel());
   }

   /**
    * Initializes the assessment panel.
    */
   private void initAssessmentPanel() {
      panels.put(ASSESSMENT, new AssessmentPanel());
   }

   /**
    * initializes the tutorial panel.
    */
   private void initTutorialPanel() {
      TutorialViewPanel tvp = new TutorialViewPanel();
      tvp.setPage(new Exercise());
      tvp.addPrevListener(new PagePreviousListener(tvp));
      tvp.addNextListener(new PageNextListener(tvp));
      tvp.addRunListener(new PageRunListener(tvp));
      panels.put(TUTORIAL, tvp);
   }

   /**
    * initializes the lesson panel.
    */
   private void initCreateLesson () {

      LessonPanel lp = new LessonPanel();
      lp.addPrevListener(new PagePListener(lp));
      lp.addNextListener(new PageNListener(lp));
      lp.addRunListener(new PageRListener(lp));

      //lp.setPage(new Exercise());
      panels.put(LESSONS, lp);
   }

   /**
    * initializes another lesson panel.
    */
   private void initCreateQuiz () {
      LessonPanel lpq = new LessonPanel();
      lpq.addPrevListener(new PagePListener(lpq));
      lpq.addNextListener(new PageNListener(lpq));
      lpq.addRunListener(new PageRListener(lpq));

      // lpq.setPage(new Exercise());
      panels.put(QUIZ, lpq);

   }

   /**
    * initializes the start panel.
    */
   private void initStartPanel() {
      JPanel start = new JPanel();
      URL url = getClass().getResource("/images/cstutor.png");
      if (url != null) {
         ImageIcon icon = new ImageIcon(url);
         start.add(new JLabel(icon));
      }
      else {
         start.add(new JLabel("image not found"));
      }
      panels.put(START, start);
   }

   /**
    * initializes the menu bar.
    */
   private void initMenuBar() {
      frame.setJMenuBar(new menu.MenuBar(this, frame, cards).getMenuBar());
   }
   
   /**
    * returns all  the courses in the program.
    * @return
    */
   public ArrayList<Course> getAllCourses(){
	   return CSTutor.getInstance().getDBCourse().selectAllCourses();
   }

}
