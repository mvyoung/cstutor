package assessment;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.*;
import javax.swing.border.*;
import java.util.ArrayList;

/**
 * Assessment GUI.
 * @author Joey Wilson 
 */
@SuppressWarnings("serial")
public class AssessmentPanel extends JPanel {

   /**
    * handler to interface with the database
    */
   AssessmentHandler handler;
   
   /**
    * Texfield to search the for specific users
    */
   JTextField search;
   ArrayList<JRadioButton> buttons = new ArrayList<JRadioButton>();

   /* SETUP FAKE DATA */
   private void fakeData() {
      /* Tom */
      handler.addAssessment(new Assessment("Tom","Conditionals","00:34","if statement",new Integer(2), new Float(79.1)));
      handler.addAssessment(new Assessment("Tom","Conditionals","00:23","switch statement",new Integer(1), new Float(69.3)));
      handler.addAssessment(new Assessment("Tom","Loop","00:12","while loop",new Integer(3), new Float(78.9)));
      handler.addAssessment(new Assessment("Tom","Loop","00:27","for loop",new Integer(2), new Float(94.2)));
      
      /* John */
      handler.addAssessment(new Assessment("John","Conditionals","00:56","if statement",new Integer(5), new Float(91.4)));
      handler.addAssessment(new Assessment("John","Conditionals","00:32","switch statement",new Integer(7), new Float(81.3)));
      handler.addAssessment(new Assessment("John","Loop","00:23","while loop",new Integer(2), new Float(88.2)));
      handler.addAssessment(new Assessment("John","Loop","00:52","for loop",new Integer(4), new Float(84.1)));
    
      /* Bill */
      handler.addAssessment(new Assessment("Bill","Conditionals","01:07","if statement",new Integer(3), new Float(73.5)));
      handler.addAssessment(new Assessment("Bill","Conditionals","00:43","switch statement",new Integer(2), new Float(83.6)));
      handler.addAssessment(new Assessment("Bill","Loop","00:17","while loop",new Integer(2), new Float(96.6)));
      handler.addAssessment(new Assessment("Bill","Loop","00:47","for loop",new Integer(1), new Float(61.3)));
      
      /* Sara */
      handler.addAssessment(new Assessment("Sara","Conditionals","01:03","if",new Integer(4), new Float(79.1)));
      handler.addAssessment(new Assessment("Sara","Conditionals","00:27","switch statement",new Integer(1), new Float(82.4)));
      handler.addAssessment(new Assessment("Sara","Loop","00:31","while loop",new Integer(2), new Float(76.1)));
      handler.addAssessment(new Assessment("Sara","Loop","00:42","for loop",new Integer(3), new Float(98.3)));
   }

   /**
    * Constructor to create a new assessment panel
    */
   public AssessmentPanel() {
      /**
       * use JPanel contrcutor to create this
       */
      super(new BorderLayout());
      setBorder(BorderFactory.createLineBorder(Color.black));
      handler = new AssessmentHandler();
      
      /**
       * Fill in some fake data if the database is empty
       */
      if (handler.getAssessments().size() == 0 ) {
         fakeData();
      }
      createAssessmentPanel();
   }
   
   /**
    * Method to fill in the header of the assessment panel and add the search listeners
    */
   private void createAssessmentPanel() {
      /**
       * Create the title panel 
       */
      JPanel titlePanel = new JPanel(new GridLayout(1,2,30,10));
      titlePanel.setBorder(BorderFactory.createLineBorder(Color.black));
      
      /**
       * Create the label to go on the panel
       */
      JLabel label = new JLabel("Assessments");
      label.setBorder(new EmptyBorder(5, 50, 25, 0) );
      label.setFont(new Font("", Font.BOLD, 24));
      titlePanel.add(label);
      
      /**
       * Create the search panel
       */
       
      JPanel searchPanel = new JPanel(); 
      searchPanel.setBorder(new EmptyBorder(5, 0, 25, 50) );
      ButtonGroup group = new ButtonGroup();
      
      /**
       * Create the check boxes for the search query
       */
      
      JLabel checkLabel = new JLabel("Search By:"); 
      checkLabel.setFont(new Font("", Font.BOLD, 12));
      searchPanel.add(checkLabel);
       
      JRadioButton userNameRadioButton = new JRadioButton("Username", true);
      searchPanel.add(userNameRadioButton);
      group.add(userNameRadioButton);
      buttons.add(userNameRadioButton);
      
      JRadioButton tutorialRadioButton = new JRadioButton("Tutorial", false);
      searchPanel.add(tutorialRadioButton);
      group.add(tutorialRadioButton);
      buttons.add(tutorialRadioButton);
      
      JRadioButton quizRadioButton = new JRadioButton("Quiz", false);
      searchPanel.add(quizRadioButton);
      group.add(quizRadioButton);
      buttons.add(quizRadioButton);
      
      /**
       * Create the search text field and add a click and key listener
       * The click listener clears the instructions when you clink on the box
       * The key listener completes the search when you press enter
       */
      search = new JTextField("Search...",20);
      search.setFont(new Font("", Font.PLAIN, 16));
      search.addMouseListener(new MouseAdapter(){
         @Override
         public void mouseClicked(MouseEvent e){
            if(search.getText().equals("Search...")) {
               search.setText("");
            }
         }
      });
      
      search.addKeyListener( new KeyAdapter(){
         public void keyPressed(KeyEvent e){
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
               updateAssessmentPanel();
            }       
         }
      });
      searchPanel.add(search);
      
      /**
       * Create a button incase a user doesn't know to press enter
       */
      JButton searchButton = new JButton("Search");
      searchButton.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) { 
            updateAssessmentPanel();
         }
      });
      searchPanel.add(searchButton);
      
      
      titlePanel.add(searchPanel);
      
      add(titlePanel,BorderLayout.NORTH);
   }
   
   /**
    * Method to update the JTable either on search or on page reload
    */
   public void updateAssessmentPanel() {
      
      /**
       * try catch to make sure the jtabel panel is there 
       */
      try {
         remove( ((BorderLayout)getLayout()).getLayoutComponent(BorderLayout.CENTER)); 
      } catch (Exception e) {
         // do nothing
      }
      
      /**
       * Add the jtable panel 
       */
      JPanel tablePanel = new JPanel(new BorderLayout());
      ArrayList<Assessment> assessmentList;
      
      /**
       * Preform search if there is a string, get all of the assessments if the search box is empty 
       */
      if (search.getText().equals("Search...") || search.getText().equals("")) {
         assessmentList = handler.getAssessments();
      }
      else {
         int selected = -1;
         String error = "";
         for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).isSelected()) {
               selected = i;
               break;
            }   
         }
         switch (selected) {
            case 0:
               error = "username";
               assessmentList = handler.getAssessmentByName(search.getText());  
               break;
            case 1:
               error = "tutorial";
               assessmentList = handler.getAssessmentByTutorial(search.getText());
               break;
            case 2:
               error = "quiz";
               assessmentList = handler.getAssessmentByQuiz(search.getText());
               break;
            default:
               return;
         }
         /**
          * Error message if no records exists for the given query
          */
         if(assessmentList.size() == 0) {
            JOptionPane.showMessageDialog(this,
               "No records exist for that " + error,
               "Error",
               JOptionPane.ERROR_MESSAGE);
            return;
         }
      }
      
      AssessmentTableModel tableModel = new AssessmentTableModel(assessmentList);
      
      /**
       * Create the JTable 
       */
      JTable table = new JTable(tableModel);
      table.setAutoCreateRowSorter(true);
      table.getRowSorter().toggleSortOrder(0);
      table.setRowHeight(25);
      
      JScrollPane scrollpane = new JScrollPane(table);
      table.setFillsViewportHeight(true);
      tablePanel.add(scrollpane);
      tablePanel.setBorder(BorderFactory.createLineBorder(Color.black));
      add(tablePanel,BorderLayout.CENTER);
 
      /**
       * Update the panel 
       */     
      revalidate();
      repaint();
   }
}
