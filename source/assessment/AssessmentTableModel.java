package assessment;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.util.ArrayList;

/**
 * Table model for the JTable used to display the assessments.
 * @author Joey Wilson 
 */
public class AssessmentTableModel extends AbstractTableModel {
    
    /**
     * List of assessments that the table 
     */
    ArrayList<Assessment> assessments;
    
    /**
     * Contructor for the table model
     * @param assessments ArrayList of Assessment objects to go in the database 
     */
    public AssessmentTableModel(ArrayList<Assessment> assessments) {
       this.assessments = assessments;
    }
    
    /**
     * Array of the columnNames
     */
    private String[] columnNames = {"Name","Tutorial","Time","Quiz","Attempts","Score"};
    
    /**
     * Method to get a name for the column
     * @param col integer of column to get the name for
     * @return name associated to that column
     */
    public String getColumnName(int col) {
        return columnNames[col].toString();
    }
    
    /**
     * Method to get how many rows the table should have
     * @return number of rows in the assessment
     */
    public int getRowCount() { 
       return assessments.size(); 
    }
    
     /**
     * Method to get how many columns the table should have
     * @return number of columns in the assessment
     */
    public int getColumnCount() {
      return columnNames.length; 
    }
    
    /**
     * Method to get the value for the cell in the table
     * @param row integer of row to get data for
     * @param col integer of column to get data for
     * @return Object to go in the cell
     */
    public Object getValueAt(int row, int col) {
       Assessment tmp = assessments.get(row); 
       /**
        * switch statement to map the col number into the arraylist
        */
       switch (col) {
          case 0: return tmp.userName;
          case 1: return tmp.tutorial;
          case 2: return tmp.time;
          case 3: return tmp.quiz;
          case 4: return tmp.attempts;
          case 5: return tmp.score;
          default: return "0";
       }
    }
}
