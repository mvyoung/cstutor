package assessment;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

// our imports
import main.CSTutor;
import ui.MenuListener;

/**
 * Assessment Action Listener to load the GUI on menu press.
 * @author Joey Wilson 
 */
public class AssessmentActionListener extends MenuListener {
   private AssessmentPanel panel;

   /**
    * Constructor for the Assessment Action Listerner
    */
   public AssessmentActionListener() {
      super();
      this.panel = (AssessmentPanel)cs.panels.get(CSTutor.ASSESSMENT);
   }

   /**
    * Method to switch to the assessment panel
    */
   private void switchToPanel() {
      /**
       * Switch panels to the assessment panel 
       */
      CardLayout cl = (CardLayout) cs.cards.getLayout();
      cl.show(cs.cards, CSTutor.ASSESSMENT);
   }

   /**
    * Method that responds to the assessment menue option being clicked
    */
   public void actionPerformed(ActionEvent e) {
       /**
        * update the assessment panel then switch to it 
        */
       panel.updateAssessmentPanel();
       switchToPanel();
   }
}

 
