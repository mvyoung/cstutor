package assessment;

import java.util.ArrayList;
import java.sql.*;

/**
 * Maintain a database of users.
 * Uses sqlite to maintain a database of user assessment.
 * @author Joey Wilson 
 */
public class AssessmentDB {
   
   /**
    * Global database connection
    */
   Connection c;
   
   /**
    * Constants for querying
    */
   public static final int ALL = 0, USERNAME = 1, TUTORIAL = 2, QUIZ = 3;  

   /**
    * Constructor for the assessment database 
    * Creates the table if it doesn't already exists
    *
    */
   public AssessmentDB() {
      
       createConnection();
       createTable();
   }

   /**
    * Method to creat the connection to the Assessment Database
    */
   public void createConnection() {
      /**
       * use jdbc to get a connection to the database 
       */
      try {
         Class.forName("org.sqlite.JDBC");
         c = DriverManager.getConnection("jdbc:sqlite:Assessment.db");
       } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
       }
   }

   /**
    * Method to create the table if it doesn't already exist
    */
   public void createTable() {
      /**
       * create the table using the sql statement in the string 
       */
      try {
         Statement stmt = c.createStatement();
         String sql = "CREATE TABLE IF NOT EXISTS Assessments " +
               "(username TEXT, " +
               " tutorial TEXT, " +
               " time TEXT, " +
               " quiz TEXT, " +
               " attempts INTEGER, " +
               " score REAL); "; 
         stmt.executeUpdate(sql);
         stmt.close();
      } catch ( Exception e ) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      }
   }

   /**
    * Method to add an assessment to the database
    * @param a Assessment to add to the database
    */
   public void addAssessment(Assessment a) {
      /**
       * Use the insert statement to store an assessment in the database 
       */
      try {
         if (!(a.userName == null || a.tutorial == null || a.time == null || a.quiz == null || a.attempts == null || a.score == null)) {
         
            Statement stmt = c.createStatement();
            String sql = "INSERT INTO Assessments (username, tutorial, time, quiz, attempts, score) " +
                         "VALUES ('" + a.userName + "', '" + a.tutorial + "', '" + a.time + "', '" + a.quiz + "', " + a.attempts + ", " + a.score + " );";
            stmt.executeUpdate(sql);
            stmt.close();
         }
      } catch ( Exception e) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      } 
   }
   /**
    * Method to retrive all of the data in the database with the given username for display in the GUI
    * @param queryParameter string to get the records for in the database
    * @param what the string should query for
    * @return returns a list of all the assessments with that username
    */
   public ArrayList<Assessment> getAssessments(String queryParameter, int type) {
      /**
       * Retrive the results from the query in the string with the given username added
       */
      try {
         String sql = "SELECT * FROM Assessments ";
         ArrayList<Assessment> assessmentList = new ArrayList<Assessment>();
         switch (type) {
            case ALL: 
               sql += ";";
               break;
            case USERNAME:
               sql += "WHERE username = '" + queryParameter + "';";
               break;
            case TUTORIAL:
               sql += "WHERE tutorial = '" + queryParameter + "';";
               break;
            case QUIZ:
               sql += "WHERE quiz = '" + queryParameter + "';";
               break;
            default:
               return assessmentList; // return the empty list and the UI will treat it like no records were found
         }
         // Vulnerable to SQL injection here
         Statement stmt = c.createStatement();
         ResultSet rs = stmt.executeQuery(sql);
         /**
          * Loop through the results to create a list to be returned 
          */
         while ( rs.next() ) {
            Assessment a = new Assessment(rs.getString("username"),rs.getString("tutorial"),rs.getString("time"),rs.getString("quiz"),rs.getInt("attempts"),rs.getFloat("score"));
            assessmentList.add(a);
         }
         rs.close();
         stmt.close();
         return assessmentList;
      } catch ( Exception e) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      } 
      return null;
   }

   /**
     * Method to close the connection to the database
     */
   public void closeConnection() {
      try {
         c.close();
      } catch ( Exception e) {
         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
      }
   }
}
