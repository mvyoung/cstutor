package assessment;
import java.util.List;

/**
 * An assessment for a student.
 * @author Joey Wilson 
 */
public class Assessment {

   /**
    * Variables that the assessment holds
    */
   public String userName;
   public String tutorial;
   public String time;
   public String quiz;
   public Integer attempts;
   public Float score;
  
   /**
    * Default constructor for the assessment
    */
   public Assessment() {}

   /**
    * Constructor fills in all the values of the assessment
    */
    /*@
      ensures
         (*
          * The new Assessment exists
          * exists a;
          *);
   @*/
   public Assessment(String userName, String tutorial, String time, String quiz, Integer attempts, Float score) { 
   
      this.userName = userName;
      this.tutorial = tutorial;
      this.time = time;
      this.quiz = quiz;
      this.attempts = attempts;
      this.score = score;
   }
   
   /**
    * Equals method used for comparison of two Assessment objects
    */
   @Override
   public boolean equals(Object obj) {
   
      /**
       * if the obj is being compared to itself return true
       */
      if (this == obj) {
         return true;
      }
      /**
       * make sure the object is an instance of Assessment
       */
      if (!(obj instanceof Assessment)) {
         return false;
      }
      
      Assessment a = (Assessment) obj;
      
      /**
       * compare each of the fields of the assessment making sure they are all equal
       */
      if (!(this.userName.equals(a.userName))) {
         return false;
      }
      if (!(this.tutorial.equals(a.tutorial))) {
         return false;
      }
      if (!(this.time.equals(a.time))) {
         return false;
      }
      if (!(this.quiz.equals(a.quiz))) {
         return false;
      }
      if (!(this.attempts.equals(a.attempts))) {
         return false;
      }
      if (!(this.score.equals(a.score))) {
         return false;
      }
      return true;
   }
}
