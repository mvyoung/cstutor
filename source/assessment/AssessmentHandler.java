package assessment;

import java.util.ArrayList;

/**
 * Wrapper class to make for an interface between the database and the application.
 * The JML is in this class for simplicity but all of these constraints are enforced by the DB class
 * @author Joey Wilson 
 */
public class AssessmentHandler {

   AssessmentDB db;
   
   /**
    * Constructor that creates a db instance.
    */
   public AssessmentHandler() {
      db = new AssessmentDB();
   }
   
   /** 
    * Add an assesment to the database.
    * @param a Assessment to add
    */
   /*@
      requires
         (*
          * That none of the fields are null
          * a.userName != null && a.tutorial != null && a.time != null
          * && a.quiz != null && a.attempts != null && a.score != null
          *);
      ensures
         (*
          * The new Assessment is added to the database
          * db.contains(a)
          *);
   @*/ 
   public void addAssessment(Assessment a) {
      db.addAssessment(a);
   }
   
   /** 
    * Get all the assesments from the database.
    * @return returns a list of all assessments
    */  
   /*@
      ensures
         (*
          * If there is a record in the database it should be in the result list
          * if the database is empty the list should be empty
          * 
          * (forall Assessment a ; db.contains(a) ;
          *         result.contains(a));
          * ||
          * !(exists Assessment a ; assesments.containts(a) ;
                     a.userName.equals(userName) && result.size() == 0); 
          *);
   @*/
   public ArrayList<Assessment> getAssessments() {
      return db.getAssessments("",AssessmentDB.ALL);
   }
   
   /** 
    * Get an assesment from the database.
    * @param userName userName to look for in the database
    * @return returns a list of all assessments matching the given username
    */ 
   /*@
      ensures
         (*
          * If there is a record with the given userName then all of the records in the
          * output list have that userName otherwise output an empty list
          * (exists Assessment a ; db.contains(a) ;
          *         a.userName.equals(userName) &&
                    (forall Assessment a; result.contains(a);
                     a.userName.equals(userName););
          * ||
          * !(exists Assessment a ; assesments.containts(a) ;
                     a.userName.equals(userName) && result.size() == 0); 
          *);
   @*/
   public ArrayList<Assessment> getAssessmentByName(String userName) {
      return db.getAssessments(userName,AssessmentDB.USERNAME);
   }
   
   /** 
    * Get an assesment from the database.
    * @param tutorial tutorial to look for in the database
    * @return returns a list of all assessments matching the given tutorial
    */ 
   /*@
      ensures
         (*
          * If there is a record with the given tutorial then all of the records in the
          * output list have that tutorial otherwise output an empty list
          * (exists Assessment a ; db.contains(a) ;
          *         a.tutorial.equals(tutorial) &&
                    (forall Assessment a; result.contains(a);
                     a.tutorial.equals(tutorial););
          * ||
          * !(exists Assessment a ; assesments.containts(a) ;
                     a.tutorial.equals(tutorial) && result.size() == 0); 
          *);
   @*/
   public ArrayList<Assessment> getAssessmentByTutorial(String tutorial) {
      return db.getAssessments(tutorial,AssessmentDB.TUTORIAL);
   }
   
   /** 
    * Get an assesment from the database.
    * @param quiz quiz to look for in the database
    * @return returns a list of all assessments matching the given quiz
    */ 
   /*@
      ensures
         (*
          * If there is a record with the given quiz then all of the records in the
          * output list have that quiz otherwise output an empty list
          * (exists Assessment a ; db.contains(a) ;
          *         a.quiz.equals(quiz) &&
                    (forall Assessment a; result.contains(a);
                     a.quiz.equals(quiz););
          * ||
          * !(exists Assessment a ; assesments.containts(a) ;
                     a.quiz.equals(quiz) && result.size() == 0); 
          *);
   @*/
   public ArrayList<Assessment> getAssessmentByQuiz(String quiz) {
      return db.getAssessments(quiz,AssessmentDB.QUIZ);
   } 
   
}
