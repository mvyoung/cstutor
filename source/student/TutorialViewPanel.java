package student;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import main.CSTutor;
import course.Page;
import db.PageDatabase;

/**
 * This panel displays a single page of a tutorial.
 * 
 * @author smirza
 *
 */
public class TutorialViewPanel extends JPanel {
   private static final long serialVersionUID = 1L;
   
   public JPanel textPanel;
   public JPanel codePanel;
   public JPanel outPanel;
   public JScrollPane textScroll, codeScroll, outScroll;
   public JSplitPane vertical, horizontal;
   
   public JButton prev, home, next, run;
   
   public JTextArea textArea;
   public JTextArea codeArea, outArea;
   
   private Page page;
   boolean pageChanged;
   
   protected PageDatabase pdb;

   /**
    * Accesses the underlying page of the panel.
    * @return
    */
   public Page getPage() {
      return page;
   }
   
   /**
    * Replaces the underlying page of the panel.
    * @param page
    */
   public void setPage(Page page) {
      this.page = page;
      pageChanged = true;
   }
   
   /**
    * Sets the page using the given page ID
    * @param id
    */
   public void setPageById(long id) {
	   Page p = pdb.selectPage(id);
	   setPage(p);
   }
   
   /**
    * Constructs the panel and initializes it.
    */
   public TutorialViewPanel() {
      super(new BorderLayout());
      pdb = CSTutor.getInstance().getDBPage();
      initialize();
   }
   
   /**
    * Initializes the panel.
    */
   private void initialize() {
      String docs = "The keyword def introduces a function definition. It must be followed by the function name and the parenthesized list of formal parameters. The statements that form the body of the function start at the next line, and must be indented.\r\n" + 
            "The first statement of the function body can optionally be a string literal; this string literal is the function's documentation string, or docstring. (More about docstrings can be found in the section Documentation Strings.) There are tools which use docstrings to automatically produce online or printed documentation, or to let the user interactively browse through code; it's good practice to include docstrings in code that you write, so make a habit of it.\r\n\n" + 
            "The execution of a function introduces a new symbol table used for the local variables of the function. More precisely, all variable assignments in a function store the value in the local symbol table; whereas variable references first look in the local symbol table, then in the local symbol tables of enclosing functions, then in the global symbol table, and finally in the table of built-in names. Thus, global variables cannot be directly assigned a value within a function (unless named in a global statement), although they may be referenced.\r\n" + 
            "The actual parameters (arguments) to a function call are introduced in the local symbol table of the called function when it is called; thus, arguments are passed using call by value (where the value is always an object reference, not the value of the object). [1] When a function calls another function, a new local symbol table is created for that call.\r\n" + 
            "A function definition introduces the function name in the current symbol table. The value of the function name has a type that is recognized by the interpreter as a user-defined function. This value can be assigned to another name which can then also be used as a function. This serves as a general renaming mechanism:";
      docs = "";
      textArea = new JTextArea(docs);
      textArea.setLineWrap(true);
      textArea.setWrapStyleWord(true);
      textScroll = new JScrollPane();
      textScroll.setBorder(BorderFactory.createTitledBorder("Documentation"));
      textScroll.setViewportView(textArea);
      textScroll.setPreferredSize(new Dimension(300, 600));
      
      Box buttonBox = Box.createHorizontalBox();
      prev = new JButton("Back");
      home = new JButton("Home");
      next = new JButton("Next");
      run = new JButton("Run");
      buttonBox.add(prev);
      buttonBox.add(home);
      buttonBox.add(next);
      buttonBox.add(run);
      
      textPanel = new JPanel(new BorderLayout());
      textPanel.add(textScroll, BorderLayout.CENTER);
      textPanel.add(buttonBox, BorderLayout.PAGE_END);
      
      
      codeArea = new JTextArea();
      codeArea.setEditable(true);
      codeScroll = new JScrollPane();
      codeScroll.setBorder(BorderFactory.createTitledBorder("Code"));
      codeScroll.setViewportView(codeArea);
      codeScroll.setPreferredSize(new Dimension(500, 500));

      codePanel = new JPanel(new BorderLayout());
      codePanel.add(codeScroll, BorderLayout.CENTER);
      
      
      outArea = new JTextArea();
      outScroll = new JScrollPane();
      outScroll.setBorder(BorderFactory.createTitledBorder("Output"));
      outScroll.setViewportView(outArea);
      outScroll.setPreferredSize(new Dimension(500, 100));
      
      outPanel = new JPanel();
      outPanel.add(outScroll, BorderLayout.CENTER);
      
      vertical = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
      horizontal = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
      
      vertical.setTopComponent(codePanel);
      vertical.setBottomComponent(outPanel);
      
      horizontal.setLeftComponent(textPanel);
      horizontal.setRightComponent(vertical);
      
      add(horizontal, BorderLayout.WEST);
   }
   
   /**
    * Adds a listener to the "previous" button.
    * @param listener
    */
   public void addPrevListener(ActionListener listener) {
      prev.addActionListener(listener);
   }
   
   /**
    * Adds a listener to the "next" button.
    * @param listener
    */
   public void addNextListener(ActionListener listener) {
      next.addActionListener(listener);
   }
   
   /**
    * Adds a listener to the "run" button.
    * @param listener
    */
   public void addRunListener(ActionListener listener) {
      run.addActionListener(listener);
   }
   
   /**
    * Rebuilds the panel if the page has changed.
    */
   public void maybeRebuild() {
      if (pageChanged) {
         pageChanged = false;
         // TODO something that redraws the panel
         textArea.setText(page.text.getText());
         codeArea.setText(page.code.getText());
         outArea.setText("");
//         revalidate();
      }
   }
}
 