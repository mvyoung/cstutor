package student;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.CSTutor;
import course.Page;
import db.PageDatabase;

/**
 * Calls the Page.prev method when invoked.
 * @author smirza
 *
 */
public class PagePreviousListener implements ActionListener {
   TutorialViewPanel panel;
   
   /**
    * Initializes the listener to act on the given panel.
    * @param panel
    */
   public PagePreviousListener(TutorialViewPanel panel) {
      this.panel = panel;
   }

   /**
    * Changes to the previous page.
    */
   @Override
   public void actionPerformed(ActionEvent event) {
	   Page current = panel.getPage();
	      PageDatabase pdb = CSTutor.getInstance().getDBPage();
	      Page prev = current.prev(pdb);
	      if (prev != null) {
	    	  panel.setPage(prev);
	          panel.maybeRebuild();  
	      }
	      
   }

}
