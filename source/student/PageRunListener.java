package student;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import course.CodeSegment;

/**
 * Calls the Page.run method when invoked.
 * @author smirza
 *
 */
public class PageRunListener implements ActionListener {

   TutorialViewPanel panel;
   
   public PageRunListener(TutorialViewPanel panel) {
      this.panel = panel;
   }
   
   /**
    * Runs the python code in the CodeSegment of the current page.
    * 
    * Redirects the standard output and error output streams of
    * the Python interpreter to the Output text area of the TutorialViewPanel.
    */
   @Override
   public void actionPerformed(ActionEvent event) {
      try {
         panel.getPage().code = new CodeSegment(panel.codeArea.getText());
         List<String> output = panel.getPage().run();
         
         panel.outArea.setText(null);
         for (String line : output) {
            panel.outArea.append(line);
         }
      }
      catch (IOException e) {
         throw new RuntimeException(e);
      }
   }

}
