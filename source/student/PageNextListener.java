package student;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.CSTutor;
import course.Page;
import db.PageDatabase;

/**
 * Calls the Page.next method when invoked.
 * @author smirza
 *
 */
public class PageNextListener implements ActionListener {

   TutorialViewPanel panel;
   
   /**
    * Initializes the listener to use the given panel.
    * @param panel
    */
   public PageNextListener(TutorialViewPanel panel) {
      this.panel = panel;
   }
   
   /**
    * Changes to the next page.
    */
   @Override
   public void actionPerformed(ActionEvent arg0) {
      Page current = panel.getPage();
      PageDatabase pdb = CSTutor.getInstance().getDBPage();
      Page next = current.next(pdb);
      if (next != null) {
    	  panel.setPage(next);
          panel.maybeRebuild();  
      }
      
   }

}
